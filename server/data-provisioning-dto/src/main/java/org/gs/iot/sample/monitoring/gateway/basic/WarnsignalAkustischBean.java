package org.gs.iot.sample.monitoring.gateway.basic;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;


/**
 * persistent data structure for hardware of type 'Buzzer'
 */
@XmlRootElement(namespace="org.gs.iot.sample.monitoring.gateway.basic")
public class WarnsignalAkustischBean { // start of class

    private boolean status;
    
    /**
     * creates an instance of WarnsignalAkustischBean
     */
    public WarnsignalAkustischBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean:DA-END
    }
    
    
    /**
     * getter for the field status
     *
     *
     *
     * @return
     */
    @XmlElement(name="status")
    public boolean isStatus() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean.isStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean.isStatus.boolean:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean.isStatus.boolean:DA-END
    }
    
    /**
     * setter for the field status
     *
     *
     *
     * @param status  the status
     */
    public void setStatus(boolean status) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean.setStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean.setStatus.boolean:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean.setStatus.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean.additional.elements.in.type:DA-END
} // end of java type