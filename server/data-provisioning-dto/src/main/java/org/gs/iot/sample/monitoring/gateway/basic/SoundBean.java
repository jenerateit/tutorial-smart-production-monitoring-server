package org.gs.iot.sample.monitoring.gateway.basic;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;


/**
 * persistent data structure for hardware of type 'SoundSensorGrove'
 */
@XmlRootElement(namespace="org.gs.iot.sample.monitoring.gateway.basic")
public class SoundBean { // start of class

    private int status;
    
    /**
     * creates an instance of SoundBean
     */
    public SoundBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.SoundBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.SoundBean:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.SoundBean:DA-END
    }
    
    
    /**
     * getter for the field status
     *
     *
     *
     * @return
     */
    @XmlElement(name="status")
    public int getStatus() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.SoundBean.getStatus.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.SoundBean.getStatus.int:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.SoundBean.getStatus.int:DA-END
    }
    
    /**
     * setter for the field status
     *
     *
     *
     * @param status  the status
     */
    public void setStatus(int status) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.SoundBean.setStatus.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.SoundBean.setStatus.int:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.SoundBean.setStatus.int:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.basic.SoundBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.SoundBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.basic.SoundBean.additional.elements.in.type:DA-END
} // end of java type