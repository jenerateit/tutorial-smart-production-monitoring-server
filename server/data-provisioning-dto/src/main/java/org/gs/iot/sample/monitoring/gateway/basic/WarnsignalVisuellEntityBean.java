package org.gs.iot.sample.monitoring.gateway.basic;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;


@XmlRootElement(namespace="org.gs.iot.sample.monitoring.gateway.basic")
public class WarnsignalVisuellEntityBean extends BaseEntityBean { // start of class

    private WarnsignalVisuellBean data;
    
    /**
     * creates an instance of WarnsignalVisuellEntityBean
     */
    public WarnsignalVisuellEntityBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean:DA-END
    }
    
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    @XmlElement(name="data")
    public WarnsignalVisuellBean getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean.getData.WarnsignalVisuellBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean.getData.WarnsignalVisuellBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean.getData.WarnsignalVisuellBean:DA-END
    }
    
    /**
     * setter for the field data
     *
     *
     *
     * @param data  the data
     */
    public void setData(WarnsignalVisuellBean data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean.setData.WarnsignalVisuellBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean.setData.WarnsignalVisuellBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean.setData.WarnsignalVisuellBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean.additional.elements.in.type:DA-END
} // end of java type