package org.gs.iot.sample.monitoring.gateway.basic;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;


/**
 * persistent data structure for hardware of type 'LED'
 */
@XmlRootElement(namespace="org.gs.iot.sample.monitoring.gateway.basic")
public class WarnsignalVisuellBean { // start of class

    private boolean status;
    
    /**
     * creates an instance of WarnsignalVisuellBean
     */
    public WarnsignalVisuellBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean:DA-END
    }
    
    
    /**
     * getter for the field status
     *
     *
     *
     * @return
     */
    @XmlElement(name="status")
    public boolean isStatus() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean.isStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean.isStatus.boolean:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean.isStatus.boolean:DA-END
    }
    
    /**
     * setter for the field status
     *
     *
     *
     * @param status  the status
     */
    public void setStatus(boolean status) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean.setStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean.setStatus.boolean:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean.setStatus.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean.additional.elements.in.type:DA-END
} // end of java type