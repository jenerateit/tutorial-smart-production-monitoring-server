package org.gs.iot.sample.monitoring.gateway.basic;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;


@XmlRootElement(namespace="org.gs.iot.sample.monitoring.gateway.basic")
public class IlluminanceEntityBean extends BaseEntityBean { // start of class

    private IlluminanceBean data;
    
    /**
     * creates an instance of IlluminanceEntityBean
     */
    public IlluminanceEntityBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean:DA-END
    }
    
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    @XmlElement(name="data")
    public IlluminanceBean getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean.getData.IlluminanceBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean.getData.IlluminanceBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean.getData.IlluminanceBean:DA-END
    }
    
    /**
     * setter for the field data
     *
     *
     *
     * @param data  the data
     */
    public void setData(IlluminanceBean data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean.setData.IlluminanceBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean.setData.IlluminanceBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean.setData.IlluminanceBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean.additional.elements.in.type:DA-END
} // end of java type