package org.gs.iot.sample.monitoring.gateway.basic;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;


@XmlRootElement(namespace="org.gs.iot.sample.monitoring.gateway.basic")
public class SoundEntityBean extends BaseEntityBean { // start of class

    private SoundBean data;
    
    /**
     * creates an instance of SoundEntityBean
     */
    public SoundEntityBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean:DA-END
    }
    
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    @XmlElement(name="data")
    public SoundBean getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean.getData.SoundBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean.getData.SoundBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean.getData.SoundBean:DA-END
    }
    
    /**
     * setter for the field data
     *
     *
     *
     * @param data  the data
     */
    public void setData(SoundBean data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean.setData.SoundBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean.setData.SoundBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean.setData.SoundBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean.additional.elements.in.type:DA-END
} // end of java type