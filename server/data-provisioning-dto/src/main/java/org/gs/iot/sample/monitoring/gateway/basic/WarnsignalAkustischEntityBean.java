package org.gs.iot.sample.monitoring.gateway.basic;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;


@XmlRootElement(namespace="org.gs.iot.sample.monitoring.gateway.basic")
public class WarnsignalAkustischEntityBean extends BaseEntityBean { // start of class

    private WarnsignalAkustischBean data;
    
    /**
     * creates an instance of WarnsignalAkustischEntityBean
     */
    public WarnsignalAkustischEntityBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean:DA-END
    }
    
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    @XmlElement(name="data")
    public WarnsignalAkustischBean getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean.getData.WarnsignalAkustischBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean.getData.WarnsignalAkustischBean:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean.getData.WarnsignalAkustischBean:DA-END
    }
    
    /**
     * setter for the field data
     *
     *
     *
     * @param data  the data
     */
    public void setData(WarnsignalAkustischBean data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean.setData.WarnsignalAkustischBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean.setData.WarnsignalAkustischBean:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean.setData.WarnsignalAkustischBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean.additional.elements.in.type:DA-END
} // end of java type