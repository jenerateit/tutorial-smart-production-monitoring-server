package com.vd;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import javax.persistence.Persistence;


public abstract class AbstractTest { // start of class

    private static EntityManagerFactory emf;
    
    private EntityManager em;
    
    
    /**
     * getter for the field em
     *
     *
     *
     * @return
     */
    protected EntityManager getEm() {
        //DA-START:com.vd.AbstractTest.getEm.EntityManager:DA-START
        //DA-ELSE:com.vd.AbstractTest.getEm.EntityManager:DA-ELSE
        return this.em;
        //DA-END:com.vd.AbstractTest.getEm.EntityManager:DA-END
    }
    
    /**
     * setter for the field em
     *
     *
     *
     * @param em  the em
     */
    protected void setEm(EntityManager em) {
        //DA-START:com.vd.AbstractTest.setEm.EntityManager:DA-START
        //DA-ELSE:com.vd.AbstractTest.setEm.EntityManager:DA-ELSE
        this.em = em;
        //DA-END:com.vd.AbstractTest.setEm.EntityManager:DA-END
    }
    
    /**
     * getter for the field emf
     *
     *
     *
     * @return
     */
    protected static EntityManagerFactory getEmf() {
        //DA-START:com.vd.AbstractTest.getEmf.EntityManagerFactory:DA-START
        //DA-ELSE:com.vd.AbstractTest.getEmf.EntityManagerFactory:DA-ELSE
        return AbstractTest.emf;
        //DA-END:com.vd.AbstractTest.getEmf.EntityManagerFactory:DA-END
    }
    
    /**
     * setter for the field emf
     *
     *
     *
     * @param emf  the emf
     */
    protected static void setEmf(EntityManagerFactory emf) {
        //DA-START:com.vd.AbstractTest.setEmf.EntityManagerFactory:DA-START
        //DA-ELSE:com.vd.AbstractTest.setEmf.EntityManagerFactory:DA-ELSE
        AbstractTest.emf = emf;
        //DA-END:com.vd.AbstractTest.setEmf.EntityManagerFactory:DA-END
    }
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:com.vd.AbstractTest.setUpBeforeClass:DA-START
        //DA-ELSE:com.vd.AbstractTest.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:com.vd.AbstractTest.setUpBeforeClass:DA-END
    }
    
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:com.vd.AbstractTest.tearDownAfterClass:DA-START
        //DA-ELSE:com.vd.AbstractTest.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:com.vd.AbstractTest.tearDownAfterClass:DA-END
    }
    
    /**
     */
    @Before
    public void setUp() {
        //DA-START:com.vd.AbstractTest.setUp:DA-START
        //DA-ELSE:com.vd.AbstractTest.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:com.vd.AbstractTest.setUp:DA-END
    }
    
    /**
     */
    @After
    public void tearDown() {
        //DA-START:com.vd.AbstractTest.tearDown:DA-START
        //DA-ELSE:com.vd.AbstractTest.tearDown:DA-ELSE
        return;
        //DA-END:com.vd.AbstractTest.tearDown:DA-END
    }
    
    //DA-START:com.vd.AbstractTest.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.AbstractTest.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.AbstractTest.additional.elements.in.type:DA-END
} // end of java type