import org.junit.Test;

import com.vd.AbstractTest;


public class TestTestDataGenerator extends AbstractTest { // start of class

    
    /**
     */
    @Test
    public void testCreateTestData() {
        //DA-START:TestTestDataGenerator.testCreateTestData:DA-START
        //DA-ELSE:TestTestDataGenerator.testCreateTestData:DA-ELSE
        new TestDataGenerator().createTestData(getEm());
        //DA-END:TestTestDataGenerator.testCreateTestData:DA-END
    }
    
    //DA-START:TestTestDataGenerator.additional.elements.in.type:DA-START
    //DA-ELSE:TestTestDataGenerator.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:TestTestDataGenerator.additional.elements.in.type:DA-END
} // end of java type