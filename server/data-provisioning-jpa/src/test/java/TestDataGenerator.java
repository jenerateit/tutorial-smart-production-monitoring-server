import java.util.LinkedHashSet;

import javax.persistence.EntityManager;

import org.gs.iot.sample.monitoring.gateway.BaseEntity;
import org.gs.iot.sample.monitoring.gateway.IlluminanceEntity;
import org.gs.iot.sample.monitoring.gateway.SoundEntity;
import org.gs.iot.sample.monitoring.gateway.TemperatureEntity;
import org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntity;
import org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity;


public class TestDataGenerator { // start of class

    private final LinkedHashSet<BaseEntity> entitiesBaseEntity = new LinkedHashSet<>();
    
    private final LinkedHashSet<TemperatureEntity> entitiesTemperatureEntity = new LinkedHashSet<>();
    
    private final LinkedHashSet<SoundEntity> entitiesSoundEntity = new LinkedHashSet<>();
    
    private final LinkedHashSet<WarnsignalVisuellEntity> entitiesWarnsignalVisuellEntity = new LinkedHashSet<>();
    
    private final LinkedHashSet<WarnsignalAkustischEntity> entitiesWarnsignalAkustischEntity = new LinkedHashSet<>();
    
    private final LinkedHashSet<IlluminanceEntity> entitiesIlluminanceEntity = new LinkedHashSet<>();
    
    /**
     * creates an instance of TestDataGenerator
     */
    public TestDataGenerator() {
        //DA-START:TestDataGenerator:DA-START
        //DA-ELSE:TestDataGenerator:DA-ELSE
        //DA-END:TestDataGenerator:DA-END
    }
    
    
    /**
     *
     * @param entityManager
     */
    public void createTestData(EntityManager entityManager) {
        //DA-START:TestDataGenerator.createTestData.EntityManager:DA-START
        //DA-ELSE:TestDataGenerator.createTestData.EntityManager:DA-ELSE
        // BaseEntity 
        // TemperatureEntity 
        // SoundEntity 
        // WarnsignalVisuellEntity 
        // WarnsignalAkustischEntity 
        // IlluminanceEntity 
        //DA-END:TestDataGenerator.createTestData.EntityManager:DA-END
    }
    
    /**
     *
     * @param entity
     */
    public void fillBaseEntity(BaseEntity entity) {
        //DA-START:TestDataGenerator.fillBaseEntity.BaseEntity:DA-START
        //DA-ELSE:TestDataGenerator.fillBaseEntity.BaseEntity:DA-ELSE
        return;
        //DA-END:TestDataGenerator.fillBaseEntity.BaseEntity:DA-END
    }
    
    /**
     *
     * @param entity
     */
    public void fillTemperatureEntity(TemperatureEntity entity) {
        //DA-START:TestDataGenerator.fillTemperatureEntity.TemperatureEntity:DA-START
        //DA-ELSE:TestDataGenerator.fillTemperatureEntity.TemperatureEntity:DA-ELSE
        return;
        //DA-END:TestDataGenerator.fillTemperatureEntity.TemperatureEntity:DA-END
    }
    
    /**
     *
     * @param entity
     */
    public void fillSoundEntity(SoundEntity entity) {
        //DA-START:TestDataGenerator.fillSoundEntity.SoundEntity:DA-START
        //DA-ELSE:TestDataGenerator.fillSoundEntity.SoundEntity:DA-ELSE
        return;
        //DA-END:TestDataGenerator.fillSoundEntity.SoundEntity:DA-END
    }
    
    /**
     *
     * @param entity
     */
    public void fillWarnsignalVisuellEntity(WarnsignalVisuellEntity entity) {
        //DA-START:TestDataGenerator.fillWarnsignalVisuellEntity.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:TestDataGenerator.fillWarnsignalVisuellEntity.WarnsignalVisuellEntity:DA-ELSE
        return;
        //DA-END:TestDataGenerator.fillWarnsignalVisuellEntity.WarnsignalVisuellEntity:DA-END
    }
    
    /**
     *
     * @param entity
     */
    public void fillWarnsignalAkustischEntity(WarnsignalAkustischEntity entity) {
        //DA-START:TestDataGenerator.fillWarnsignalAkustischEntity.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:TestDataGenerator.fillWarnsignalAkustischEntity.WarnsignalAkustischEntity:DA-ELSE
        return;
        //DA-END:TestDataGenerator.fillWarnsignalAkustischEntity.WarnsignalAkustischEntity:DA-END
    }
    
    /**
     *
     * @param entity
     */
    public void fillIlluminanceEntity(IlluminanceEntity entity) {
        //DA-START:TestDataGenerator.fillIlluminanceEntity.IlluminanceEntity:DA-START
        //DA-ELSE:TestDataGenerator.fillIlluminanceEntity.IlluminanceEntity:DA-ELSE
        return;
        //DA-END:TestDataGenerator.fillIlluminanceEntity.IlluminanceEntity:DA-END
    }
    
    /**
     *
     * @param arg0  the arg0
     */
    public static void main(String[] arg0) {
        //DA-START:TestDataGenerator.main.String.ARRAY:DA-START
        //DA-ELSE:TestDataGenerator.main.String.ARRAY:DA-ELSE
        return;
        //DA-END:TestDataGenerator.main.String.ARRAY:DA-END
    }
    
    /**
     *
     * @return
     */
    public BaseEntity getRandomEntityBaseEntity() {
        //DA-START:TestDataGenerator.getRandomEntityBaseEntity.BaseEntity:DA-START
        //DA-ELSE:TestDataGenerator.getRandomEntityBaseEntity.BaseEntity:DA-ELSE
        return null;
        //DA-END:TestDataGenerator.getRandomEntityBaseEntity.BaseEntity:DA-END
    }
    
    /**
     *
     * @return
     */
    public TemperatureEntity getRandomEntityTemperatureEntity() {
        //DA-START:TestDataGenerator.getRandomEntityTemperatureEntity.TemperatureEntity:DA-START
        //DA-ELSE:TestDataGenerator.getRandomEntityTemperatureEntity.TemperatureEntity:DA-ELSE
        return null;
        //DA-END:TestDataGenerator.getRandomEntityTemperatureEntity.TemperatureEntity:DA-END
    }
    
    /**
     *
     * @return
     */
    public SoundEntity getRandomEntitySoundEntity() {
        //DA-START:TestDataGenerator.getRandomEntitySoundEntity.SoundEntity:DA-START
        //DA-ELSE:TestDataGenerator.getRandomEntitySoundEntity.SoundEntity:DA-ELSE
        return null;
        //DA-END:TestDataGenerator.getRandomEntitySoundEntity.SoundEntity:DA-END
    }
    
    /**
     *
     * @return
     */
    public WarnsignalVisuellEntity getRandomEntityWarnsignalVisuellEntity() {
        //DA-START:TestDataGenerator.getRandomEntityWarnsignalVisuellEntity.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:TestDataGenerator.getRandomEntityWarnsignalVisuellEntity.WarnsignalVisuellEntity:DA-ELSE
        return null;
        //DA-END:TestDataGenerator.getRandomEntityWarnsignalVisuellEntity.WarnsignalVisuellEntity:DA-END
    }
    
    /**
     *
     * @return
     */
    public WarnsignalAkustischEntity getRandomEntityWarnsignalAkustischEntity() {
        //DA-START:TestDataGenerator.getRandomEntityWarnsignalAkustischEntity.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:TestDataGenerator.getRandomEntityWarnsignalAkustischEntity.WarnsignalAkustischEntity:DA-ELSE
        return null;
        //DA-END:TestDataGenerator.getRandomEntityWarnsignalAkustischEntity.WarnsignalAkustischEntity:DA-END
    }
    
    /**
     *
     * @return
     */
    public IlluminanceEntity getRandomEntityIlluminanceEntity() {
        //DA-START:TestDataGenerator.getRandomEntityIlluminanceEntity.IlluminanceEntity:DA-START
        //DA-ELSE:TestDataGenerator.getRandomEntityIlluminanceEntity.IlluminanceEntity:DA-ELSE
        return null;
        //DA-END:TestDataGenerator.getRandomEntityIlluminanceEntity.IlluminanceEntity:DA-END
    }
    
    /**
     * creates a random value for the given type
     *
     * @param type
     * @return
     */
    public Object getRandomValue(Class<?> type) {
        //DA-START:TestDataGenerator.getRandomValue.Object.Class:DA-START
        //DA-ELSE:TestDataGenerator.getRandomValue.Object.Class:DA-ELSE
        return null;
        //DA-END:TestDataGenerator.getRandomValue.Object.Class:DA-END
    }
    
    //DA-START:TestDataGenerator.additional.elements.in.type:DA-START
    //DA-ELSE:TestDataGenerator.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:TestDataGenerator.additional.elements.in.type:DA-END
} // end of java type