package org.gs.iot.sample.monitoring.gateway;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.vd.AbstractTest;


public class TestTemperatureEntity extends AbstractTest { // start of class

    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.setUpBeforeClass:DA-END
    }
    
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.tearDownAfterClass:DA-END
    }
    
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.setUp:DA-ELSE
         super.setUp();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.setUp:DA-END
    }
    
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.tearDown:DA-ELSE
         super.tearDown();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.tearDown:DA-END
    }
    
    /**
     */
    @Test
    public void testFind() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.testFind:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.testFind:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.testFind:DA-END
    }
    
    /**
     */
    @Test
    public void testPersist() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.testPersist:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.testPersist:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.testPersist:DA-END
    }
    
    /**
     */
    @Test
    public void testMerge() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.testMerge:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.testMerge:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.testMerge:DA-END
    }
    
    /**
     */
    @Test
    public void testRemove() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.testRemove:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.testRemove:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.testRemove:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntity.additional.elements.in.type:DA-END
} // end of java type