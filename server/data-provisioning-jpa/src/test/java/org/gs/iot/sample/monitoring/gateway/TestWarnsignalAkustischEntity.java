package org.gs.iot.sample.monitoring.gateway;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.vd.AbstractTest;


public class TestWarnsignalAkustischEntity extends AbstractTest { // start of class

    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.setUpBeforeClass:DA-END
    }
    
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.tearDownAfterClass:DA-END
    }
    
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.setUp:DA-ELSE
         super.setUp();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.setUp:DA-END
    }
    
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.tearDown:DA-ELSE
         super.tearDown();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.tearDown:DA-END
    }
    
    /**
     */
    @Test
    public void testFind() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.testFind:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.testFind:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.testFind:DA-END
    }
    
    /**
     */
    @Test
    public void testPersist() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.testPersist:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.testPersist:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.testPersist:DA-END
    }
    
    /**
     */
    @Test
    public void testMerge() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.testMerge:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.testMerge:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.testMerge:DA-END
    }
    
    /**
     */
    @Test
    public void testRemove() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.testRemove:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.testRemove:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.testRemove:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntity.additional.elements.in.type:DA-END
} // end of java type