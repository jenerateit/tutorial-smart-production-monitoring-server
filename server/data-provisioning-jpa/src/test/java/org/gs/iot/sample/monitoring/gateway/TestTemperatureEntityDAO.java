package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class TestTemperatureEntityDAO { // start of class

    private static EntityManagerFactory emf;
    
    private static TemperatureEntityDAO dao;
    
    private EntityManager em;
    
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("H2DS-IOT-DEV");
        dao = new TemperatureEntityDAO();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.setUpBeforeClass:DA-END
    }
    
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.tearDownAfterClass:DA-END
    }
    
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.setUp:DA-END
    }
    
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.tearDown:DA-END
    }
    
    /**
     */
    @Test
    public void testGet() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testGet:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testGet:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testGet:DA-END
    }
    
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testGetAll:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testGetAll:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testGetAll:DA-END
    }
    
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testCreate:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testCreate:DA-END
    }
    
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testUpdate:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testUpdate:DA-END
    }
    
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testDelete:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.testDelete:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.TestTemperatureEntityDAO.additional.elements.in.type:DA-END
} // end of java type