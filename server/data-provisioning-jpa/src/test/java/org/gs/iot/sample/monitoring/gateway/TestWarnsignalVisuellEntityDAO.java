package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class TestWarnsignalVisuellEntityDAO { // start of class

    private static EntityManagerFactory emf;
    
    private static WarnsignalVisuellEntityDAO dao;
    
    private EntityManager em;
    
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("H2DS-IOT-DEV");
        dao = new WarnsignalVisuellEntityDAO();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.setUpBeforeClass:DA-END
    }
    
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.tearDownAfterClass:DA-END
    }
    
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.setUp:DA-END
    }
    
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.tearDown:DA-END
    }
    
    /**
     */
    @Test
    public void testGet() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testGet:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testGet:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testGet:DA-END
    }
    
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testGetAll:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testGetAll:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testGetAll:DA-END
    }
    
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testCreate:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testCreate:DA-END
    }
    
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testUpdate:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testUpdate:DA-END
    }
    
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testDelete:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.testDelete:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalVisuellEntityDAO.additional.elements.in.type:DA-END
} // end of java type