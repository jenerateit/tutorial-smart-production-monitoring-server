package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class TestWarnsignalAkustischEntityDAO { // start of class

    private static EntityManagerFactory emf;
    
    private static WarnsignalAkustischEntityDAO dao;
    
    private EntityManager em;
    
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("H2DS-IOT-DEV");
        dao = new WarnsignalAkustischEntityDAO();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.setUpBeforeClass:DA-END
    }
    
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.tearDownAfterClass:DA-END
    }
    
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.setUp:DA-END
    }
    
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.tearDown:DA-END
    }
    
    /**
     */
    @Test
    public void testGet() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testGet:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testGet:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testGet:DA-END
    }
    
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testGetAll:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testGetAll:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testGetAll:DA-END
    }
    
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testCreate:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testCreate:DA-END
    }
    
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testUpdate:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testUpdate:DA-END
    }
    
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testDelete:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.testDelete:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.TestWarnsignalAkustischEntityDAO.additional.elements.in.type:DA-END
} // end of java type