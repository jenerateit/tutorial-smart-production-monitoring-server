package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class TestSoundEntityDAO { // start of class

    private static EntityManagerFactory emf;
    
    private static SoundEntityDAO dao;
    
    private EntityManager em;
    
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("H2DS-IOT-DEV");
        dao = new SoundEntityDAO();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.setUpBeforeClass:DA-END
    }
    
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.tearDownAfterClass:DA-END
    }
    
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.setUp:DA-END
    }
    
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.tearDown:DA-END
    }
    
    /**
     */
    @Test
    public void testGet() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testGet:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testGet:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testGet:DA-END
    }
    
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testGetAll:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testGetAll:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testGetAll:DA-END
    }
    
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testCreate:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testCreate:DA-END
    }
    
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testUpdate:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testUpdate:DA-END
    }
    
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testDelete:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.testDelete:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntityDAO.additional.elements.in.type:DA-END
} // end of java type