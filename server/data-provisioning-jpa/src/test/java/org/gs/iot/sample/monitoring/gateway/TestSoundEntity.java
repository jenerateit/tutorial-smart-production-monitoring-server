package org.gs.iot.sample.monitoring.gateway;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.vd.AbstractTest;


public class TestSoundEntity extends AbstractTest { // start of class

    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.setUpBeforeClass:DA-END
    }
    
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.tearDownAfterClass:DA-END
    }
    
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.setUp:DA-ELSE
         super.setUp();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.setUp:DA-END
    }
    
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.tearDown:DA-ELSE
         super.tearDown();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.tearDown:DA-END
    }
    
    /**
     */
    @Test
    public void testFind() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.testFind:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.testFind:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.testFind:DA-END
    }
    
    /**
     */
    @Test
    public void testPersist() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.testPersist:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.testPersist:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.testPersist:DA-END
    }
    
    /**
     */
    @Test
    public void testMerge() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.testMerge:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.testMerge:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.testMerge:DA-END
    }
    
    /**
     */
    @Test
    public void testRemove() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.testRemove:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.testRemove:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.testRemove:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.TestSoundEntity.additional.elements.in.type:DA-END
} // end of java type