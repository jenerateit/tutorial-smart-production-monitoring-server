package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class TestIlluminanceEntityDAO { // start of class

    private static EntityManagerFactory emf;
    
    private static IlluminanceEntityDAO dao;
    
    private EntityManager em;
    
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("H2DS-IOT-DEV");
        dao = new IlluminanceEntityDAO();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.setUpBeforeClass:DA-END
    }
    
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.tearDownAfterClass:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.tearDownAfterClass:DA-END
    }
    
    /**
     */
    @Before
    public void setUp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.setUp:DA-END
    }
    
    /**
     */
    @After
    public void tearDown() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.tearDown:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.tearDown:DA-END
    }
    
    /**
     */
    @Test
    public void testGet() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testGet:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testGet:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testGet:DA-END
    }
    
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testGetAll:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testGetAll:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testGetAll:DA-END
    }
    
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testCreate:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testCreate:DA-END
    }
    
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testUpdate:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testUpdate:DA-END
    }
    
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testDelete:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.testDelete:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.TestIlluminanceEntityDAO.additional.elements.in.type:DA-END
} // end of java type