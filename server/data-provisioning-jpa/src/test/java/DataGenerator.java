import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Utility class to generate test data for database entries.
 */
public class DataGenerator {

	private static Random random = new Random();
	private static Map<Object, Integer> stringCounter = new HashMap<>();
	private static boolean fillStringsUpToMaximumLength = false;
	private static Map<String, List<String>> dictionaries = new HashMap<>();

	public static Date DATE_1970 = new Date(0);
	public static Date DATE_1DAY_BEFORE_TODAY = new Date((new Date()).getTime() - (24L * 60 * 60 * 1000));
	public static Date DATE_1WEEK_BEFORE_TODAY = new Date((new Date()).getTime() - (7L * 24 * 60 * 60 * 1000));
	public static Date DATE_30DAYS_BEFORE_TODAY = new Date((new Date()).getTime() - (30L * 24 * 60 * 60 * 1000));
	public static Date DATE_1YEAR_LATER_THAN_TODAY = new Date((new Date()).getTime() + (365L * 24 * 60 * 60 * 1000));
	
	public static void main(String[] args) {
		for (int ii=0; ii<100; ii++) {
		    System.out.println(getStringValue(false, 5, 9, "testdata"));
		}
		
	}

	/**
	 * @param name
	 */
	private static void loadFile(String name) {
		if (name == null || name.length() == 0) {
			;
		} else if (dictionaries.containsKey(name)) {
			;
		} else {
			URL url = TestDataGenerator.class.getClassLoader().getResource(name);
			System.out.println("URL: " + url);
			if (url != null) {
				try {
					List<String> lines = new ArrayList<>();

					ZipInputStream zis = new ZipInputStream(url.openStream());
					System.out.println("ZipInputStream: " + zis);
					ZipEntry entry = null;
					while ((entry = zis.getNextEntry()) != null) {
//						System.out.println("Entry: " + entry.getName());
						if (entry != null && entry.getName() != null
								&& entry.getName().toLowerCase().startsWith("final")) {
							System.out.println("Entry: " + entry.getName());
							LineNumberReader reader = new LineNumberReader(new InputStreamReader(zis));
							String text = reader.readLine();
							while (text != null && text.length() > 0) {
								lines.add(text);
								text = reader.readLine();
							}
						}
					}
					dictionaries.put(name, lines);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				throw new RuntimeException(
						"ERROR: dictionary with name " + name + " not found as a resource of class loader "
								+ TestDataGenerator.class.getClassLoader().getClass().getName());
			}
		}
	}

	/**
	 * @return the fillStringsUpToMaximumLength
	 */
	public static boolean getFillStringsUpToMaximumLength() {
		return fillStringsUpToMaximumLength;
	}

	/**
	 * @param fillStringsUpToMaximumLength
	 *            the fillStringsUpToMaximumLength to set
	 */
	public static void setFillStringsUpToMaximumLength(boolean fillStringsUpToMaximumLength) {
		DataGenerator.fillStringsUpToMaximumLength = fillStringsUpToMaximumLength;
	}

	/**
	 * Generates a random Integer value
	 * 
	 * @return a random Integer value
	 */
	public static Integer getIntegerValue() {
		return getIntegerValue(false);
	}

	/**
	 * Generates a random Integer value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @return a random Integer value
	 */
	public static Integer getIntegerValue(boolean nullable) {
		return getIntegerValue(nullable, Integer.MAX_VALUE);
	}

	/**
	 * Generates a random Integer value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param maxValue
	 *            the maximum random value
	 * @return a random Integer value
	 */
	public static Integer getIntegerValue(boolean nullable, int maxValue) {
		return getIntegerValue(nullable, Integer.MIN_VALUE, maxValue);
	}

	/**
	 * Generates a random Integer value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param minValue
	 *            the smalest possible random value
	 * @param maxValue
	 *            the biggest possible random value
	 * @return a random Integer value
	 */
	public static Integer getIntegerValue(boolean nullable, int minValue, int maxValue) {
		if (minValue >= maxValue) {
			throw new IllegalArgumentException(
					"The max value '" + maxValue + "' is less or equal the min value '" + minValue + "'");
		}

		if (nullable && random.nextBoolean()) {
			return null;
		}

		BigDecimal max = new BigDecimal(maxValue);
		BigDecimal min = new BigDecimal(minValue);
		return new Integer(max.subtract(min).multiply(new BigDecimal(random.nextDouble())).add(min).intValue());
	}

	/**
	 * Generates a random Long value
	 * 
	 * @return a random Long value
	 */
	public static Long getLongValue() {
		return getLongValue(false);
	}

	/**
	 * Generates a random Long value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @return a random Long value
	 */
	public static Long getLongValue(boolean nullable) {
		return getLongValue(nullable, Long.MAX_VALUE);
	}

	/**
	 * Generates a random Long value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param maxValue
	 *            the maximum random value
	 * @return a random Long value
	 */
	public static Long getLongValue(boolean nullable, long maxValue) {
		return getLongValue(nullable, Long.MIN_VALUE, maxValue);
	}

	/**
	 * Generates a random Long value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param minValue
	 *            the smalest possible random value
	 * @param maxValue
	 *            the biggest possible random value
	 * @return a random Long value
	 */
	public static Long getLongValue(boolean nullable, long minValue, long maxValue) {
		if (minValue >= maxValue) {
			throw new IllegalArgumentException(
					"The max value '" + maxValue + "' is less or equal the min value '" + minValue + "'");
		}

		if (nullable && random.nextBoolean()) {
			return null;
		}

		BigDecimal max = new BigDecimal(maxValue);
		BigDecimal min = new BigDecimal(minValue);
		return new Long(max.subtract(min).multiply(new BigDecimal(random.nextDouble())).add(min).longValue());
	}

	/**
	 * Generates a random Float value
	 * 
	 * @return a random Float value
	 */
	public static Float getFloatValue() {
		return getFloatValue(false);
	}

	/**
	 * Generates a random Float value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @return a random Float value
	 */
	public static Float getFloatValue(boolean nullabel) {
		return getFloatValue(nullabel, Float.MAX_VALUE);
	}

	/**
	 * Generates a random Float value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param maxValue
	 *            the biggest possible random value
	 * @return a random Float value
	 */
	public static Float getFloatValue(boolean nullable, float maxValue) {
		return getFloatValue(nullable, Float.MAX_VALUE * -1, maxValue);
	}

	/**
	 * Generates a random Float value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param minValue
	 *            the smalest possible random value
	 * @param maxValue
	 *            the biggest possible random value
	 * @return a random Float value
	 * @throws IllegalArgumentException
	 *             if the maxValue is equals or less than the minValue
	 */
	public static Float getFloatValue(boolean nullable, float minValue, float maxValue) {
		if (minValue >= maxValue) {
			throw new IllegalArgumentException(
					"The max value '" + maxValue + "' is less or equal the min value '" + minValue + "'");
		}

		if (nullable && random.nextBoolean()) {
			return null;
		}

		BigDecimal max = new BigDecimal(maxValue);
		BigDecimal min = new BigDecimal(minValue);
		return new Float(max.subtract(min).multiply(new BigDecimal(random.nextDouble())).add(min).floatValue());
	}

	/**
	 * Generates a random Double value
	 * 
	 * @param maxValue
	 *            the biggest possible random value
	 * @return a random Double value
	 */
	public static Double getDoubleValue() {
		return getDoubleValue(false);
	}

	/**
	 * Generates a random Double value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @return a random Double value
	 */
	public static Double getDoubleValue(boolean nullable) {
		return getDoubleValue(nullable, Double.MAX_VALUE);
	}

	/**
	 * Generates a random Double value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param maxValue
	 *            the biggest possible random value
	 * @return a random Double value
	 */
	public static Double getDoubleValue(boolean nullable, double maxValue) {
		return getDoubleValue(nullable, Double.MAX_VALUE * -1, maxValue);
	}

	/**
	 * Generates a random Double value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param minValue
	 *            the smalest possible random value
	 * @param maxValue
	 *            the biggest possible random value
	 * @return a random Double value
	 * @throws IllegalArgumentException
	 *             if the maxValue is equals or less than the minValue
	 */
	public static Double getDoubleValue(boolean nullable, double minValue, double maxValue) {
		if (minValue >= maxValue) {
			throw new IllegalArgumentException(
					"The max value '" + maxValue + "' is less or equal the min value '" + minValue + "'");
		}

		if (nullable && random.nextBoolean()) {
			return null;
		}

		BigDecimal max = new BigDecimal(maxValue);
		BigDecimal min = new BigDecimal(minValue);
		return new Double(max.subtract(min).multiply(new BigDecimal(random.nextDouble())).add(min).doubleValue());
	}

	/**
	 * Generates a random Date value
	 * 
	 * @return a random Date value
	 */
	public static Date getDateValue() {
		return getDateValue(false);
	}

	/**
	 * Generates a random Date value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @return a random Date value
	 */
	public static Date getDateValue(boolean nullable) {
		return getDateValue(nullable, new Date(Long.MAX_VALUE));
	}

	/**
	 * Generates a random Date value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param maxDate
	 *            the maximum date value
	 * @return a random Date value
	 * @throws IllegalArgumentException
	 *             if the maxDate object is null
	 */
	public static Date getDateValue(boolean nullable, Date maxDate) {
		return getDateValue(nullable, new Date(0), maxDate);
	}

	/**
	 * Generates a random Date value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param minDate
	 *            the minimum date value
	 * @param maxDate
	 *            the maximum date value
	 * @return a random Date value
	 * @throws IllegalArgumentException
	 *             if the minDate or maxDate object is null
	 */
	public static Date getDateValue(boolean nullable, Date minDate, Date maxDate) {
		if (minDate == null) {
			throw new IllegalArgumentException("The minDate may not be null");

		} else if (maxDate == null) {
			throw new IllegalArgumentException("The maxDate may not be null");

		} else if (maxDate.compareTo(minDate) < 0) {
			throw new IllegalArgumentException("The maxDate " + maxDate + " is earlier than the minDate " + minDate);

		} else if (nullable && random.nextBoolean()) {
			return null;

		} else {
			BigDecimal max = new BigDecimal(maxDate.getTime());
			BigDecimal min = new BigDecimal(minDate.getTime());
			return new Date(max.subtract(min).multiply(new BigDecimal(random.nextDouble())).add(min).longValue());
		}
	}

	/**
	 * Generates a random Date value
	 * 
	 * @return a random Date value
	 */
	public static Timestamp getTimestampValue() {
		return getTimestampValue(false);
	}

	/**
	 * Generates a random Date value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @return a random Date value
	 */
	public static Timestamp getTimestampValue(boolean nullable) {
		return getTimestampValue(nullable, new Date(Long.MAX_VALUE));
	}

	/**
	 * Generates a random Date value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param maxDate
	 *            the maximum date value
	 * @return a random Date value
	 * @throws IllegalArgumentException
	 *             if the maxDate object is null
	 */
	public static Timestamp getTimestampValue(boolean nullable, Date maxDate) {
		return getTimestampValue(nullable, new Date(0), maxDate);
	}

	/**
	 * Generates a random Date value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param minDate
	 *            the minimum date value
	 * @param maxDate
	 *            the maximum date value
	 * @return a random Date value
	 * @throws IllegalArgumentException
	 *             if the minDate or maxDate object is null
	 */
	public static Timestamp getTimestampValue(boolean nullable, Date minDate, Date maxDate) {
		Timestamp result = null;

		if (minDate == null) {
			throw new IllegalArgumentException("The minDate may not be null");

		} else if (maxDate == null) {
			throw new IllegalArgumentException("The maxDate may not be null");

		} else if (maxDate.compareTo(minDate) < 0) {
			throw new IllegalArgumentException("The maxDate is earlier than the minDate");

		} else if (nullable && random.nextBoolean()) {
			result = null;

		} else {
			Date date = getDateValue(nullable, minDate, maxDate);
			if (date != null) {
				result = new Timestamp(date.getTime());
			}
		}

		return result;
	}

	/**
	 * Generates a random String value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param maxLength
	 *            the max length of the generated string
	 * @return a random String value
	 * @throws IllegalArgumentException
	 *             if the min and max length does not fit
	 */
	public static String getStringValue(boolean nullabel, int maxLenth) {
		return getStringValue(nullabel, 0, maxLenth);
	}

	/**
	 * Generates a random String value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param minLength
	 *            the min length of the generated string
	 * @param maxLength
	 *            the max length of the generated string
	 * @return a random String value
	 * @throws IllegalArgumentException
	 *             if the min and max length does not fit
	 */
	public static String getStringValue(boolean nullable, int minLength, int maxLength) {
		return getStringValue(nullable, minLength, maxLength, null);
	}

	/**
	 * Generates a random String value
	 * 
	 * @param nullable
	 *            if the value may be a null value
	 * @param minLength
	 *            the min length of the generated string
	 * @param maxLength
	 *            the max length of the generated string
	 * @param proposal
	 *            a string that, if it is not null, should be part of the
	 *            returned string value
	 * @return a random String value
	 * @throws IllegalArgumentException
	 *             if the min and max length does not fit
	 */
	public static String getStringValue(boolean nullable, int minLength, int maxLength, Object proposal) {
		String result = null;
		if (maxLength <= minLength) {
			throw new IllegalArgumentException("The maxlength is less than the minLength");

		} else if (nullable && random.nextBoolean()) {
			return null;

		} else {

			List<String> words = null;
			if (proposal == null) {
				loadFile("scowl-2017.08.24.zip");
				words = dictionaries.get("scowl-2017.08.24.zip");
			}

			StringBuilder sb = null;
			if (proposal == null) {
				Integer length = getIntegerValue(false, minLength, maxLength);
				sb = new StringBuilder(length.intValue() + 20);
				int ii = 0;
				do {
					Integer nextPos = getIntegerValue(false, 0, words.size() - 1);
					String word = words.get(nextPos.intValue());
					ii++;
					if (word.length() + sb.length() < length) {
						if (sb.length() > 0) sb.append(" ");
						sb.append(word);
					} else if (ii > 100) {
						if (sb.length() == 0) {
							while (sb.length() < length) {
								sb.append("x");
							}
						}
						break;
					}
				} while (sb.length() < length);
			} else {
				sb = new StringBuilder(proposal.toString());
				int counter = 0;
				if (stringCounter.containsKey(proposal)) {
					counter = stringCounter.get(proposal);
				} else {
					stringCounter.put(proposal, counter);
				}
				sb.append(counter);
				if (sb.length() > maxLength) {
					result = sb.substring(0, maxLength);
				} else if (sb.length() == maxLength) {
					// use the string as is
				}
				counter++;
				stringCounter.put(proposal, counter);
			}
			
			if (sb.length() >  maxLength) {
				sb.delete(maxLength, sb.length());
			}
			if (getFillStringsUpToMaximumLength()) {
				// append as many "#" to the string such that the
				// maxlength size is reached
				// this way we can easily test in the GUI whether the
				// maximum sized strings
				// can be seen properly
				while (sb.length() < maxLength) {
					sb.append('#');
				}
			}
			result = sb.toString();

			return result;
		}
	}
}