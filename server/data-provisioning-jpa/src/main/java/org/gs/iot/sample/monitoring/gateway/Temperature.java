package org.gs.iot.sample.monitoring.gateway;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * persistent data structure for hardware of type 'TemperatureSensorGrove'
 */
@Embeddable
public class Temperature implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private float status;
    
    
    /**
     * getter for the field status
     *
     *
     *
     * @return
     */
    @Basic
    @Column(name="status", unique=false, nullable=true)
    public float getStatus() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.Temperature.getStatus.float:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.Temperature.getStatus.float:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.Temperature.getStatus.float:DA-END
    }
    
    /**
     * setter for the field status
     *
     *
     *
     * @param status  the status
     */
    public void setStatus(float status) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.Temperature.setStatus.float:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.Temperature.setStatus.float:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.Temperature.setStatus.float:DA-END
    }
    
    /**
     *
     * @return
     */
    public int hashCode() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.Temperature.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.Temperature.hashCode.int:DA-ELSE
        return 0;
        //DA-END:org.gs.iot.sample.monitoring.gateway.Temperature.hashCode.int:DA-END
    }
    
    /**
     *
     * @param arg0  the arg0
     * @return
     */
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.Temperature.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.Temperature.equals.Object.boolean:DA-ELSE
        return true;
        //DA-END:org.gs.iot.sample.monitoring.gateway.Temperature.equals.Object.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.Temperature.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.Temperature.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.Temperature.additional.elements.in.type:DA-END
} // end of java type