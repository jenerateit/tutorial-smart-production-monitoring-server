package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;


public class IlluminanceEntityLifecycleListener { // start of class

    
    /**
     *
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(IlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.preUpdate.IlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.preUpdate.IlluminanceEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.preUpdate.IlluminanceEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(IlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.postUpdate.IlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.postUpdate.IlluminanceEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.postUpdate.IlluminanceEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(IlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.prePersist.IlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.prePersist.IlluminanceEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.prePersist.IlluminanceEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(IlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.postPersist.IlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.postPersist.IlluminanceEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.postPersist.IlluminanceEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(IlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.preRemove.IlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.preRemove.IlluminanceEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.preRemove.IlluminanceEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(IlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.postRemove.IlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.postRemove.IlluminanceEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.postRemove.IlluminanceEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(IlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.postLoad.IlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.postLoad.IlluminanceEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.postLoad.IlluminanceEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type