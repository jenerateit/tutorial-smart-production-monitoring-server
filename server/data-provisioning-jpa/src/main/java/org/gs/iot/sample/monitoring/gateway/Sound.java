package org.gs.iot.sample.monitoring.gateway;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * persistent data structure for hardware of type 'SoundSensorGrove'
 */
@Embeddable
public class Sound implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private int status;
    
    
    /**
     * getter for the field status
     *
     *
     *
     * @return
     */
    @Basic
    @Column(name="status", unique=false, nullable=true)
    public int getStatus() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.Sound.getStatus.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.Sound.getStatus.int:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.Sound.getStatus.int:DA-END
    }
    
    /**
     * setter for the field status
     *
     *
     *
     * @param status  the status
     */
    public void setStatus(int status) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.Sound.setStatus.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.Sound.setStatus.int:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.Sound.setStatus.int:DA-END
    }
    
    /**
     *
     * @return
     */
    public int hashCode() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.Sound.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.Sound.hashCode.int:DA-ELSE
        return 0;
        //DA-END:org.gs.iot.sample.monitoring.gateway.Sound.hashCode.int:DA-END
    }
    
    /**
     *
     * @param arg0  the arg0
     * @return
     */
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.Sound.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.Sound.equals.Object.boolean:DA-ELSE
        return true;
        //DA-END:org.gs.iot.sample.monitoring.gateway.Sound.equals.Object.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.Sound.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.Sound.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.Sound.additional.elements.in.type:DA-END
} // end of java type