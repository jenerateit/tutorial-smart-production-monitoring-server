package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;


/**
 * JPA Metamodel description for {@link org.gs.iot.sample.monitoring.gateway.WarnsignalAkustisch}.
 *
 * persistent data structure for hardware of type 'Buzzer'
 *
 * @see org.gs.iot.sample.monitoring.gateway.WarnsignalAkustisch
 */
@StaticMetamodel(value=org.gs.iot.sample.monitoring.gateway.WarnsignalAkustisch.class)
public class WarnsignalAkustisch_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.monitoring.gateway.WarnsignalAkustisch#isStatus()}.
     *
     * @see org.gs.iot.sample.monitoring.gateway.WarnsignalAkustisch#isStatus()
     */
    public static volatile SingularAttribute<WarnsignalAkustisch, Boolean> status;
    
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustisch_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustisch_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustisch_.additional.elements.in.type:DA-END
} // end of java type