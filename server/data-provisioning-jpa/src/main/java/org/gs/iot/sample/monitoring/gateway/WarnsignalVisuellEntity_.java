package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;


/**
 * JPA Metamodel description for {@link org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity}.
 *
 * @see org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity
 */
@StaticMetamodel(value=org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.class)
public class WarnsignalVisuellEntity_ extends BaseEntity_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.monitoring.gateway.BaseEntity#getPk()}.
     *
     * @see org.gs.iot.sample.monitoring.gateway.BaseEntity#getPk()
     */
    public static volatile SingularAttribute<WarnsignalVisuellEntity, Long> pk;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity#getData()}.
     *
     * @see org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity#getData()
     */
    public static volatile SingularAttribute<WarnsignalVisuellEntity, WarnsignalVisuell> data;
    
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity_.additional.elements.in.type:DA-END
} // end of java type