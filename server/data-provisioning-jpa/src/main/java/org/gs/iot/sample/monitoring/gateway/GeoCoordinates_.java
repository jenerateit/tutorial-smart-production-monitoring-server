package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;


/**
 * JPA Metamodel description for {@link org.gs.iot.sample.monitoring.gateway.GeoCoordinates}.
 *
 * @see org.gs.iot.sample.monitoring.gateway.GeoCoordinates
 */
@StaticMetamodel(value=org.gs.iot.sample.monitoring.gateway.GeoCoordinates.class)
public class GeoCoordinates_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.monitoring.gateway.GeoCoordinates#getLng()}.
     *
     * @see org.gs.iot.sample.monitoring.gateway.GeoCoordinates#getLng()
     */
    public static volatile SingularAttribute<GeoCoordinates, Integer> lng;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.monitoring.gateway.GeoCoordinates#getLat()}.
     *
     * @see org.gs.iot.sample.monitoring.gateway.GeoCoordinates#getLat()
     */
    public static volatile SingularAttribute<GeoCoordinates, Integer> lat;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.monitoring.gateway.GeoCoordinates#getAlt()}.
     *
     * @see org.gs.iot.sample.monitoring.gateway.GeoCoordinates#getAlt()
     */
    public static volatile SingularAttribute<GeoCoordinates, Integer> alt;
    
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.GeoCoordinates_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.GeoCoordinates_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.GeoCoordinates_.additional.elements.in.type:DA-END
} // end of java type