package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;


/**
 * JPA Metamodel description for {@link org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntity}.
 *
 * @see org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntity
 */
@StaticMetamodel(value=org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntity.class)
public class WarnsignalAkustischEntity_ extends BaseEntity_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.monitoring.gateway.BaseEntity#getPk()}.
     *
     * @see org.gs.iot.sample.monitoring.gateway.BaseEntity#getPk()
     */
    public static volatile SingularAttribute<WarnsignalAkustischEntity, Long> pk;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntity#getData()}.
     *
     * @see org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntity#getData()
     */
    public static volatile SingularAttribute<WarnsignalAkustischEntity, WarnsignalAkustisch> data;
    
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntity_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntity_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntity_.additional.elements.in.type:DA-END
} // end of java type