package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;


/**
 * JPA Metamodel description for {@link org.gs.iot.sample.monitoring.gateway.Sound}.
 *
 * persistent data structure for hardware of type 'SoundSensorGrove'
 *
 * @see org.gs.iot.sample.monitoring.gateway.Sound
 */
@StaticMetamodel(value=org.gs.iot.sample.monitoring.gateway.Sound.class)
public class Sound_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.monitoring.gateway.Sound#getStatus()}.
     *
     * @see org.gs.iot.sample.monitoring.gateway.Sound#getStatus()
     */
    public static volatile SingularAttribute<Sound, Integer> status;
    
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.Sound_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.Sound_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.Sound_.additional.elements.in.type:DA-END
} // end of java type