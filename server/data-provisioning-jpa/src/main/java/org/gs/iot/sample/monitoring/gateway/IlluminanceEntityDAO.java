package org.gs.iot.sample.monitoring.gateway;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;


public class IlluminanceEntityDAO { // start of class

    
    /**
     *
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static IlluminanceEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.get.EntityManager.Object.IlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.get.EntityManager.Object.IlluminanceEntity:DA-ELSE
        return entityManager.find(IlluminanceEntity.class, id);
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.get.EntityManager.Object.IlluminanceEntity:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<IlluminanceEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<IlluminanceEntity> cq = cb.createQuery(IlluminanceEntity.class);
        cq.from(IlluminanceEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static IlluminanceEntity create(EntityManager entityManager, IlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.create.EntityManager.IlluminanceEntity.IlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.create.EntityManager.IlluminanceEntity.IlluminanceEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.create.EntityManager.IlluminanceEntity.IlluminanceEntity:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static IlluminanceEntity update(EntityManager entityManager, IlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.update.EntityManager.IlluminanceEntity.IlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.update.EntityManager.IlluminanceEntity.IlluminanceEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.update.EntityManager.IlluminanceEntity.IlluminanceEntity:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, IlluminanceEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.delete.EntityManager.IlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.delete.EntityManager.IlluminanceEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.delete.EntityManager.IlluminanceEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO.additional.elements.in.type:DA-END
} // end of java type