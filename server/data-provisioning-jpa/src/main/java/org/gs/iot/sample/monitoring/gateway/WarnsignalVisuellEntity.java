package org.gs.iot.sample.monitoring.gateway;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


//DA-START:gateway.WarnsignalVisuellEntity:DA-START
//DA-ELSE:gateway.WarnsignalVisuellEntity:DA-ELSE
@Entity
@Table(name="warnsignalvisuellentity")
@EntityListeners(value={org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.class})
//DA-END:gateway.WarnsignalVisuellEntity:DA-END

public class WarnsignalVisuellEntity extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private WarnsignalVisuell data;
    
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.getData.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.getData.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.getData.annotations:DA-END
    public WarnsignalVisuell getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.getData.WarnsignalVisuell:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.getData.WarnsignalVisuell:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.getData.WarnsignalVisuell:DA-END
    }
    
    /**
     * setter for the field data
     *
     *
     *
     * @param data  the data
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.setData.WarnsignalVisuell.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.setData.WarnsignalVisuell.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.setData.WarnsignalVisuell.annotations:DA-END
    public void setData(WarnsignalVisuell data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.setData.WarnsignalVisuell:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.setData.WarnsignalVisuell:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.setData.WarnsignalVisuell:DA-END
    }
    
    /**
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.hashCode.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.hashCode.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.hashCode.int:DA-END
    }
    
    /**
     *
     * @param arg0  the arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.equals.Object.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.equals.Object.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.equals.Object.boolean:DA-END
    }
    
    /**
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.getPk.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="warnsignalvisuellentity_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="warnsignalvisuellentity_seq", sequenceName="warnsignalvisuellentity_seq", initialValue=1, allocationSize=1)
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.getPk.Long:DA-ELSE
        return super.getPkInternal();
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.getPk.Long:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity.additional.elements.in.type:DA-END
} // end of java type