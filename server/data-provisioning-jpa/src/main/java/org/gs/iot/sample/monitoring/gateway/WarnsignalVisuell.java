package org.gs.iot.sample.monitoring.gateway;

import java.io.Serializable;

import javax.persistence.Embeddable;


/**
 * persistent data structure for hardware of type 'LED'
 */
@Embeddable
public class WarnsignalVisuell implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private boolean status;
    
    
    /**
     * getter for the field status
     *
     *
     *
     * @return
     */
    public boolean isStatus() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.isStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.isStatus.boolean:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.isStatus.boolean:DA-END
    }
    
    /**
     * setter for the field status
     *
     *
     *
     * @param status  the status
     */
    public void setStatus(boolean status) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.setStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.setStatus.boolean:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.setStatus.boolean:DA-END
    }
    
    /**
     *
     * @return
     */
    public int hashCode() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.hashCode.int:DA-ELSE
        return 0;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.hashCode.int:DA-END
    }
    
    /**
     *
     * @param arg0  the arg0
     * @return
     */
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.equals.Object.boolean:DA-ELSE
        return true;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.equals.Object.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell.additional.elements.in.type:DA-END
} // end of java type