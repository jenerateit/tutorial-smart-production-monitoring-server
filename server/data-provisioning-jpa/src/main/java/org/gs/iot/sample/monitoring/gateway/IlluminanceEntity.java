package org.gs.iot.sample.monitoring.gateway;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


//DA-START:gateway.IlluminanceEntity:DA-START
//DA-ELSE:gateway.IlluminanceEntity:DA-ELSE
@Entity
@Table(name="illuminanceentity")
@EntityListeners(value={org.gs.iot.sample.monitoring.gateway.IlluminanceEntityLifecycleListener.class})
//DA-END:gateway.IlluminanceEntity:DA-END

public class IlluminanceEntity extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private Illuminance data;
    
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.getData.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.getData.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.getData.annotations:DA-END
    public Illuminance getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.getData.Illuminance:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.getData.Illuminance:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.getData.Illuminance:DA-END
    }
    
    /**
     * setter for the field data
     *
     *
     *
     * @param data  the data
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.setData.Illuminance.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.setData.Illuminance.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.setData.Illuminance.annotations:DA-END
    public void setData(Illuminance data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.setData.Illuminance:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.setData.Illuminance:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.setData.Illuminance:DA-END
    }
    
    /**
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.hashCode.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.hashCode.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.hashCode.int:DA-END
    }
    
    /**
     *
     * @param arg0  the arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.equals.Object.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.equals.Object.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.equals.Object.boolean:DA-END
    }
    
    /**
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.getPk.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="illuminanceentity_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="illuminanceentity_seq", sequenceName="illuminanceentity_seq", initialValue=1, allocationSize=1)
    //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.getPk.Long:DA-ELSE
        return super.getPkInternal();
        //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.getPk.Long:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.IlluminanceEntity.additional.elements.in.type:DA-END
} // end of java type