package org.gs.iot.sample.monitoring.gateway;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;


public class WarnsignalAkustischEntityDAO { // start of class

    
    /**
     *
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static WarnsignalAkustischEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.get.EntityManager.Object.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.get.EntityManager.Object.WarnsignalAkustischEntity:DA-ELSE
        return entityManager.find(WarnsignalAkustischEntity.class, id);
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.get.EntityManager.Object.WarnsignalAkustischEntity:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<WarnsignalAkustischEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<WarnsignalAkustischEntity> cq = cb.createQuery(WarnsignalAkustischEntity.class);
        cq.from(WarnsignalAkustischEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static WarnsignalAkustischEntity create(EntityManager entityManager, WarnsignalAkustischEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.create.EntityManager.WarnsignalAkustischEntity.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.create.EntityManager.WarnsignalAkustischEntity.WarnsignalAkustischEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.create.EntityManager.WarnsignalAkustischEntity.WarnsignalAkustischEntity:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static WarnsignalAkustischEntity update(EntityManager entityManager, WarnsignalAkustischEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.update.EntityManager.WarnsignalAkustischEntity.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.update.EntityManager.WarnsignalAkustischEntity.WarnsignalAkustischEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.update.EntityManager.WarnsignalAkustischEntity.WarnsignalAkustischEntity:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, WarnsignalAkustischEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.delete.EntityManager.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.delete.EntityManager.WarnsignalAkustischEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.delete.EntityManager.WarnsignalAkustischEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO.additional.elements.in.type:DA-END
} // end of java type