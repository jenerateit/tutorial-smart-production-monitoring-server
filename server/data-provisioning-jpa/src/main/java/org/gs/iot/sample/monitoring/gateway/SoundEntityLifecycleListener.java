package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;


public class SoundEntityLifecycleListener { // start of class

    
    /**
     *
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(SoundEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.preUpdate.SoundEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.preUpdate.SoundEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.preUpdate.SoundEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(SoundEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.postUpdate.SoundEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.postUpdate.SoundEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.postUpdate.SoundEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(SoundEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.prePersist.SoundEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.prePersist.SoundEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.prePersist.SoundEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(SoundEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.postPersist.SoundEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.postPersist.SoundEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.postPersist.SoundEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(SoundEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.preRemove.SoundEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.preRemove.SoundEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.preRemove.SoundEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(SoundEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.postRemove.SoundEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.postRemove.SoundEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.postRemove.SoundEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(SoundEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.postLoad.SoundEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.postLoad.SoundEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.postLoad.SoundEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type