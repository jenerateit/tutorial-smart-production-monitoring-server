package org.gs.iot.sample.monitoring.gateway;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;


public class WarnsignalVisuellEntityDAO { // start of class

    
    /**
     *
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static WarnsignalVisuellEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.get.EntityManager.Object.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.get.EntityManager.Object.WarnsignalVisuellEntity:DA-ELSE
        return entityManager.find(WarnsignalVisuellEntity.class, id);
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.get.EntityManager.Object.WarnsignalVisuellEntity:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<WarnsignalVisuellEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<WarnsignalVisuellEntity> cq = cb.createQuery(WarnsignalVisuellEntity.class);
        cq.from(WarnsignalVisuellEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static WarnsignalVisuellEntity create(EntityManager entityManager, WarnsignalVisuellEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.create.EntityManager.WarnsignalVisuellEntity.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.create.EntityManager.WarnsignalVisuellEntity.WarnsignalVisuellEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.create.EntityManager.WarnsignalVisuellEntity.WarnsignalVisuellEntity:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static WarnsignalVisuellEntity update(EntityManager entityManager, WarnsignalVisuellEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.update.EntityManager.WarnsignalVisuellEntity.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.update.EntityManager.WarnsignalVisuellEntity.WarnsignalVisuellEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.update.EntityManager.WarnsignalVisuellEntity.WarnsignalVisuellEntity:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, WarnsignalVisuellEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.delete.EntityManager.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.delete.EntityManager.WarnsignalVisuellEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.delete.EntityManager.WarnsignalVisuellEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO.additional.elements.in.type:DA-END
} // end of java type