package org.gs.iot.sample.monitoring.gateway;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;


public class SoundEntityDAO { // start of class

    
    /**
     *
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static SoundEntity get(EntityManager entityManager, Object id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.get.EntityManager.Object.SoundEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.get.EntityManager.Object.SoundEntity:DA-ELSE
        return entityManager.find(SoundEntity.class, id);
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.get.EntityManager.Object.SoundEntity:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<SoundEntity> getAll(EntityManager entityManager) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<SoundEntity> cq = cb.createQuery(SoundEntity.class);
        cq.from(SoundEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static SoundEntity create(EntityManager entityManager, SoundEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.create.EntityManager.SoundEntity.SoundEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.create.EntityManager.SoundEntity.SoundEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.create.EntityManager.SoundEntity.SoundEntity:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static SoundEntity update(EntityManager entityManager, SoundEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.update.EntityManager.SoundEntity.SoundEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.update.EntityManager.SoundEntity.SoundEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.update.EntityManager.SoundEntity.SoundEntity:DA-END
    }
    
    /**
     *
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, SoundEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.delete.EntityManager.SoundEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.delete.EntityManager.SoundEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.delete.EntityManager.SoundEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntityDAO.additional.elements.in.type:DA-END
} // end of java type