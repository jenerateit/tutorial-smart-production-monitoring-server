package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;


/**
 * JPA Metamodel description for {@link org.gs.iot.sample.monitoring.gateway.Illuminance}.
 *
 * persistent data structure for hardware of type 'DigitalLightTSL2561Grove'
 *
 * @see org.gs.iot.sample.monitoring.gateway.Illuminance
 */
@StaticMetamodel(value=org.gs.iot.sample.monitoring.gateway.Illuminance.class)
public class Illuminance_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.monitoring.gateway.Illuminance#getStatus()}.
     *
     * @see org.gs.iot.sample.monitoring.gateway.Illuminance#getStatus()
     */
    public static volatile SingularAttribute<Illuminance, Float> status;
    
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.Illuminance_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.Illuminance_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.Illuminance_.additional.elements.in.type:DA-END
} // end of java type