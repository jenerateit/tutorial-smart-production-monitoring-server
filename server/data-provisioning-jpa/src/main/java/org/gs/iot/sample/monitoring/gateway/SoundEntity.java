package org.gs.iot.sample.monitoring.gateway;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


//DA-START:gateway.SoundEntity:DA-START
//DA-ELSE:gateway.SoundEntity:DA-ELSE
@Entity
@Table(name="soundentity")
@EntityListeners(value={org.gs.iot.sample.monitoring.gateway.SoundEntityLifecycleListener.class})
//DA-END:gateway.SoundEntity:DA-END

public class SoundEntity extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private Sound data;
    
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntity.getData.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntity.getData.annotations:DA-ELSE
    @Embedded
    //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntity.getData.annotations:DA-END
    public Sound getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntity.getData.Sound:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntity.getData.Sound:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntity.getData.Sound:DA-END
    }
    
    /**
     * setter for the field data
     *
     *
     *
     * @param data  the data
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntity.setData.Sound.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntity.setData.Sound.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntity.setData.Sound.annotations:DA-END
    public void setData(Sound data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntity.setData.Sound:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntity.setData.Sound:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntity.setData.Sound:DA-END
    }
    
    /**
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntity.hashCode.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntity.hashCode.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntity.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntity.hashCode.int:DA-END
    }
    
    /**
     *
     * @param arg0  the arg0
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntity.equals.Object.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntity.equals.Object.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntity.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntity.equals.Object.boolean:DA-END
    }
    
    /**
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntity.getPk.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntity.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="soundentity_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="soundentity_seq", sequenceName="soundentity_seq", initialValue=1, allocationSize=1)
    //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntity.getPk.Long:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntity.getPk.Long:DA-ELSE
        return super.getPkInternal();
        //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntity.getPk.Long:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.SoundEntity.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.SoundEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.SoundEntity.additional.elements.in.type:DA-END
} // end of java type