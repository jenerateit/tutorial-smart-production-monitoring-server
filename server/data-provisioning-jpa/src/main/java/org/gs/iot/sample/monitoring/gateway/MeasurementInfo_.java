package org.gs.iot.sample.monitoring.gateway;

import java.util.Date;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;


/**
 * JPA Metamodel description for {@link org.gs.iot.sample.monitoring.gateway.MeasurementInfo}.
 *
 * @see org.gs.iot.sample.monitoring.gateway.MeasurementInfo
 */
@StaticMetamodel(value=org.gs.iot.sample.monitoring.gateway.MeasurementInfo.class)
public class MeasurementInfo_ { // start of class

    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.monitoring.gateway.MeasurementInfo#getTimeOfMeasurement()}.
     *
     * @see org.gs.iot.sample.monitoring.gateway.MeasurementInfo#getTimeOfMeasurement()
     */
    public static volatile SingularAttribute<MeasurementInfo, Date> timeOfMeasurement;
    
    /**
     * JPA Metamodel mapping for {@link org.gs.iot.sample.monitoring.gateway.MeasurementInfo#getTimeZoneOfMeasurement()}.
     *
     * @see org.gs.iot.sample.monitoring.gateway.MeasurementInfo#getTimeZoneOfMeasurement()
     */
    public static volatile SingularAttribute<MeasurementInfo, String> timeZoneOfMeasurement;
    
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.MeasurementInfo_.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.MeasurementInfo_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.MeasurementInfo_.additional.elements.in.type:DA-END
} // end of java type