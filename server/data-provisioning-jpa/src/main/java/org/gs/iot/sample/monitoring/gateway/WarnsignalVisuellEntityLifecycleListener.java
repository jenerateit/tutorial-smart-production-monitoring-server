package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;


public class WarnsignalVisuellEntityLifecycleListener { // start of class

    
    /**
     *
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(WarnsignalVisuellEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.preUpdate.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.preUpdate.WarnsignalVisuellEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.preUpdate.WarnsignalVisuellEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(WarnsignalVisuellEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.postUpdate.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.postUpdate.WarnsignalVisuellEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.postUpdate.WarnsignalVisuellEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(WarnsignalVisuellEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.prePersist.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.prePersist.WarnsignalVisuellEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.prePersist.WarnsignalVisuellEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(WarnsignalVisuellEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.postPersist.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.postPersist.WarnsignalVisuellEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.postPersist.WarnsignalVisuellEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(WarnsignalVisuellEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.preRemove.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.preRemove.WarnsignalVisuellEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.preRemove.WarnsignalVisuellEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(WarnsignalVisuellEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.postRemove.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.postRemove.WarnsignalVisuellEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.postRemove.WarnsignalVisuellEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(WarnsignalVisuellEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.postLoad.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.postLoad.WarnsignalVisuellEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.postLoad.WarnsignalVisuellEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type