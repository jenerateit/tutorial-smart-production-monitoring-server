package org.gs.iot.sample.monitoring.gateway;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Embeddable
public class MeasurementInfo implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private Date timeOfMeasurement;
    
    private String timeZoneOfMeasurement;
    
    
    /**
     * getter for the field timeOfMeasurement
     *
     *
     *
     * @return
     */
    @Basic
    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="timeofmeasurement", unique=false, nullable=true)
    public Date getTimeOfMeasurement() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.getTimeOfMeasurement.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.getTimeOfMeasurement.Date:DA-ELSE
        return this.timeOfMeasurement;
        //DA-END:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.getTimeOfMeasurement.Date:DA-END
    }
    
    /**
     * setter for the field timeOfMeasurement
     *
     *
     *
     * @param timeOfMeasurement  the timeOfMeasurement
     */
    public void setTimeOfMeasurement(Date timeOfMeasurement) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.setTimeOfMeasurement.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.setTimeOfMeasurement.Date:DA-ELSE
        this.timeOfMeasurement = timeOfMeasurement;
        //DA-END:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.setTimeOfMeasurement.Date:DA-END
    }
    
    /**
     * getter for the field timeZoneOfMeasurement
     *
     *
     *
     * @return
     */
    @Basic
    @Column(name="timezoneofmeasurement", unique=false, nullable=true)
    public String getTimeZoneOfMeasurement() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.getTimeZoneOfMeasurement.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.getTimeZoneOfMeasurement.String:DA-ELSE
        return this.timeZoneOfMeasurement;
        //DA-END:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.getTimeZoneOfMeasurement.String:DA-END
    }
    
    /**
     * setter for the field timeZoneOfMeasurement
     *
     *
     *
     * @param timeZoneOfMeasurement  the timeZoneOfMeasurement
     */
    public void setTimeZoneOfMeasurement(String timeZoneOfMeasurement) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.setTimeZoneOfMeasurement.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.setTimeZoneOfMeasurement.String:DA-ELSE
        this.timeZoneOfMeasurement = timeZoneOfMeasurement;
        //DA-END:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.setTimeZoneOfMeasurement.String:DA-END
    }
    
    /**
     *
     * @return
     */
    public int hashCode() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.hashCode.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.hashCode.int:DA-ELSE
        return 0;
        //DA-END:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.hashCode.int:DA-END
    }
    
    /**
     *
     * @param arg0  the arg0
     * @return
     */
    public boolean equals(Object arg0) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.equals.Object.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.equals.Object.boolean:DA-ELSE
        return true;
        //DA-END:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.equals.Object.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.MeasurementInfo.additional.elements.in.type:DA-END
} // end of java type