package org.gs.iot.sample.monitoring.gateway;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;


public class WarnsignalAkustischEntityLifecycleListener { // start of class

    
    /**
     *
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(WarnsignalAkustischEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.preUpdate.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.preUpdate.WarnsignalAkustischEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.preUpdate.WarnsignalAkustischEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(WarnsignalAkustischEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.postUpdate.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.postUpdate.WarnsignalAkustischEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.postUpdate.WarnsignalAkustischEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(WarnsignalAkustischEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.prePersist.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.prePersist.WarnsignalAkustischEntity:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.prePersist.WarnsignalAkustischEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(WarnsignalAkustischEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.postPersist.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.postPersist.WarnsignalAkustischEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.postPersist.WarnsignalAkustischEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(WarnsignalAkustischEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.preRemove.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.preRemove.WarnsignalAkustischEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.preRemove.WarnsignalAkustischEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(WarnsignalAkustischEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.postRemove.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.postRemove.WarnsignalAkustischEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.postRemove.WarnsignalAkustischEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(WarnsignalAkustischEntity entity) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.postLoad.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.postLoad.WarnsignalAkustischEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.postLoad.WarnsignalAkustischEntity:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type