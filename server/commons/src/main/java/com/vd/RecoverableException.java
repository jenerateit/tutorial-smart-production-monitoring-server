/**
 * 
 */
package com.vd;

import javax.ejb.ApplicationException;

/**
 * This exception represents an exception where the web ui needs to display
 * error messages on the current page only to inform the user that an internal error has occurred.
 * This exception does not indicate a complete system failure but only
 * a problem that affects the current page. Other pages of the web application 
 * can still be used (in contrast to UnrecoverableException).
 * 
 * @author mmt
 *
 */
@ApplicationException(rollback=true)
public class RecoverableException extends AbstractWebException {

	private static final long serialVersionUID = 5098752469959045430L;
	

	/**
	 * @param message
	 */
	public RecoverableException(String message) {
		super(message);
	}
	
	/**
	 * @param message
	 * @param businessException
	 */
	public RecoverableException(String message, BusinessException businessException) {
		super(message, businessException);
	}


	/**
	 * @param cause
	 */
	public RecoverableException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public RecoverableException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public RecoverableException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
