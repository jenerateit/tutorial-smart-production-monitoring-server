/**
 * 
 */
package com.vd;

import java.util.HashMap;
import java.util.Map;

/**
 * Hier werden die möglichen Fehler aufgezählt, die im TicketOffice Webservice auftreten können.
 * Die Status-Nummer steht im Feld "status" des Webservice-Typs "TOWSStatus".
 * 
 * @author mmt
 *
 */
public enum BusinessException {
	
	OK (0, "ecOK"),
	UNBEKANNTER_KUNDE (1, "ecUnbekKunde"),
	SESSION_UNBEKANNT (2, "ecSessionUnbek"),
	SESSION_ABGELAUFEN (3, "ecSessionAbgelaufen", true),  // critical since you cannot make any service calls without a valid session id
	UNBEKANNTE_PERSON (4, "ecUnbekPerson"),
	UNBEKANNTER_KVP (5, "ecUnbekKVP"),
	UNBEKANNTER_KONTOINHABER (6, "ecUnbekKtoInh"),
	INTERNER_FEHLER (7, "ecInternerFehler", true),
	BILD_EXISTIERT_NICHT (8, "ecBildExistiertNicht"),
	KEIN_BILD_HINTERLEGT (9, "ecKeinBildHinterlegt"),
	FEHLER_ERSTELLUNG_BILDPFAD (10, "ecFehlerErstellungBildPfad"),
	FEHLER_BILD_SPEICHERN (11, "ecFehlerBildSpeichern"),
	NICHT_UNTERSTUETZTES_DATENFORMAT (12, "ecNichtUnterstuetztesDatenFormat"),
	KEINE_BILDDATEN (13, "ecKeineBilddaten"),
	ANMELDUNG_FEHLGESCHLAGEN (14, "ecAnmeldungFehlgeschlagen", true),
	UNBEKANNTER_FKI (15, "ecUnbekFKI"),
	UNBEKANNTES_SUCHKRITERIUM (16, "ecUnbekSuchkrit"),
	STARTORT_NICHT_EINDEUTIG (17, "ecStartOrtNichtEindeutig"),
	ZIELORT_NICHT_EINDEUTIG (18, "ecZielOrtNichtEindeutig"),
	STARTORT_NICHT_GEFUNDEN (19, "ecStartOrtNichtGefunden"),
	ZIELORT_NICHT_GEFUNDEN (20, "ecZielOrtNichtGefunden"),
	KEINE_RELATION (21, "ecKeineRelation"),
	NICHT_UNTERSTUETZTE_AUSDRUCKSART (22, "ecNichtUnterstutzteAusdruckArt"),
	KTO_NEGATIV_LISTE (23, "ecKTONegativListe"),
	/**
	 * TODO was bedeutet dies? besseren Namen hier nutzen
	 */
	BER_GUELT (24, "ecBerGuelt"),
	UNBEKANNTE_APP_INSTANZ (25, "ecUnbekAppInstanz"),
	VERKAUF_BEREITS_STORNIERT (26, "ecVerkaufBereitsStorniert"),
	STORNO_ZEITRAUM_ABGELAUFEN (27, "ecStornoZeitraumAbgelaufen"),
	VERKAUFS_DATENSATZ_UNBEKANNT (28, "ecVerkaufsDatensatzUnbek"),
	VERKAUF_NICHT_UEBER_BES (29, "ecVerkaufNichtUeberBES"),
	VERKAUF_BEREITS_VERBUCHT (30, "ecVerkaufBereitsVerbucht"),
	TARIFMODUS_NICHT_UNTERSTUETZT (31, "ecTarifmodusNichtUnterstuetzt"),
	RELATIONS_ANFRAGE_ORTE_UNGUELTIG (32, "ecRelationsAnfrageOrteUngueltig"),
	UNGUELTIGES_FREI_AB_DATUM (33, "ecUngueltigesFreiAbDatum"),
	FEHLER_BEI_BERECHTIGUNGSANLAGE (34, "ecFehlerBeiBerechtigungsanlage"),
	TARIFMODUS_NICHT_FUER_ERLAUBT (35, "ecTarifmodusNichtFuerErlaubt"),
	FEHLER_BEI_ERSTATTUNG (36, "ecFehlerBeiErstattung"),
	BERECHTIGUNG_NICHT_GEFUNDEN (37, "ecBerNichtGefunden"),
	RUECKNAHME_NICHT_DURCHGEFUEHRT (38, "ecRuecknahmeNichtDurchgefuehrt"),
	FEHLER_BEI_ERSATZKARTE (39, "ecFehlerBeiErsatzkarte"),
	FEHLER_BERECHTIGUNG_LOESCHEN (40, "ecFehlerBerLoeschen"),
	FEHLER_BERECHTIGUNG_KUENDIGEN (41, "ecFelherBerKuendigung"),
	LIZENZDATEI_UNGUELTIG (42, "ecLizenzdateiUngueltig"),
	FEHLER_BEI_TA_ABSCHLIESSEN (43, "ecFehlerbeiTAAbschliessen"),
	TA_EINES_ANDEREN_ANWENDERS (44, "ecTAEinesAnderenAnwender"),
	FEHLER_BEI_AUSDRUCK_ERSTELLEN (45, "ecFehlerBeiAusdruckErstellen"),
	TAGESABSCHLUSS_ABGESCHLOSSEN (46, "ecTagesabschlussAbgeschlossen"),
	FEHLER_KUNDE_SPEICHERN (47, "ecFehlerKundeSpeichern"),
	FEHLER_FKI_SPEICHERN (48, "ecFehlerFKISpeichern"),
	FEHLER_KTO_SPEICHERN (49, "ecFehlerKTOSpeichern"),
	BERECHTIGUNG_BEREITS_AUSGEGEBEN (50, "ecBerechtigungBereitsAusgegeben"),
	ZAHLUNG_UEBER_KASSEN_SST_NOCH_NICHT_DURCHGEFUEHRT (51, "ecZahlungUeberKassenSSTNochNichtDurchgefuehrt"),
	KASSEN_SST_DATEN_LOESCHEN (52, "ecKassenSSTDatenLoeschen"),
	BERECHTIGUNG_AUSGABE_UEBER_KSST (53, "ecBerechtigungAusgabeUeberKSST"),
	UNGUELTIGES_STARTDATUM (54, "ecUngueltigesStartdatum"),
	FEHLER_IM_RECHNUNGSLAUF (55, "ecFehlerImRechnungslauf"),
	GUELTIGKEIT_KONNTE_NICHT_ERMITTELT_WERDEN (56, "ecGueltigkeitKonnteNichtErmitteltWerden"),
	FEHLER_BEI_PREISERMITTLUNG (57, "ecFehlerBeiPreisermittlung"),
	FEHLER_BEI_ERMITTLUNG_DER_GUELTIGKEITS_REGELN (58, "ecFehlerBeiErmittlungDerGueltigkeitsRegeln"),
	SUCHERGEBNIS_NICHT_EINDEUTIG (59, "ecSuchergebnisNichtEindeutig"),
	IMPORT_DATEI_NICHT_VORHANDEN (60, "ecImportDateiNichtVorhanden"),
	;
	
	private final static Map<Integer, BusinessException> exceptionMap = new HashMap<>();
	
	static {
		for (BusinessException exception : values()) {
			if (exception.getStatus() != null) {
				exceptionMap.put(exception.getStatus(), exception);
			}
		}
	}
	
	private final Integer status;
	
	private final String delphiEnumName;
	
	private final boolean fatalError;
	
	private BusinessException(Integer status, String delphiEnumName, boolean fatalError) {
		this.status = status;
		this.delphiEnumName = delphiEnumName;
		this.fatalError = fatalError;
	}
	
	private BusinessException(Integer status, String delphiEnumName) {
		this.status = status;
		this.delphiEnumName = delphiEnumName;
		this.fatalError = false;
	}

	public Integer getStatus() {
		return status;
	}

	public String getDelphiEnumName() {
		return delphiEnumName;
	}
	
	/**
	 * @param status
	 * @return business exception enum entry that relates to the given status number
	 */
	public static BusinessException getForStatus(Integer status) {
		return exceptionMap.get(status);
	}

	public boolean isFatalError() {
		return fatalError;
	}
}
