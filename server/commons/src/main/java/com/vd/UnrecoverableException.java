/**
 * 
 */
package com.vd;

import javax.ejb.ApplicationException;

/**
 * This exception represents an exception where the web ui needs to display
 * an error page to inform the user that an internal error has occurred.
 * The user should contact support to get information about how to proceed.
 * 
 * @author mmt
 *
 */
@ApplicationException(rollback=true)
public class UnrecoverableException extends AbstractWebException {

	private static final long serialVersionUID = 5098752469959045430L;
	
	/**
	 * @param message
	 */
	public UnrecoverableException(String message) {
		super(message);
	}
	
	/**
	 * @param message
	 * @param businessException
	 */
	public UnrecoverableException(String message, BusinessException businessException) {
		super(message, businessException);
	}

	/**
	 * @param cause
	 */
	public UnrecoverableException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnrecoverableException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnrecoverableException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
