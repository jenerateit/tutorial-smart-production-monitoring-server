/**
 * 
 */
package com.vd;

import javax.faces.application.FacesMessage.Severity;

/**
 * @author mmt
 *
 */
public interface MessageParamsI {

	Severity getSeverity();
	
	String getMessageKeyForSummary();
	
	String getMessageKeyForDetails();
	
	BusinessException getCause();
}
