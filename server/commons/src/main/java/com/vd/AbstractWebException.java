/**
 * 
 */
package com.vd;

import javax.ejb.ApplicationException;

/**
 * 
 * @author mmt
 *
 */
@ApplicationException(rollback=true)
public abstract class AbstractWebException extends RuntimeException {

	private static final long serialVersionUID = 5098752469959045430L;
	
	private final BusinessException businessException;
	

	/**
	 * @param message
	 */
	public AbstractWebException(String message) {
		super(message);
		this.businessException = null;
	}
	
	/**
	 * @param message
	 * @param businessException
	 */
	public AbstractWebException(String message, BusinessException businessException) {
		super(message);
		this.businessException = businessException;
	}

	/**
	 * @param cause
	 */
	public AbstractWebException(Throwable cause) {
		super(cause);
		this.businessException = null;
	}

	/**
	 * @param message
	 * @param cause
	 */
	public AbstractWebException(String message, Throwable cause) {
		super(message, cause);
		this.businessException = null;
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public AbstractWebException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.businessException = null;
	}

	public BusinessException getBusinessException() {
		return businessException;
	}
}
