package com.vd;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.ProjectStage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

public abstract class AbstractManagedBean implements Serializable {

	private static final long serialVersionUID = -3533968998823002481L;
	
	private SessionDataHolder sessionDataHolder;
	
	  /**
     * Centralized, common entry for the initialization of managed beans. By doing this
     * we get a centralized handling of exceptions that can occur during the initialization
     * of managed beans.
     * 
     */
    @PostConstruct
    public void initialize() {
    	try {
    		init();
    	} catch (RecoverableException ex) {
    		// in this case only the current page is affected - no invalidation or redirection is needed
    		ex.printStackTrace();
    		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();    		
    		try {
    			externalContext.redirect(externalContext.getRequestContextPath() + "/recoverableError.xhtml");
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} finally {
				responseComplete();
			}
    	} catch (Throwable th) {
    		th.printStackTrace();
    		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();    		
    		try {
    			externalContext.redirect(externalContext.getRequestContextPath() + "/error.xhtml");
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} finally {
				responseComplete();
			}
    	}
    }
    
    /**
     * reads data from an EJB or from a different source and provides this data to xhtml pages
     */
    public void init() {
    	if (getSessionDataHolder() == null) {
			setSessionDataHolder(new SessionDataHolder());
		}
    	
    	if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().containsKey("error")) {
   		    addMessage(FacesMessage.SEVERITY_ERROR, FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("error"), null);    		     		 
   	 	}
    }
    
    /**
     * Diese Methode hat eine ähnliche Aufgabe wie initialize(), nur wird initializeApplication()
     * von einem expliziten Managed Bean aus innerhalb eines View-Action-Listeners aufgerufen.
     * Es gibt fälle, in denen ein solcher Trigger für den Aufruf für die Initialisierung
     * von ManagedBeans sinnvoller ist (im Vergleich zu @PostConstruct). 
     * 
     * @return
     */
    protected final String initializeApplication() {
    	String result = null;
    	try {
    		initApplication();
    	} catch (RecoverableException ex) {
    		// in this case only the current page is affected - no invalidation or redirection is needed
    		ex.printStackTrace();
    		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();    		
   			result = externalContext.getRequestContextPath() + "/recoverableError.xhtml";
    	} catch (Throwable th) {
    		th.printStackTrace();
    		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();    		
            result = externalContext.getRequestContextPath() + "/error.xhtml";
    	}
    	
    	return result;
    }
    
    /**
     * Diese Methode wird von initializeApplication() aus aufgerufen und wird typischerweise in
     * einem Managed Bean überschrieben. Die Aufgabe von initApplication() ist ähnlich wie die
     * Aufgabe von init(). Weitere Informationen stehen ind er JavaDoc von initializeApplication().
     * 
     * @return
     */
    protected String initApplication() {
    	// do nothing by default
    	return null;
    }
    
    /**
	 * 
	 * @param key
	 * @return
	 */
	protected String getMessageProperty(String key) {
		return getMessageProperty(key, "messages");
	}
	
	/**
	 * @param key
	 * @param returnNullIfAbsent
	 * @return
	 */
	protected String getMessageProperty(String key, boolean returnNullIfAbsent) {
		return getMessageProperty(key, "messages", returnNullIfAbsent);
	}
	
	/**
	 * This is the default implementation for getting an internationalized label
	 * instead of using msg['...'] within an XHTML page. This method should be overwritten
	 * by the capability specific abstract managed bean.
	 * 
	 * @param key
	 * @return
	 */
	public String getMsg(String key) {
		return getMessageProperty(key);
	}
	
	protected String getMessageProperty(String key, String baseName) {
		return getMessageProperty(key, baseName, true);
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	protected String getMessageProperty(String key, String baseName, boolean returnNullIfAbsent) {
		
		Locale locale = null;
		
		if (FacesContext.getCurrentInstance().getViewRoot() != null) {
		    locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	    } else {
	    	locale = Locale.getDefault();  // it is better to show something in a different language as to show nothing at all
	    }
	    
		String value = null;
		
		// --- first try to get the value from overwrite resource bundles
		ResourceBundle bundleForOverwrite = null;
		try {
		    bundleForOverwrite = ResourceBundle.getBundle(baseName + "-overwrite", locale);
		    if (bundleForOverwrite != null) {
			    value = bundleForOverwrite.getString(key);
			}
		} catch (MissingResourceException ex) {
			// overwrite resource bundle not found - this is not an error
		}
		
		if (value == null || value.length() == 0) {
			// key value pair is not overwritten, so we are going to read the default
			ResourceBundle bundle = ResourceBundle.getBundle(baseName, locale);
			try {
			    value = bundle.getString(key);
			} catch (MissingResourceException ex) {
				if (!returnNullIfAbsent) value = "***" + key + "***";
			}
		}
		
		return value;
	}
	
	/**
	 * @param key
	 * @return
	 */
	protected String getValidationMessageProperty(String key) {
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		ResourceBundle bundle = ResourceBundle.getBundle("ValidationMessages", locale);
		String value = null;
		try {
			value = bundle.getString(key);
		} catch (MissingResourceException ex) {
			value = "???" + key + "???";
		}
		return value;
	}
	
    protected void addErrorMessage(String message, String messageDetails, Object ...messageParamsForDetails) {
		
		String detailsMessage = "";
		if (messageDetails != null && messageDetails.length() > 0) {
			detailsMessage = MessageFormat.format(messageDetails, messageParamsForDetails);
			if (detailsMessage.equals(messageDetails)) {
				// no replacement of message param has taken place => simply add the message param objects' toString() representation
				detailsMessage = appendMessageParams(detailsMessage, messageParamsForDetails);
			}
		}
		
		FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, message != null ? message : "", detailsMessage);
		
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
	}
    
    /**
     * Helper method to append message params' toString() representation to a given string.
     * 
     * @param originalString
     * @param messageParams
     * @return
     */
    private String appendMessageParams(String originalString, Object ...messageParams) {
    	String comma = "";
    	String result = originalString;
    	if (messageParams != null && messageParams.length > 0) {
			result = result + " (";
			for (Object messageParam : messageParams) {
			    if (messageParam != null) {
			    	result = result + comma + messageParam.toString();
			    	comma = " ,";
			    }
			}
			result = result + ")";
    	}
    	
    	return result;
    }

	/**
	 * @param severity
	 * @param messageKeyForSummary
	 * @param messageKeyForDetails
	 */
	protected void addMessage(FacesMessage.Severity severity, String messageKeyForSummary, String messageKeyForDetails, Object ...messageParamsForDetails) {
		
		String summaryMessage = null;
		String detailsMessage = null;

		if (messageKeyForSummary != null) {
			summaryMessage = getMessageProperty(messageKeyForSummary, false);
		}
		
		if (messageKeyForDetails != null) {
			detailsMessage = getMessageProperty(messageKeyForDetails, false);
			if (detailsMessage != null && detailsMessage.length() > 0 && messageParamsForDetails != null) {
				String formattedDetailsMessage = MessageFormat.format(detailsMessage, messageParamsForDetails);
				if (formattedDetailsMessage.equals(detailsMessage)) {
					// no replacement of message param has taken place => simply add the message param objects' toString() representation
					detailsMessage = appendMessageParams(detailsMessage, messageParamsForDetails);
				} else {
					detailsMessage = formattedDetailsMessage;
				}
			}
		} else {
			if (messageParamsForDetails != null) {
				summaryMessage = MessageFormat.format(summaryMessage, messageParamsForDetails);
			}
		}
		
		FacesMessage facesMessage = new FacesMessage(severity, messageKeyForSummary == null ? null : summaryMessage,
                messageKeyForDetails == null ? "" : detailsMessage);
		
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
	}
	
	/**
	 * @param messageParams
	 * @param messageParamsForDetails
	 */
	protected void addMessage(MessageParamsI messageParams, Object ...messageParamsForDetails) {
		if (messageParams != null) {
		    addMessage(messageParams.getSeverity(), messageParams.getMessageKeyForSummary(), messageParams.getMessageKeyForDetails(), messageParamsForDetails);
		} else {
			// simply ignore this, no messages given ==> no message is shown
		}
	}
	
	/**
	 * @param severity
	 * @param messageKeyForSummary
	 * @param messageKeyForDetails
	 */
	protected void addValidationMessage(FacesMessage.Severity severity, String messageKeyForSummary, String messageKeyForDetails) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(severity, getValidationMessageProperty(messageKeyForSummary), messageKeyForDetails != null && messageKeyForDetails.length() > 0 ? getValidationMessageProperty(messageKeyForDetails) : ""));
	}
	
	/**
	 * @return
	 */
	protected boolean isDevelopmentStage() {
		return FacesContext.getCurrentInstance().isProjectStage(ProjectStage.Development);
	}
	
	public String getTimeFormat() {
		SimpleDateFormat timeFormat = (SimpleDateFormat) SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT);
		return timeFormat.toPattern();
	}
	
	public String getDateFormat() {
		@SuppressWarnings("unused")
		SimpleDateFormat dateFormat = (SimpleDateFormat) SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT);
//		return dateFormat.toPattern();
		return "dd.MM.yyyy";
	}
	
	public String getDateTimeFormat() {
		SimpleDateFormat dateTimeFormat = (SimpleDateFormat) SimpleDateFormat.getDateInstance(SimpleDateFormat.FULL);
		return dateTimeFormat.toPattern();
	}
	
	public String getDateTimeFormatLocalized() {
    	SimpleDateFormat localizedDateTimeFormat = (SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, getLocale());
		return localizedDateTimeFormat.toPattern();
    }
	
	public SimpleDateFormat getDateTimeFormatObjectLocalized() {
		return getDateTimeFormatObjectLocalized(false);
	}
	
	public SimpleDateFormat getDateTimeFormatObjectLocalized(boolean fourDigitsForYear) {
		SimpleDateFormat result = (SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, getLocale());
		if (fourDigitsForYear) {
			String pattern = result.toPattern().replaceAll("y+","yyyy");
		    result.applyPattern(pattern);
		}
		return result;
	}
	
	public SimpleDateFormat getDateFormatObjectLocalized() {
		return getDateFormatObjectLocalized(false);
	}
	
	public SimpleDateFormat getDateFormatObjectLocalized(boolean fourDigitsForYear) {
		SimpleDateFormat result = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT, getLocale());
		if (fourDigitsForYear) {
			String pattern = result.toPattern().replaceAll("y+","yyyy");
		    result.applyPattern(pattern);
		}
		
		return result;
	}
	
	public SimpleDateFormat getDateFormatLongObjectLocalized() {
		return (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.LONG, getLocale());
	}
	
	public SimpleDateFormat getDateFormatLongObjectLocalized(boolean fourDigitsForYear) {
		SimpleDateFormat result = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.LONG, getLocale());
		if (fourDigitsForYear) {
			String pattern = result.toPattern().replaceAll("y+","yyyy");
		    result.applyPattern(pattern);
		}
		
		return result;
	}
	
	/**
	 * @param dayInMonth a day that nominates the month in question, if this is null, the current day is being used instead
	 * @return
	 */
	protected Date getFirstDayOfMonth(Date dayInMonth) {
		GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
		
		if (dayInMonth != null) {
			cal.setTime(dayInMonth);
		}
				
	    cal.set(Calendar.DAY_OF_MONTH, 1);
	    return cal.getTime(); 
	}

	public SessionDataHolder getSessionDataHolder() {
		return sessionDataHolder;
	}

	public void setSessionDataHolder(SessionDataHolder sessionDataHolder) {
		this.sessionDataHolder = sessionDataHolder;
	}
	
	/**
	 * Delegates to faces context's responseComplete(). This method
	 * provides a central place to add additional logic that needs to be executed
	 * whenever a responseComplete() is going to be made.
	 */
	public void responseComplete() {
		FacesContext.getCurrentInstance().responseComplete();
	}
	
	/**
	 * Simple helper method in order to get shorter names for method calls
	 * @return
	 */
	protected String getViewId() {
		return FacesContext.getCurrentInstance().getViewRoot() == null ? null : FacesContext.getCurrentInstance().getViewRoot().getViewId();
	}
	
	/**
	 * @return
	 */
	public Locale getLocale() {
		return FacesContext.getCurrentInstance().getViewRoot().getLocale();
	}
	
	public Locale getRequestLocale() {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
	}
	
	/**
     * this method can get called when you define an event inside a f:metadata tag, e.g.: <f:event type="preRenderView" listener="#{myBean.initialize}"/>
     * 
     * @param event  the event
     */
    public void preRenderViewListener(ComponentSystemEvent event) {}
}
