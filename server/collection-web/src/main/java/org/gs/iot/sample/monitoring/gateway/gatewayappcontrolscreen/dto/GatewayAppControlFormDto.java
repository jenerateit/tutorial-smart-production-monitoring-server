package org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto;


import java.io.Serializable;

import com.vd.AbstractDto;

public class GatewayAppControlFormDto extends AbstractDto implements Serializable { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.serialVersionUID:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.serialVersionUID:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.serialVersionUID:DA-END
    private static final long serialVersionUID = 1L;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.dataProvisioningHost:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.dataProvisioningHost:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.dataProvisioningHost:DA-END
    private String dataProvisioningHost;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.dataProvisioningContextRoot:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.dataProvisioningContextRoot:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.dataProvisioningContextRoot:DA-END
    private String dataProvisioningContextRoot;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.dataProvisioningApplicationPath:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.dataProvisioningApplicationPath:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.dataProvisioningApplicationPath:DA-END
    private String dataProvisioningApplicationPath;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.mqttBrokerHostnameOrIp:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.mqttBrokerHostnameOrIp:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.mqttBrokerHostnameOrIp:DA-END
    private String mqttBrokerHostnameOrIp;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.mqttBrokerPort:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.mqttBrokerPort:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.mqttBrokerPort:DA-END
    private String mqttBrokerPort;
    
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.fields:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.fields:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.fields:DA-END
    /**
     * creates an instance of GatewayAppControlFormDto
     */
    public GatewayAppControlFormDto() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto:DA-END
    }
    
    
    /**
     *
     * @return
     */
    @Override
    public boolean isDirty() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.isDirty.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.isDirty.boolean:DA-ELSE
        boolean result = super.isDirty();
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.isDirty.boolean:DA-END
    }
    
    /**
     */
    public void clearAllDirty() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.clearAllDirty:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.clearAllDirty:DA-ELSE
        super.setDirty(false);
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.clearAllDirty:DA-END
    }
    
    /**
     * setter for the field dataProvisioningHost
     *
     *
     *
     * @param dataProvisioningHost  the dataProvisioningHost
     */
    public void setDataProvisioningHost(String dataProvisioningHost) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setDataProvisioningHost.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setDataProvisioningHost.String:DA-ELSE
        if (isDifferent(this.dataProvisioningHost, dataProvisioningHost)) {
            this.setDirty(true);
            this.dataProvisioningHost = dataProvisioningHost;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setDataProvisioningHost.String:DA-END
    }
    
    /**
     * getter for the field dataProvisioningHost
     *
     *
     *
     * @return
     */
    public String getDataProvisioningHost() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getDataProvisioningHost.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getDataProvisioningHost.String:DA-ELSE
        return this.dataProvisioningHost;
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getDataProvisioningHost.String:DA-END
    }
    
    /**
     * setter for the field dataProvisioningContextRoot
     *
     *
     *
     * @param dataProvisioningContextRoot  the dataProvisioningContextRoot
     */
    public void setDataProvisioningContextRoot(String dataProvisioningContextRoot) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setDataProvisioningContextRoot.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setDataProvisioningContextRoot.String:DA-ELSE
        if (isDifferent(this.dataProvisioningContextRoot, dataProvisioningContextRoot)) {
            this.setDirty(true);
            this.dataProvisioningContextRoot = dataProvisioningContextRoot;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setDataProvisioningContextRoot.String:DA-END
    }
    
    /**
     * getter for the field dataProvisioningContextRoot
     *
     *
     *
     * @return
     */
    public String getDataProvisioningContextRoot() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getDataProvisioningContextRoot.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getDataProvisioningContextRoot.String:DA-ELSE
        return this.dataProvisioningContextRoot;
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getDataProvisioningContextRoot.String:DA-END
    }
    
    /**
     * setter for the field dataProvisioningApplicationPath
     *
     *
     *
     * @param dataProvisioningApplicationPath  the dataProvisioningApplicationPath
     */
    public void setDataProvisioningApplicationPath(String dataProvisioningApplicationPath) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setDataProvisioningApplicationPath.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setDataProvisioningApplicationPath.String:DA-ELSE
        if (isDifferent(this.dataProvisioningApplicationPath, dataProvisioningApplicationPath)) {
            this.setDirty(true);
            this.dataProvisioningApplicationPath = dataProvisioningApplicationPath;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setDataProvisioningApplicationPath.String:DA-END
    }
    
    /**
     * getter for the field dataProvisioningApplicationPath
     *
     *
     *
     * @return
     */
    public String getDataProvisioningApplicationPath() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getDataProvisioningApplicationPath.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getDataProvisioningApplicationPath.String:DA-ELSE
        return this.dataProvisioningApplicationPath;
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getDataProvisioningApplicationPath.String:DA-END
    }
    
    /**
     * setter for the field mqttBrokerHostnameOrIp
     *
     *
     *
     * @param mqttBrokerHostnameOrIp  the mqttBrokerHostnameOrIp
     */
    public void setMqttBrokerHostnameOrIp(String mqttBrokerHostnameOrIp) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setMqttBrokerHostnameOrIp.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setMqttBrokerHostnameOrIp.String:DA-ELSE
        if (isDifferent(this.mqttBrokerHostnameOrIp, mqttBrokerHostnameOrIp)) {
            this.setDirty(true);
            this.mqttBrokerHostnameOrIp = mqttBrokerHostnameOrIp;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setMqttBrokerHostnameOrIp.String:DA-END
    }
    
    /**
     * getter for the field mqttBrokerHostnameOrIp
     *
     *
     *
     * @return
     */
    public String getMqttBrokerHostnameOrIp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getMqttBrokerHostnameOrIp.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getMqttBrokerHostnameOrIp.String:DA-ELSE
        return this.mqttBrokerHostnameOrIp;
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getMqttBrokerHostnameOrIp.String:DA-END
    }
    
    /**
     * setter for the field mqttBrokerPort
     *
     *
     *
     * @param mqttBrokerPort  the mqttBrokerPort
     */
    public void setMqttBrokerPort(String mqttBrokerPort) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setMqttBrokerPort.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setMqttBrokerPort.String:DA-ELSE
        if (isDifferent(this.mqttBrokerPort, mqttBrokerPort)) {
            this.setDirty(true);
            this.mqttBrokerPort = mqttBrokerPort;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.setMqttBrokerPort.String:DA-END
    }
    
    /**
     * getter for the field mqttBrokerPort
     *
     *
     *
     * @return
     */
    public String getMqttBrokerPort() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getMqttBrokerPort.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getMqttBrokerPort.String:DA-ELSE
        return this.mqttBrokerPort;
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.getMqttBrokerPort.String:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.methods:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.methods:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.methods:DA-END
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlFormDto.additional.elements.in.type:DA-END
} // end of java type