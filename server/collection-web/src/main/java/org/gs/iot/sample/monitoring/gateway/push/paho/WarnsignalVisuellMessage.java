package org.gs.iot.sample.monitoring.gateway.push.paho;


import java.util.Date;
import java.util.logging.Logger;

public class WarnsignalVisuellMessage { // start of class

    private static Logger LOGGER = java.util.logging.Logger.getLogger(WarnsignalVisuellMessage.class.getName());
    
    private final String topic;
    
    private final String appId;
    
    private final String clientId;
    
    private String resourceId;
    
    private String content;
    
    private int qos;
    
    private boolean retained;
    
    private boolean duplicate;
    
    private final Date messageArrival;
    
    /**
     * creates an instance of WarnsignalVisuellMessage
     *
     * @param topic  the topic
     * @param appId  the appId
     * @param clientId  the clientId
     * @param messageArrival  the messageArrival
     */
    public WarnsignalVisuellMessage(String topic, String appId, String clientId, Date messageArrival) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.String.String.String.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.String.String.String.Date:DA-ELSE
        this.topic = topic;
        this.appId = appId;
        this.clientId = clientId;
        this.messageArrival = messageArrival;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.String.String.String.Date:DA-END
    }
    
    
    /**
     * getter for the field topic
     *
     *
     *
     * @return
     */
    public String getTopic() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getTopic.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getTopic.String:DA-ELSE
        return this.topic;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getTopic.String:DA-END
    }
    
    /**
     * getter for the field appId
     *
     *
     *
     * @return
     */
    public String getAppId() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getAppId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getAppId.String:DA-ELSE
        return this.appId;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getAppId.String:DA-END
    }
    
    /**
     * getter for the field clientId
     *
     *
     *
     * @return
     */
    public String getClientId() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getClientId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getClientId.String:DA-ELSE
        return this.clientId;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getClientId.String:DA-END
    }
    
    /**
     * getter for the field resourceId
     *
     *
     *
     * @return
     */
    public String getResourceId() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getResourceId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getResourceId.String:DA-ELSE
        return this.resourceId;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getResourceId.String:DA-END
    }
    
    /**
     * setter for the field resourceId
     *
     *
     *
     * @param resourceId  the resourceId
     */
    public void setResourceId(String resourceId) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setResourceId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setResourceId.String:DA-ELSE
        this.resourceId = resourceId;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setResourceId.String:DA-END
    }
    
    /**
     * getter for the field content
     *
     *
     *
     * @return
     */
    public String getContent() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getContent.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getContent.String:DA-ELSE
        return this.content;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getContent.String:DA-END
    }
    
    /**
     * setter for the field content
     *
     *
     *
     * @param content  the content
     */
    public void setContent(String content) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setContent.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setContent.String:DA-ELSE
        this.content = content;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setContent.String:DA-END
    }
    
    /**
     * getter for the field qos
     *
     *
     *
     * @return
     */
    public int getQos() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getQos.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getQos.int:DA-ELSE
        return this.qos;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getQos.int:DA-END
    }
    
    /**
     * setter for the field qos
     *
     *
     *
     * @param qos  the qos
     */
    public void setQos(int qos) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setQos.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setQos.int:DA-ELSE
        this.qos = qos;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setQos.int:DA-END
    }
    
    /**
     * getter for the field retained
     *
     *
     *
     * @return
     */
    public boolean isRetained() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.isRetained.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.isRetained.boolean:DA-ELSE
        return this.retained;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.isRetained.boolean:DA-END
    }
    
    /**
     * setter for the field retained
     *
     *
     *
     * @param retained  the retained
     */
    public void setRetained(boolean retained) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setRetained.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setRetained.boolean:DA-ELSE
        this.retained = retained;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setRetained.boolean:DA-END
    }
    
    /**
     * getter for the field duplicate
     *
     *
     *
     * @return
     */
    public boolean isDuplicate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.isDuplicate.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.isDuplicate.boolean:DA-ELSE
        return this.duplicate;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.isDuplicate.boolean:DA-END
    }
    
    /**
     * setter for the field duplicate
     *
     *
     *
     * @param duplicate  the duplicate
     */
    public void setDuplicate(boolean duplicate) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setDuplicate.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setDuplicate.boolean:DA-ELSE
        this.duplicate = duplicate;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.setDuplicate.boolean:DA-END
    }
    
    /**
     * getter for the field messageArrival
     *
     *
     *
     * @return
     */
    public Date getMessageArrival() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getMessageArrival.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getMessageArrival.Date:DA-ELSE
        return this.messageArrival;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.getMessageArrival.Date:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessage.additional.elements.in.type:DA-END
} // end of java type