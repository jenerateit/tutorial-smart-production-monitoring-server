package org.gs.iot.sample.monitoring.gateway.push.paho;


import java.util.Date;
import java.util.logging.Logger;

public class IlluminanceMessage { // start of class

    private static Logger LOGGER = java.util.logging.Logger.getLogger(IlluminanceMessage.class.getName());
    
    private final String topic;
    
    private final String appId;
    
    private final String clientId;
    
    private String resourceId;
    
    private String content;
    
    private int qos;
    
    private boolean retained;
    
    private boolean duplicate;
    
    private final Date messageArrival;
    
    /**
     * creates an instance of IlluminanceMessage
     *
     * @param topic  the topic
     * @param appId  the appId
     * @param clientId  the clientId
     * @param messageArrival  the messageArrival
     */
    public IlluminanceMessage(String topic, String appId, String clientId, Date messageArrival) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.String.String.String.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.String.String.String.Date:DA-ELSE
        this.topic = topic;
        this.appId = appId;
        this.clientId = clientId;
        this.messageArrival = messageArrival;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.String.String.String.Date:DA-END
    }
    
    
    /**
     * getter for the field topic
     *
     *
     *
     * @return
     */
    public String getTopic() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getTopic.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getTopic.String:DA-ELSE
        return this.topic;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getTopic.String:DA-END
    }
    
    /**
     * getter for the field appId
     *
     *
     *
     * @return
     */
    public String getAppId() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getAppId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getAppId.String:DA-ELSE
        return this.appId;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getAppId.String:DA-END
    }
    
    /**
     * getter for the field clientId
     *
     *
     *
     * @return
     */
    public String getClientId() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getClientId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getClientId.String:DA-ELSE
        return this.clientId;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getClientId.String:DA-END
    }
    
    /**
     * getter for the field resourceId
     *
     *
     *
     * @return
     */
    public String getResourceId() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getResourceId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getResourceId.String:DA-ELSE
        return this.resourceId;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getResourceId.String:DA-END
    }
    
    /**
     * setter for the field resourceId
     *
     *
     *
     * @param resourceId  the resourceId
     */
    public void setResourceId(String resourceId) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setResourceId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setResourceId.String:DA-ELSE
        this.resourceId = resourceId;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setResourceId.String:DA-END
    }
    
    /**
     * getter for the field content
     *
     *
     *
     * @return
     */
    public String getContent() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getContent.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getContent.String:DA-ELSE
        return this.content;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getContent.String:DA-END
    }
    
    /**
     * setter for the field content
     *
     *
     *
     * @param content  the content
     */
    public void setContent(String content) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setContent.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setContent.String:DA-ELSE
        this.content = content;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setContent.String:DA-END
    }
    
    /**
     * getter for the field qos
     *
     *
     *
     * @return
     */
    public int getQos() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getQos.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getQos.int:DA-ELSE
        return this.qos;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getQos.int:DA-END
    }
    
    /**
     * setter for the field qos
     *
     *
     *
     * @param qos  the qos
     */
    public void setQos(int qos) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setQos.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setQos.int:DA-ELSE
        this.qos = qos;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setQos.int:DA-END
    }
    
    /**
     * getter for the field retained
     *
     *
     *
     * @return
     */
    public boolean isRetained() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.isRetained.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.isRetained.boolean:DA-ELSE
        return this.retained;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.isRetained.boolean:DA-END
    }
    
    /**
     * setter for the field retained
     *
     *
     *
     * @param retained  the retained
     */
    public void setRetained(boolean retained) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setRetained.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setRetained.boolean:DA-ELSE
        this.retained = retained;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setRetained.boolean:DA-END
    }
    
    /**
     * getter for the field duplicate
     *
     *
     *
     * @return
     */
    public boolean isDuplicate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.isDuplicate.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.isDuplicate.boolean:DA-ELSE
        return this.duplicate;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.isDuplicate.boolean:DA-END
    }
    
    /**
     * setter for the field duplicate
     *
     *
     *
     * @param duplicate  the duplicate
     */
    public void setDuplicate(boolean duplicate) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setDuplicate.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setDuplicate.boolean:DA-ELSE
        this.duplicate = duplicate;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.setDuplicate.boolean:DA-END
    }
    
    /**
     * getter for the field messageArrival
     *
     *
     *
     * @return
     */
    public Date getMessageArrival() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getMessageArrival.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getMessageArrival.Date:DA-ELSE
        return this.messageArrival;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.getMessageArrival.Date:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessage.additional.elements.in.type:DA-END
} // end of java type