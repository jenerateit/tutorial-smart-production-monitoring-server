package org.gs.iot.sample.monitoring.gateway.push.paho;


import java.util.logging.Logger;

import org.primefaces.push.EventBus;
import org.primefaces.push.RemoteEndpoint;
import org.primefaces.push.annotation.OnClose;
import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.OnOpen;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.annotation.Singleton;

@PushEndpoint(value="/illuminance/message")
@Singleton
public class IlluminanceMessageResource { // start of class

    private static Logger LOGGER = java.util.logging.Logger.getLogger(IlluminanceMessageResource.class.getName());
    
    
    /**
     *
     * @param remoteEndpoint  the remoteEndpoint
     * @param eventBus  the eventBus
     */
    @OnOpen
    public void onOpen(RemoteEndpoint remoteEndpoint, EventBus eventBus) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessageResource.onOpen.RemoteEndpoint.EventBus:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessageResource.onOpen.RemoteEndpoint.EventBus:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessageResource.onOpen.RemoteEndpoint.EventBus:DA-END
    }
    
    /**
     *
     * @param remoteEndpoint  the remoteEndpoint
     * @param eventBus  the eventBus
     */
    @OnClose
    public void onClose(RemoteEndpoint remoteEndpoint, EventBus eventBus) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessageResource.onClose.RemoteEndpoint.EventBus:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessageResource.onClose.RemoteEndpoint.EventBus:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessageResource.onClose.RemoteEndpoint.EventBus:DA-END
    }
    
    /**
     *
     * @param message  the message
     * @return
     */
    @OnMessage(encoders={org.primefaces.push.impl.JSONEncoder.class})
    public IlluminanceMessage onMessage(IlluminanceMessage message) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessageResource.onMessage.IlluminanceMessage.IlluminanceMessage:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessageResource.onMessage.IlluminanceMessage.IlluminanceMessage:DA-ELSE
        return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessageResource.onMessage.IlluminanceMessage.IlluminanceMessage:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessageResource.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessageResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.IlluminanceMessageResource.additional.elements.in.type:DA-END
} // end of java type