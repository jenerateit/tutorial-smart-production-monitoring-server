package org.gs.iot.sample.monitoring.gateway.paho;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean;
import org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityResourceService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//DA-START:paho.WarnsignalVisuellPahoClientBeanMessageProcessor:DA-START
//DA-ELSE:paho.WarnsignalVisuellPahoClientBeanMessageProcessor:DA-ELSE
@Stateless
//DA-END:paho.WarnsignalVisuellPahoClientBeanMessageProcessor:DA-END

public class WarnsignalVisuellPahoClientBeanMessageProcessor { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.LOGGER:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.LOGGER:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.LOGGER:DA-END
    private static Logger LOGGER = java.util.logging.Logger.getLogger(WarnsignalVisuellPahoClientBeanMessageProcessor.class.getName());
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.configurationBean:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.configurationBean:DA-ELSE
    @EJB
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.configurationBean:DA-END
    private GatewayAppConfigurationEjb configurationBean;
    
    /**
     * creates an instance of WarnsignalVisuellPahoClientBeanMessageProcessor
     */
    public WarnsignalVisuellPahoClientBeanMessageProcessor() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor:DA-END
    }
    
    
    /**
     * getter for the field configurationBean
     *
     *
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.getConfigurationBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.getConfigurationBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.getConfigurationBean.annotations:DA-END
    public GatewayAppConfigurationEjb getConfigurationBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.getConfigurationBean.GatewayAppConfigurationEjb:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.getConfigurationBean.GatewayAppConfigurationEjb:DA-ELSE
        return this.configurationBean;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.getConfigurationBean.GatewayAppConfigurationEjb:DA-END
    }
    
    /**
     * setter for the field configurationBean
     *
     *
     *
     * @param configurationBean  the configurationBean
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.setConfigurationBean.GatewayAppConfigurationEjb.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.setConfigurationBean.GatewayAppConfigurationEjb.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.setConfigurationBean.GatewayAppConfigurationEjb.annotations:DA-END
    public void setConfigurationBean(GatewayAppConfigurationEjb configurationBean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.setConfigurationBean.GatewayAppConfigurationEjb:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.setConfigurationBean.GatewayAppConfigurationEjb:DA-ELSE
        this.configurationBean = configurationBean;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.setConfigurationBean.GatewayAppConfigurationEjb:DA-END
    }
    
    /**
     *
     * @param topic  the topic
     * @param message  the message
     * @param clientId  the clientId
     * @param appId  the appId
     * @param messageArrival  the messageArrival
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.processMessage.String.MqttMessage.String.String.Date.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.processMessage.String.MqttMessage.String.String.Date.annotations:DA-ELSE
    @Asynchronous
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.processMessage.String.MqttMessage.String.String.Date.annotations:DA-END
    public Future<WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult> processMessage(String topic, MqttMessage message, String clientId, String appId, Date messageArrival) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.processMessage.String.MqttMessage.String.String.Date.Future:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.processMessage.String.MqttMessage.String.String.Date.Future:DA-ELSE
        LOGGER.info("processMessage(), topic:" + topic + ", message:" + message + ", clientId:" + clientId + ", appId:" + appId + ", messageArrival:" + messageArrival);
        
        String json = new String(message.getPayload(), StandardCharsets.UTF_8);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssX").create();
        WarnsignalVisuellEntityBean bean = gson.fromJson(json, WarnsignalVisuellEntityBean.class);
        bean.setClientId(clientId);
        bean.setAppId(appId);
        bean.setResourceId(topic);  // TODO set real resourceId instead of topic
        bean.setTimeOfMessageReception(messageArrival);
        
        String baseUrl = configurationBean.getDataProvisioningHost() + "/" + configurationBean.getDataProvisioningContextRoot() + "/" + configurationBean.getDataProvisioningApplicationPath() + "/";
        Retrofit retrofit = new Retrofit.Builder()
        	    .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
        	    .build();
        
        WarnsignalVisuellEntityResourceService service = retrofit.create(WarnsignalVisuellEntityResourceService.class);
        Call<WarnsignalVisuellEntityBean> call = service.create(bean);
        try {
        	Response<WarnsignalVisuellEntityBean> response = call.execute();
            if (response.isSuccessful()) {
        		// HTTP response code between 200 and 300
        		LOGGER.info("successfully stored sensor data, HTTP code: " + response.code() + ", message: " + response.message());
        		return new AsyncResult<>(new ProcessingResult("SUCCESS", response.isSuccessful()));
        	} else {
        		LOGGER.severe("failed to store sensor data, HTTP code: " + response.code() + ", message: " + response.message());
        		return new AsyncResult<>(new ProcessingResult("FAILURE", response.isSuccessful()));
        	}
        } catch (IOException | RuntimeException e) {
        	e.printStackTrace();
        	String errorMessage = "failed to store sensor data due to an IOException or RuntimeException:" + e.getMessage();
        	LOGGER.severe(errorMessage);
        	// TODO handle this in a better way
        }
        
        LOGGER.severe("was not able to execute HTTP request to store the sensor data and to get the HTTP response");
        return new AsyncResult<>(new ProcessingResult("ERROR", false));
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.processMessage.String.MqttMessage.String.String.Date.Future:DA-END
    }
    
    
    //DA-START:paho.ProcessingResult:DA-START
    //DA-ELSE:paho.ProcessingResult:DA-ELSE
    //DA-END:paho.ProcessingResult:DA-END
    class ProcessingResult { // start of class
    
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.status:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.status:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.status:DA-END
        private final String status;
        
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.success:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.success:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.success:DA-END
        private final boolean success;
        
        /**
         * creates an instance of ProcessingResult
         *
         * @param status  the status
         * @param success  the success
         */
        public ProcessingResult(String status, boolean success) {
            //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.String.boolean:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.String.boolean:DA-ELSE
            this.status = status;
            this.success = success;
            //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.String.boolean:DA-END
        }
        
        
        /**
         * getter for the field status
         *
         *
         *
         * @return
         */
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.getStatus.annotations:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.getStatus.annotations:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.getStatus.annotations:DA-END
        public String getStatus() {
            //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.getStatus.String:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.getStatus.String:DA-ELSE
            return this.status;
            //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.getStatus.String:DA-END
        }
        
        /**
         * getter for the field success
         *
         *
         *
         * @return
         */
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.isSuccess.annotations:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.isSuccess.annotations:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.isSuccess.annotations:DA-END
        public boolean isSuccess() {
            //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.isSuccess.boolean:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.isSuccess.boolean:DA-ELSE
            return this.success;
            //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.isSuccess.boolean:DA-END
        }
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.ProcessingResult.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.WarnsignalVisuellPahoClientBeanMessageProcessor.additional.elements.in.type:DA-END
} // end of java type