package org.gs.iot.sample.monitoring.gateway.ui;


import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ComponentSystemEvent;

import com.vd.AbstractManagedBean;

@ManagedBean(name="gatewayAppControlScreenBeanCustomization")
@ViewScoped
public class GatewayAppControlScreenBeanCustomization extends AbstractManagedBean implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    /**
     * provides configuration items that have default values and can be overwritten in individual web projects
     */
    private final Map<String, Object> configuration = new LinkedHashMap<String, Object>();
    
    /**
     * provides configuration items that are explicitely typed
     */
    private final GatewayAppControlScreenBeanCustomization.Configuration configurationItems = new GatewayAppControlScreenBeanCustomization.Configuration();
    
    @ManagedProperty(value="#{gatewayAppControlScreenBeanCustomization2}")
    private GatewayAppControlScreenBeanCustomization.GatewayAppControlScreenBeanCustomizationI gatewayAppControlScreenBeanCustomization;
    
    
    /**
     * getter for the field configuration
     *
     * provides configuration items that have default values and can be overwritten in individual web projects
     *
     * @return
     */
    public Map<String, Object> getConfiguration() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.getConfiguration.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.getConfiguration.Map:DA-ELSE
        return this.configuration;
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.getConfiguration.Map:DA-END
    }
    
    /**
     * getter for the field configurationItems
     *
     * provides configuration items that are explicitely typed
     *
     * @return
     */
    public GatewayAppControlScreenBeanCustomization.Configuration getConfigurationItems() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.getConfigurationItems.Configuration:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.getConfigurationItems.Configuration:DA-ELSE
        return this.configurationItems;
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.getConfigurationItems.Configuration:DA-END
    }
    
    /**
     * reads data from an EJB or from a different source and provides this data to xhtml pages
     */
    @Override
    public void init() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.init:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.init:DA-ELSE
        super.init();
        initConfiguration();
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.init:DA-END
    }
    
    /**
     * this method can get called when you define an event inside a f:metadata tag, e.g.: <f:event type="preRenderView" listener="#{myBean.initialize}"/>
     *
     * @param event  the event
     */
    @Override
    public void preRenderViewListener(ComponentSystemEvent event) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.preRenderViewListener.ComponentSystemEvent:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.preRenderViewListener.ComponentSystemEvent:DA-ELSE
         super.preRenderViewListener(event);
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.preRenderViewListener.ComponentSystemEvent:DA-END
    }
    
    /**
     * initializes the configuration map and reads additional custom configuration items from an injected managed bean
     */
    private void initConfiguration() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.initConfiguration:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.initConfiguration:DA-ELSE
        if (this.gatewayAppControlScreenBeanCustomization != null && this.gatewayAppControlScreenBeanCustomization != this) {
        	if (this.gatewayAppControlScreenBeanCustomization.getConfiguration() != null) {
        	    configuration.putAll(this.gatewayAppControlScreenBeanCustomization.getConfiguration());
        	}
            this.gatewayAppControlScreenBeanCustomization.getConfigurationItems(this.configurationItems);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.initConfiguration:DA-END
    }
    
    /**
     * setter for the field gatewayAppControlScreenBeanCustomization
     *
     *
     *
     * @param gatewayAppControlScreenBeanCustomization  the gatewayAppControlScreenBeanCustomization
     */
    public void setGatewayAppControlScreenBeanCustomization(GatewayAppControlScreenBeanCustomization.GatewayAppControlScreenBeanCustomizationI gatewayAppControlScreenBeanCustomization) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.setGatewayAppControlScreenBeanCustomization.GatewayAppControlScreenBeanCustomizationI:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.setGatewayAppControlScreenBeanCustomization.GatewayAppControlScreenBeanCustomizationI:DA-ELSE
        this.gatewayAppControlScreenBeanCustomization = gatewayAppControlScreenBeanCustomization;
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.setGatewayAppControlScreenBeanCustomization.GatewayAppControlScreenBeanCustomizationI:DA-END
    }
    
    
    public class Configuration { // start of class
    
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.Configuration.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.Configuration.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.Configuration.additional.elements.in.type:DA-END
    } // end of java type
    
    public static interface GatewayAppControlScreenBeanCustomizationI { // start of interface
    
        
        /**
         *
         * @return
         */
        Map<String, Object> getConfiguration();
        
        /**
         *
         * @param configuration  the configuration
         */
        void getConfigurationItems(GatewayAppControlScreenBeanCustomization.Configuration configuration);
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.GatewayAppControlScreenBeanCustomizationI.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.GatewayAppControlScreenBeanCustomizationI.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.GatewayAppControlScreenBeanCustomizationI.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanCustomization.additional.elements.in.type:DA-END
} // end of java type