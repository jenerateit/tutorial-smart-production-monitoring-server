package org.gs.iot.sample.monitoring.gateway.ui;


import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ComponentSystemEvent;

import com.vd.AbstractManagedBean;

@ManagedBean(name="gatewayAppControlScreenBeanNavigationCustomization")
@SessionScoped
public class GatewayAppControlScreenBeanNavigationCustomization extends AbstractManagedBean implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{gatewayAppControlScreenBeanNavigationCustomization2}")
    private GatewayAppControlScreenBeanNavigationCustomization.GatewayAppControlScreenBeanNavigationCustomizationI gatewayAppControlScreenBeanNavigationCustomization;
    
    
    /**
     * reads data from an EJB or from a different source and provides this data to xhtml pages
     */
    @Override
    public void init() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.init:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.init:DA-ELSE
        super.init();
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.init:DA-END
    }
    
    /**
     * this method can get called when you define an event inside a f:metadata tag, e.g.: <f:event type="preRenderView" listener="#{myBean.initialize}"/>
     *
     * @param event  the event
     */
    @Override
    public void preRenderViewListener(ComponentSystemEvent event) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.preRenderViewListener.ComponentSystemEvent:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.preRenderViewListener.ComponentSystemEvent:DA-ELSE
         super.preRenderViewListener(event);
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.preRenderViewListener.ComponentSystemEvent:DA-END
    }
    
    /**
     * setter for the field gatewayAppControlScreenBeanNavigationCustomization
     *
     *
     *
     * @param gatewayAppControlScreenBeanNavigationCustomization  the gatewayAppControlScreenBeanNavigationCustomization
     */
    public void setGatewayAppControlScreenBeanNavigationCustomization(GatewayAppControlScreenBeanNavigationCustomization.GatewayAppControlScreenBeanNavigationCustomizationI gatewayAppControlScreenBeanNavigationCustomization) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.setGatewayAppControlScreenBeanNavigationCustomization.GatewayAppControlScreenBeanNavigationCustomizationI:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.setGatewayAppControlScreenBeanNavigationCustomization.GatewayAppControlScreenBeanNavigationCustomizationI:DA-ELSE
        this.gatewayAppControlScreenBeanNavigationCustomization = gatewayAppControlScreenBeanNavigationCustomization;
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.setGatewayAppControlScreenBeanNavigationCustomization.GatewayAppControlScreenBeanNavigationCustomizationI:DA-END
    }
    
    
    public static interface GatewayAppControlScreenBeanNavigationCustomizationI { // start of interface
    
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.GatewayAppControlScreenBeanNavigationCustomizationI.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.GatewayAppControlScreenBeanNavigationCustomizationI.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.GatewayAppControlScreenBeanNavigationCustomizationI.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBeanNavigationCustomization.additional.elements.in.type:DA-END
} // end of java type