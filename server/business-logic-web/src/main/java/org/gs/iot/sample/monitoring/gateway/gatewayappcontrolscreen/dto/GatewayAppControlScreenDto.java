package org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto;


import com.vd.AbstractDto;
import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

/**
 * Dto class for UIStructuralContainer ('layout' element) 'GatewayAppControlScreen' in module 'GatewayAppUi'.
 * Support for dirty flag is added by the DTO generator.
 */
public class GatewayAppControlScreenDto extends AbstractDto implements Serializable { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.serialVersionUID:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.serialVersionUID:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.serialVersionUID:DA-END
    private static final long serialVersionUID = 1L;
    
    /**
     * holds a single object for the editor data container named 'GatewayAppControlForm' in module 'GatewayAppUi'
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.gatewayAppControlForm:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.gatewayAppControlForm:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.gatewayAppControlForm:DA-END
    private GatewayAppControlFormDto gatewayAppControlForm = new GatewayAppControlFormDto();
    
    /**
     * holds a single object with status NEW for the editor data container named 'GatewayAppControlForm' in module 'GatewayAppUi'
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.gatewayAppControlFormDtoNew:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.gatewayAppControlFormDtoNew:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.gatewayAppControlFormDtoNew:DA-END
    private GatewayAppControlFormDto gatewayAppControlFormDtoNew = new GatewayAppControlFormDto();
    
    /**
     * holds all objects for the editor data container named 'GatewayAppControlForm' in module 'GatewayAppUi'
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.gatewayAppControlFormDtos:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.gatewayAppControlFormDtos:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.gatewayAppControlFormDtos:DA-END
    private final List<GatewayAppControlFormDto> gatewayAppControlFormDtos = new ArrayList<GatewayAppControlFormDto>();
    
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.fields:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.fields:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.fields:DA-END
    /**
     * creates an instance of GatewayAppControlScreenDto
     */
    public GatewayAppControlScreenDto() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto:DA-END
    }
    
    
    /**
     * getter for the field gatewayAppControlForm
     *
     * holds a single object for the editor data container named 'GatewayAppControlForm' in module 'GatewayAppUi'
     *
     * @return
     */
    public GatewayAppControlFormDto getGatewayAppControlForm() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.getGatewayAppControlForm.GatewayAppControlFormDto:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.getGatewayAppControlForm.GatewayAppControlFormDto:DA-ELSE
        return this.gatewayAppControlForm;
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.getGatewayAppControlForm.GatewayAppControlFormDto:DA-END
    }
    
    /**
     * setter for the field gatewayAppControlForm
     *
     * holds a single object for the editor data container named 'GatewayAppControlForm' in module 'GatewayAppUi'
     *
     * @param gatewayAppControlForm  the gatewayAppControlForm
     */
    public void setGatewayAppControlForm(GatewayAppControlFormDto gatewayAppControlForm) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.setGatewayAppControlForm.GatewayAppControlFormDto:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.setGatewayAppControlForm.GatewayAppControlFormDto:DA-ELSE
        if (isDifferent(this.gatewayAppControlForm, gatewayAppControlForm)) {
            this.setDirty(true);
            this.gatewayAppControlForm = gatewayAppControlForm;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.setGatewayAppControlForm.GatewayAppControlFormDto:DA-END
    }
    
    /**
     * getter for the field gatewayAppControlFormDtoNew
     *
     * holds a single object with status NEW for the editor data container named 'GatewayAppControlForm' in module 'GatewayAppUi'
     *
     * @return
     */
    public GatewayAppControlFormDto getGatewayAppControlFormDtoNew() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.getGatewayAppControlFormDtoNew.GatewayAppControlFormDto:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.getGatewayAppControlFormDtoNew.GatewayAppControlFormDto:DA-ELSE
        return this.gatewayAppControlFormDtoNew;
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.getGatewayAppControlFormDtoNew.GatewayAppControlFormDto:DA-END
    }
    
    /**
     * setter for the field gatewayAppControlFormDtoNew
     *
     * holds a single object with status NEW for the editor data container named 'GatewayAppControlForm' in module 'GatewayAppUi'
     *
     * @param gatewayAppControlFormDtoNew  the gatewayAppControlFormDtoNew
     */
    public void setGatewayAppControlFormDtoNew(GatewayAppControlFormDto gatewayAppControlFormDtoNew) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.setGatewayAppControlFormDtoNew.GatewayAppControlFormDto:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.setGatewayAppControlFormDtoNew.GatewayAppControlFormDto:DA-ELSE
        if (isDifferent(this.gatewayAppControlFormDtoNew, gatewayAppControlFormDtoNew)) {
            this.setDirty(true);
            this.gatewayAppControlFormDtoNew = gatewayAppControlFormDtoNew;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.setGatewayAppControlFormDtoNew.GatewayAppControlFormDto:DA-END
    }
    
    /**
     * getter for the field gatewayAppControlFormDtos
     *
     * holds all objects for the editor data container named 'GatewayAppControlForm' in module 'GatewayAppUi'
     *
     * @return
     */
    public List<GatewayAppControlFormDto> getGatewayAppControlFormDtos() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.getGatewayAppControlFormDtos.List:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.getGatewayAppControlFormDtos.List:DA-ELSE
        return this.gatewayAppControlFormDtos;
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.getGatewayAppControlFormDtos.List:DA-END
    }
    
    /**
     * adder for the field gatewayAppControlFormDtos
     *
     * holds all objects for the editor data container named 'GatewayAppControlForm' in module 'GatewayAppUi'
     *
     * @param element  the element
     * @return
     */
    public boolean addGatewayAppControlFormDtos(GatewayAppControlFormDto element) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.addGatewayAppControlFormDtos.GatewayAppControlFormDto.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.addGatewayAppControlFormDtos.GatewayAppControlFormDto.boolean:DA-ELSE
        boolean result = this.gatewayAppControlFormDtos.add(element);
        this.setDirty( this.isDirty() || result);
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.addGatewayAppControlFormDtos.GatewayAppControlFormDto.boolean:DA-END
    }
    
    /**
     */
    public void selectFirstDtoOfGatewayAppControlForm() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.selectFirstDtoOfGatewayAppControlForm:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.selectFirstDtoOfGatewayAppControlForm:DA-ELSE
        if (gatewayAppControlFormDtos.size() > 0) {
        	gatewayAppControlForm = gatewayAppControlFormDtos.get(0);
        } else {
            gatewayAppControlForm = null;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.selectFirstDtoOfGatewayAppControlForm:DA-END
    }
    
    /**
     *
     * @param dto  the dto
     */
    public void addNewDtoToGatewayAppControlForm(GatewayAppControlFormDto dto) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.addNewDtoToGatewayAppControlForm.GatewayAppControlFormDto:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.addNewDtoToGatewayAppControlForm.GatewayAppControlFormDto:DA-ELSE
        gatewayAppControlFormDtos.add(0, dto);
        gatewayAppControlForm = dto;
        gatewayAppControlFormDtoNew = dto;
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.addNewDtoToGatewayAppControlForm.GatewayAppControlFormDto:DA-END
    }
    
    /**
     *
     * @param dto  the dto
     */
    public void removeDtoFromGatewayAppControlForm(GatewayAppControlFormDto dto) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.removeDtoFromGatewayAppControlForm.GatewayAppControlFormDto:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.removeDtoFromGatewayAppControlForm.GatewayAppControlFormDto:DA-ELSE
        if (gatewayAppControlFormDtos.size() > 0) {
            gatewayAppControlFormDtos.remove(dto);
            if (gatewayAppControlForm == dto) gatewayAppControlForm = null;
            if (gatewayAppControlFormDtoNew == dto) gatewayAppControlFormDtoNew = null; 
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.removeDtoFromGatewayAppControlForm.GatewayAppControlFormDto:DA-END
    }
    
    /**
     *
     * @return
     */
    @Override
    public boolean isDirty() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.isDirty.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.isDirty.boolean:DA-ELSE
        boolean result = super.isDirty();
        if (gatewayAppControlForm != null) result = result || gatewayAppControlForm.isDirty();
        if (gatewayAppControlFormDtoNew != null) result = result || gatewayAppControlFormDtoNew.isDirty();
        for (AbstractDto dto : gatewayAppControlFormDtos) {
            result = result || dto.isDirty();
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.isDirty.boolean:DA-END
    }
    
    /**
     */
    public void clearAllDirty() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.clearAllDirty:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.clearAllDirty:DA-ELSE
        super.setDirty(false);
        if (gatewayAppControlForm != null) gatewayAppControlForm.setDirty(false);
        if (gatewayAppControlForm != null) gatewayAppControlForm.clearAllDirty();
        if (gatewayAppControlFormDtoNew != null) gatewayAppControlFormDtoNew.setDirty(false);
        if (gatewayAppControlFormDtoNew != null) gatewayAppControlFormDtoNew.clearAllDirty();
        for (AbstractDto dto : gatewayAppControlFormDtos) {
            if (dto != null) dto.setDirty(false);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.clearAllDirty:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.methods:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.methods:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.methods:DA-END
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto.additional.elements.in.type:DA-END
} // end of java type