package org.gs.iot.sample.monitoring.gateway.push.paho;


import java.util.logging.Logger;
import java.util.Date;

public class SoundConnectionStatus { // start of class

    private static Logger LOGGER = java.util.logging.Logger.getLogger(SoundConnectionStatus.class.getName());
    
    private final String status;
    
    private final String cause;
    
    private final Date timestamp;
    
    /**
     * creates an instance of SoundConnectionStatus
     *
     * @param status  the status
     * @param cause  the cause
     * @param timestamp  the timestamp
     */
    public SoundConnectionStatus(String status, String cause, Date timestamp) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.String.String.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.String.String.Date:DA-ELSE
        this.status = status;
        this.cause = cause;
        this.timestamp = timestamp;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.String.String.Date:DA-END
    }
    
    
    /**
     * getter for the field status
     *
     *
     *
     * @return
     */
    public String getStatus() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.getStatus.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.getStatus.String:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.getStatus.String:DA-END
    }
    
    /**
     * getter for the field cause
     *
     *
     *
     * @return
     */
    public String getCause() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.getCause.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.getCause.String:DA-ELSE
        return this.cause;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.getCause.String:DA-END
    }
    
    /**
     * getter for the field timestamp
     *
     *
     *
     * @return
     */
    public Date getTimestamp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.getTimestamp.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.getTimestamp.Date:DA-ELSE
        return this.timestamp;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.getTimestamp.Date:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.SoundConnectionStatus.additional.elements.in.type:DA-END
} // end of java type