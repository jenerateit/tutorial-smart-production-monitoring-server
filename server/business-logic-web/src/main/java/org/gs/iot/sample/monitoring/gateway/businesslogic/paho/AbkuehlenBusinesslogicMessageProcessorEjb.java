package org.gs.iot.sample.monitoring.gateway.businesslogic.paho;


import javax.ejb.Stateless;
import java.util.logging.Logger;
import javax.ejb.EJB;
import org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb;
import javax.ejb.Asynchronous;
import java.util.concurrent.Future;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import java.util.Date;

/**
 * Wenn es über 30 Grad warm wird, dann soll die Kühlung
 * angeschaltet werden.
 */
//DA-START:paho.AbkuehlenBusinesslogicMessageProcessorEjb:DA-START
//DA-ELSE:paho.AbkuehlenBusinesslogicMessageProcessorEjb:DA-ELSE
@Stateless
//DA-END:paho.AbkuehlenBusinesslogicMessageProcessorEjb:DA-END

public class AbkuehlenBusinesslogicMessageProcessorEjb { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.LOGGER:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.LOGGER:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.LOGGER:DA-END
    private static Logger LOGGER = java.util.logging.Logger.getLogger(AbkuehlenBusinesslogicMessageProcessorEjb.class.getName());
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.configurationBean:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.configurationBean:DA-ELSE
    @EJB
    //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.configurationBean:DA-END
    private GatewayAppConfigurationEjb configurationBean;
    
    /**
     * creates an instance of AbkuehlenBusinesslogicMessageProcessorEjb
     */
    public AbkuehlenBusinesslogicMessageProcessorEjb() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb:DA-END
    }
    
    
    /**
     * getter for the field configurationBean
     *
     *
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.getConfigurationBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.getConfigurationBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.getConfigurationBean.annotations:DA-END
    public GatewayAppConfigurationEjb getConfigurationBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.getConfigurationBean.GatewayAppConfigurationEjb:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.getConfigurationBean.GatewayAppConfigurationEjb:DA-ELSE
        return this.configurationBean;
        //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.getConfigurationBean.GatewayAppConfigurationEjb:DA-END
    }
    
    /**
     * setter for the field configurationBean
     *
     *
     *
     * @param configurationBean  the configurationBean
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.setConfigurationBean.GatewayAppConfigurationEjb.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.setConfigurationBean.GatewayAppConfigurationEjb.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.setConfigurationBean.GatewayAppConfigurationEjb.annotations:DA-END
    public void setConfigurationBean(GatewayAppConfigurationEjb configurationBean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.setConfigurationBean.GatewayAppConfigurationEjb:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.setConfigurationBean.GatewayAppConfigurationEjb:DA-ELSE
        this.configurationBean = configurationBean;
        //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.setConfigurationBean.GatewayAppConfigurationEjb:DA-END
    }
    
    /**
     *
     * @param topic  the topic
     * @param message  the message
     * @param clientId  the clientId
     * @param appId  the appId
     * @param messageArrival  the messageArrival
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.processMessage.String.MqttMessage.String.String.Date.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.processMessage.String.MqttMessage.String.String.Date.annotations:DA-ELSE
    @Asynchronous
    //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.processMessage.String.MqttMessage.String.String.Date.annotations:DA-END
    public Future<AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult> processMessage(String topic, MqttMessage message, String clientId, String appId, Date messageArrival) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.processMessage.String.MqttMessage.String.String.Date.Future:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.processMessage.String.MqttMessage.String.String.Date.Future:DA-ELSE
        // TODO implement business logic
        return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.processMessage.String.MqttMessage.String.String.Date.Future:DA-END
    }
    
    
    //DA-START:paho.ProcessingResult:DA-START
    //DA-ELSE:paho.ProcessingResult:DA-ELSE
    //DA-END:paho.ProcessingResult:DA-END
    class ProcessingResult { // start of class
    
        //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.status:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.status:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.status:DA-END
        private final String status;
        
        //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.success:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.success:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.success:DA-END
        private final boolean success;
        
        /**
         * creates an instance of ProcessingResult
         *
         * @param status  the status
         * @param success  the success
         */
        public ProcessingResult(String status, boolean success) {
            //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.String.boolean:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.String.boolean:DA-ELSE
            this.status = status;
            this.success = success;
            //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.String.boolean:DA-END
        }
        
        
        /**
         * getter for the field status
         *
         *
         *
         * @return
         */
        //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.getStatus.annotations:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.getStatus.annotations:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.getStatus.annotations:DA-END
        public String getStatus() {
            //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.getStatus.String:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.getStatus.String:DA-ELSE
            return this.status;
            //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.getStatus.String:DA-END
        }
        
        /**
         * getter for the field success
         *
         *
         *
         * @return
         */
        //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.isSuccess.annotations:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.isSuccess.annotations:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.isSuccess.annotations:DA-END
        public boolean isSuccess() {
            //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.isSuccess.boolean:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.isSuccess.boolean:DA-ELSE
            return this.success;
            //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.isSuccess.boolean:DA-END
        }
        //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.ProcessingResult.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.AbkuehlenBusinesslogicMessageProcessorEjb.additional.elements.in.type:DA-END
} // end of java type