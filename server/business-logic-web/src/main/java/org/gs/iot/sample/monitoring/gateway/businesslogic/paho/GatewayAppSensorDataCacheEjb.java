package org.gs.iot.sample.monitoring.gateway.businesslogic.paho;


import javax.ejb.Singleton;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import java.util.logging.Logger;

//DA-START:paho.GatewayAppSensorDataCacheEjb:DA-START
//DA-ELSE:paho.GatewayAppSensorDataCacheEjb:DA-ELSE
@Singleton
@ConcurrencyManagement(value=ConcurrencyManagementType.BEAN)
//DA-END:paho.GatewayAppSensorDataCacheEjb:DA-END

public class GatewayAppSensorDataCacheEjb { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.GatewayAppSensorDataCacheEjb.LOGGER:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.GatewayAppSensorDataCacheEjb.LOGGER:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.GatewayAppSensorDataCacheEjb.LOGGER:DA-END
    private static Logger LOGGER = java.util.logging.Logger.getLogger(GatewayAppSensorDataCacheEjb.class.getName());
    
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.GatewayAppSensorDataCacheEjb.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.GatewayAppSensorDataCacheEjb.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.businesslogic.paho.GatewayAppSensorDataCacheEjb.additional.elements.in.type:DA-END
} // end of java type