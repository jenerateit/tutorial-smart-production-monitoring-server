package org.gs.iot.sample.monitoring.gateway.ui;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import com.vd.AbstractManagedBean;
import java.io.Serializable;
import org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto;
import javax.ejb.EJB;
import org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB;
import javax.faces.bean.ManagedProperty;
import javax.faces.event.ComponentSystemEvent;

@ManagedBean(name="gatewayAppControlScreenBean")
@ViewScoped
public class GatewayAppControlScreenBean extends AbstractManagedBean implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private GatewayAppControlScreenDto dto = new GatewayAppControlScreenDto();
    
    @EJB
    private GatewayAppControlScreenEJB ejb;
    
    private GatewayAppControlScreenBean.InitializationParameters initParameters = new GatewayAppControlScreenBean.InitializationParameters();
    
    @ManagedProperty(value="#{gatewayAppControlScreenBeanCustomization}")
    private GatewayAppControlScreenBeanCustomization gatewayAppControlScreenBeanCustomization;
    
    @ManagedProperty(value="#{gatewayAppControlScreenBeanNavigationCustomization}")
    private GatewayAppControlScreenBeanNavigationCustomization gatewayAppControlScreenBeanNavigationCustomization;
    
    
    /**
     * getter for the field dto
     *
     *
     *
     * @return
     */
    public GatewayAppControlScreenDto getDto() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.getDto.GatewayAppControlScreenDto:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.getDto.GatewayAppControlScreenDto:DA-ELSE
        return this.dto;
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.getDto.GatewayAppControlScreenDto:DA-END
    }
    
    /**
     * reads data from an EJB or from a different source and provides this data to xhtml pages
     */
    @Override
    public void init() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.init:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.init:DA-ELSE
        super.init();
        dto = ejb.load(dto, getSessionDataHolder());
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.init:DA-END
    }
    
    /**
     * this method can get called when you define an event inside a f:metadata tag, e.g.: <f:event type="preRenderView" listener="#{myBean.initialize}"/>
     *
     * @param event  the event
     */
    @Override
    public void preRenderViewListener(ComponentSystemEvent event) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.preRenderViewListener.ComponentSystemEvent:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.preRenderViewListener.ComponentSystemEvent:DA-ELSE
         super.preRenderViewListener(event);
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.preRenderViewListener.ComponentSystemEvent:DA-END
    }
    
    /**
     * The purpose of this method is to create and fill dto objects. It can be called from init() and from event handlers to (re-)initialize dto objects.
     */
    private void initDtos() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.initDtos:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.initDtos:DA-ELSE
        this.dto.clearAllDirty();
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.initDtos:DA-END
    }
    
    /**
     * The purpose of this method is to provide CSS styles to a web page that are applied in that web page only.
     *
     * @return
     */
    public String getPageStyle() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.getPageStyle.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.getPageStyle.String:DA-ELSE
        return "";
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.getPageStyle.String:DA-END
    }
    
    /**
     * This action handler has the purpose to provide a second place to intialize the managed bean's data (the other is on @PostConstruct)
     *
     * @return
     */
    public String viewActionPhaseInvokeApplication() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.viewActionPhaseInvokeApplication.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.viewActionPhaseInvokeApplication.String:DA-ELSE
        return initializeApplication();
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.viewActionPhaseInvokeApplication.String:DA-END
    }
    
    /**
     * In this method you write code to initialize a managed bean's data. It is up to you, whether you call this from init() (indirectly being called through @PostConstruct) or from viewActionPhaseInvokeApplication() (called through a viewAction)
     *
     * @return
     */
    @Override
    public String initApplication() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.initApplication.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.initApplication.String:DA-ELSE
        return super.initApplication();
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.initApplication.String:DA-END
    }
    
    /**
     * getter for the field initParameters
     *
     *
     *
     * @return
     */
    public GatewayAppControlScreenBean.InitializationParameters getInitParameters() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.getInitParameters.InitializationParameters:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.getInitParameters.InitializationParameters:DA-ELSE
        return this.initParameters;
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.getInitParameters.InitializationParameters:DA-END
    }
    
    /**
     * setter for the field gatewayAppControlScreenBeanCustomization
     *
     *
     *
     * @param gatewayAppControlScreenBeanCustomization  the gatewayAppControlScreenBeanCustomization
     */
    public void setGatewayAppControlScreenBeanCustomization(GatewayAppControlScreenBeanCustomization gatewayAppControlScreenBeanCustomization) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.setGatewayAppControlScreenBeanCustomization.GatewayAppControlScreenBeanCustomization:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.setGatewayAppControlScreenBeanCustomization.GatewayAppControlScreenBeanCustomization:DA-ELSE
        this.gatewayAppControlScreenBeanCustomization = gatewayAppControlScreenBeanCustomization;
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.setGatewayAppControlScreenBeanCustomization.GatewayAppControlScreenBeanCustomization:DA-END
    }
    
    /**
     * setter for the field gatewayAppControlScreenBeanNavigationCustomization
     *
     *
     *
     * @param gatewayAppControlScreenBeanNavigationCustomization  the gatewayAppControlScreenBeanNavigationCustomization
     */
    public void setGatewayAppControlScreenBeanNavigationCustomization(GatewayAppControlScreenBeanNavigationCustomization gatewayAppControlScreenBeanNavigationCustomization) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.setGatewayAppControlScreenBeanNavigationCustomization.GatewayAppControlScreenBeanNavigationCustomization:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.setGatewayAppControlScreenBeanNavigationCustomization.GatewayAppControlScreenBeanNavigationCustomization:DA-ELSE
        this.gatewayAppControlScreenBeanNavigationCustomization = gatewayAppControlScreenBeanNavigationCustomization;
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.setGatewayAppControlScreenBeanNavigationCustomization.GatewayAppControlScreenBeanNavigationCustomization:DA-END
    }
    
    
    public class InitializationParameters { // start of class
    
        //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.InitializationParameters.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.InitializationParameters.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.InitializationParameters.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.ui.GatewayAppControlScreenBean.additional.elements.in.type:DA-END
} // end of java type