package org.gs.iot.sample.monitoring.gateway.ejb;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import com.vd.AbstractEJB;
import org.gs.iot.sample.monitoring.gateway.gatewayappcontrolscreen.dto.GatewayAppControlScreenDto;
import com.vd.SessionDataHolder;

//DA-START:ejb.GatewayAppControlScreenEJB:DA-START
//DA-ELSE:ejb.GatewayAppControlScreenEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:ejb.GatewayAppControlScreenEJB:DA-END

public class GatewayAppControlScreenEJB extends AbstractEJB { // start of class

    /**
     * creates an instance of GatewayAppControlScreenEJB
     */
    public GatewayAppControlScreenEJB() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB:DA-END
    }
    
    
    /**
     * load the initial data that a managed bean then uses to serve one or more xhtml pages to fill it with dynamic data
     *
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB.load.GatewayAppControlScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB.load.GatewayAppControlScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB.load.GatewayAppControlScreenDto.SessionDataHolder.annotations:DA-END
    public GatewayAppControlScreenDto load(GatewayAppControlScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB.load.GatewayAppControlScreenDto.SessionDataHolder.GatewayAppControlScreenDto:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB.load.GatewayAppControlScreenDto.SessionDataHolder.GatewayAppControlScreenDto:DA-ELSE
        return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB.load.GatewayAppControlScreenDto.SessionDataHolder.GatewayAppControlScreenDto:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.ejb.GatewayAppControlScreenEJB.additional.elements.in.type:DA-END
} // end of java type