package org.gs.iot.sample.monitoring.gateway.paho;


import javax.ejb.Singleton;
import javax.inject.Named;
import java.util.logging.Logger;

//DA-START:paho.GatewayAppConfigurationEjb:DA-START
//DA-ELSE:paho.GatewayAppConfigurationEjb:DA-ELSE
@Singleton
@Named
//DA-END:paho.GatewayAppConfigurationEjb:DA-END

public class GatewayAppConfigurationEjb { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.LOGGER:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.LOGGER:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.LOGGER:DA-END
    private static Logger LOGGER = java.util.logging.Logger.getLogger(GatewayAppConfigurationEjb.class.getName());
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.dataProvisioningHost:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.dataProvisioningHost:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.dataProvisioningHost:DA-END
    private String dataProvisioningHost;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.dataProvisioningContextRoot:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.dataProvisioningContextRoot:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.dataProvisioningContextRoot:DA-END
    private String dataProvisioningContextRoot;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.dataProvisioningApplicationPath:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.dataProvisioningApplicationPath:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.dataProvisioningApplicationPath:DA-END
    private String dataProvisioningApplicationPath;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.mqttBrokerHostnameOrIp:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.mqttBrokerHostnameOrIp:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.mqttBrokerHostnameOrIp:DA-END
    private String mqttBrokerHostnameOrIp;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.mqttBrokerPort:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.mqttBrokerPort:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.mqttBrokerPort:DA-END
    private String mqttBrokerPort;
    
    /**
     * creates an instance of GatewayAppConfigurationEjb
     */
    public GatewayAppConfigurationEjb() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb:DA-END
    }
    
    
    /**
     * getter for the field dataProvisioningHost
     *
     *
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningHost.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningHost.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningHost.annotations:DA-END
    public String getDataProvisioningHost() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningHost.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningHost.String:DA-ELSE
        return this.dataProvisioningHost;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningHost.String:DA-END
    }
    
    /**
     * setter for the field dataProvisioningHost
     *
     *
     *
     * @param dataProvisioningHost  the dataProvisioningHost
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningHost.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningHost.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningHost.String.annotations:DA-END
    public void setDataProvisioningHost(String dataProvisioningHost) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningHost.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningHost.String:DA-ELSE
        this.dataProvisioningHost = dataProvisioningHost;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningHost.String:DA-END
    }
    
    /**
     * getter for the field dataProvisioningContextRoot
     *
     *
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningContextRoot.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningContextRoot.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningContextRoot.annotations:DA-END
    public String getDataProvisioningContextRoot() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningContextRoot.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningContextRoot.String:DA-ELSE
        return this.dataProvisioningContextRoot;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningContextRoot.String:DA-END
    }
    
    /**
     * setter for the field dataProvisioningContextRoot
     *
     *
     *
     * @param dataProvisioningContextRoot  the dataProvisioningContextRoot
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningContextRoot.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningContextRoot.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningContextRoot.String.annotations:DA-END
    public void setDataProvisioningContextRoot(String dataProvisioningContextRoot) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningContextRoot.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningContextRoot.String:DA-ELSE
        this.dataProvisioningContextRoot = dataProvisioningContextRoot;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningContextRoot.String:DA-END
    }
    
    /**
     * getter for the field dataProvisioningApplicationPath
     *
     *
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningApplicationPath.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningApplicationPath.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningApplicationPath.annotations:DA-END
    public String getDataProvisioningApplicationPath() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningApplicationPath.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningApplicationPath.String:DA-ELSE
        return this.dataProvisioningApplicationPath;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getDataProvisioningApplicationPath.String:DA-END
    }
    
    /**
     * setter for the field dataProvisioningApplicationPath
     *
     *
     *
     * @param dataProvisioningApplicationPath  the dataProvisioningApplicationPath
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningApplicationPath.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningApplicationPath.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningApplicationPath.String.annotations:DA-END
    public void setDataProvisioningApplicationPath(String dataProvisioningApplicationPath) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningApplicationPath.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningApplicationPath.String:DA-ELSE
        this.dataProvisioningApplicationPath = dataProvisioningApplicationPath;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setDataProvisioningApplicationPath.String:DA-END
    }
    
    /**
     * getter for the field mqttBrokerHostnameOrIp
     *
     *
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getMqttBrokerHostnameOrIp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getMqttBrokerHostnameOrIp.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getMqttBrokerHostnameOrIp.annotations:DA-END
    public String getMqttBrokerHostnameOrIp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getMqttBrokerHostnameOrIp.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getMqttBrokerHostnameOrIp.String:DA-ELSE
        return this.mqttBrokerHostnameOrIp;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getMqttBrokerHostnameOrIp.String:DA-END
    }
    
    /**
     * setter for the field mqttBrokerHostnameOrIp
     *
     *
     *
     * @param mqttBrokerHostnameOrIp  the mqttBrokerHostnameOrIp
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setMqttBrokerHostnameOrIp.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setMqttBrokerHostnameOrIp.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setMqttBrokerHostnameOrIp.String.annotations:DA-END
    public void setMqttBrokerHostnameOrIp(String mqttBrokerHostnameOrIp) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setMqttBrokerHostnameOrIp.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setMqttBrokerHostnameOrIp.String:DA-ELSE
        this.mqttBrokerHostnameOrIp = mqttBrokerHostnameOrIp;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setMqttBrokerHostnameOrIp.String:DA-END
    }
    
    /**
     * getter for the field mqttBrokerPort
     *
     *
     *
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getMqttBrokerPort.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getMqttBrokerPort.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getMqttBrokerPort.annotations:DA-END
    public String getMqttBrokerPort() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getMqttBrokerPort.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getMqttBrokerPort.String:DA-ELSE
        return this.mqttBrokerPort;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.getMqttBrokerPort.String:DA-END
    }
    
    /**
     * setter for the field mqttBrokerPort
     *
     *
     *
     * @param mqttBrokerPort  the mqttBrokerPort
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setMqttBrokerPort.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setMqttBrokerPort.String.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setMqttBrokerPort.String.annotations:DA-END
    public void setMqttBrokerPort(String mqttBrokerPort) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setMqttBrokerPort.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setMqttBrokerPort.String:DA-ELSE
        this.mqttBrokerPort = mqttBrokerPort;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.setMqttBrokerPort.String:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppConfigurationEjb.additional.elements.in.type:DA-END
} // end of java type