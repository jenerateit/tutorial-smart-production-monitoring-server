package org.gs.iot.sample.monitoring.gateway.push.paho;


import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.annotation.Singleton;
import java.util.logging.Logger;
import org.primefaces.push.annotation.OnOpen;
import org.primefaces.push.RemoteEndpoint;
import org.primefaces.push.EventBus;
import org.primefaces.push.annotation.OnClose;
import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.impl.JSONEncoder;

@PushEndpoint(value="/warnsignalvisuell/message")
@Singleton
public class WarnsignalVisuellMessageResource { // start of class

    private static Logger LOGGER = java.util.logging.Logger.getLogger(WarnsignalVisuellMessageResource.class.getName());
    
    
    /**
     *
     * @param remoteEndpoint  the remoteEndpoint
     * @param eventBus  the eventBus
     */
    @OnOpen
    public void onOpen(RemoteEndpoint remoteEndpoint, EventBus eventBus) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessageResource.onOpen.RemoteEndpoint.EventBus:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessageResource.onOpen.RemoteEndpoint.EventBus:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessageResource.onOpen.RemoteEndpoint.EventBus:DA-END
    }
    
    /**
     *
     * @param remoteEndpoint  the remoteEndpoint
     * @param eventBus  the eventBus
     */
    @OnClose
    public void onClose(RemoteEndpoint remoteEndpoint, EventBus eventBus) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessageResource.onClose.RemoteEndpoint.EventBus:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessageResource.onClose.RemoteEndpoint.EventBus:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessageResource.onClose.RemoteEndpoint.EventBus:DA-END
    }
    
    /**
     *
     * @param message  the message
     * @return
     */
    @OnMessage(encoders={org.primefaces.push.impl.JSONEncoder.class})
    public WarnsignalVisuellMessage onMessage(WarnsignalVisuellMessage message) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessageResource.onMessage.WarnsignalVisuellMessage.WarnsignalVisuellMessage:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessageResource.onMessage.WarnsignalVisuellMessage.WarnsignalVisuellMessage:DA-ELSE
        return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessageResource.onMessage.WarnsignalVisuellMessage.WarnsignalVisuellMessage:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessageResource.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessageResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.push.paho.WarnsignalVisuellMessageResource.additional.elements.in.type:DA-END
} // end of java type