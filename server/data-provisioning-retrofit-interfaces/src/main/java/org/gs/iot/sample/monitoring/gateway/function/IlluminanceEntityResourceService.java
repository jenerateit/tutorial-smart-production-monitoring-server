package org.gs.iot.sample.monitoring.gateway.function;
import java.util.List;

import org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface IlluminanceEntityResourceService { // start of interface

    
    /**
     *
     * @param id  the id
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @DELETE(value="illuminanceentity/{id}")
    Call<ResponseBody> delete(@Path(value="id") long id);
    
    /**
     *
     * @param id  the id
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @GET(value="illuminanceentity/{id}")
    Call<IlluminanceEntityBean> read(@Path(value="id") long id);
    
    /**
     *
     * @param offset  the offset
     * @param limit  the limit
     * @param ids  the ids
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @GET(value="illuminanceentity")
    Call<List<IlluminanceEntityBean>> readList(@Query(value="offset") int offset, @Query(value="limit") int limit, @Query(value="ids") List<String> ids);
    
    /**
     *
     * @param id  the id
     * @param bean  the bean
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @PUT(value="illuminanceentity/{id}")
    Call<IlluminanceEntityBean> update(@Path(value="id") long id, IlluminanceEntityBean bean);
    
    /**
     *
     * @param bean  the bean
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @POST(value="illuminanceentity")
    Call<IlluminanceEntityBean> create(IlluminanceEntityBean bean);
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityResourceService.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityResourceService.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityResourceService.additional.elements.in.type:DA-END
} // end of java type