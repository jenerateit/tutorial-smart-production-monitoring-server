package org.gs.iot.sample.monitoring.gateway.function;
import java.util.List;

import org.gs.iot.sample.monitoring.gateway.basic.TemperatureEntityBean;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface TemperatureEntityResourceService { // start of interface

    
    /**
     *
     * @param id  the id
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @DELETE(value="temperatureentity/{id}")
    Call<ResponseBody> delete(@Path(value="id") long id);
    
    /**
     *
     * @param id  the id
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @GET(value="temperatureentity/{id}")
    Call<TemperatureEntityBean> read(@Path(value="id") long id);
    
    /**
     *
     * @param offset  the offset
     * @param limit  the limit
     * @param ids  the ids
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @GET(value="temperatureentity")
    Call<List<TemperatureEntityBean>> readList(@Query(value="offset") int offset, @Query(value="limit") int limit, @Query(value="ids") List<String> ids);
    
    /**
     *
     * @param id  the id
     * @param bean  the bean
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @PUT(value="temperatureentity/{id}")
    Call<TemperatureEntityBean> update(@Path(value="id") long id, TemperatureEntityBean bean);
    
    /**
     *
     * @param bean  the bean
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @POST(value="temperatureentity")
    Call<TemperatureEntityBean> create(TemperatureEntityBean bean);
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityResourceService.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityResourceService.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityResourceService.additional.elements.in.type:DA-END
} // end of java type