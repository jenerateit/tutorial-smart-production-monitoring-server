package org.gs.iot.sample.monitoring.gateway.function;
import java.util.List;

import org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface WarnsignalVisuellEntityResourceService { // start of interface

    
    /**
     *
     * @param id  the id
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @DELETE(value="warnsignalvisuellentity/{id}")
    Call<ResponseBody> delete(@Path(value="id") long id);
    
    /**
     *
     * @param id  the id
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @GET(value="warnsignalvisuellentity/{id}")
    Call<WarnsignalVisuellEntityBean> read(@Path(value="id") long id);
    
    /**
     *
     * @param offset  the offset
     * @param limit  the limit
     * @param ids  the ids
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @GET(value="warnsignalvisuellentity")
    Call<List<WarnsignalVisuellEntityBean>> readList(@Query(value="offset") int offset, @Query(value="limit") int limit, @Query(value="ids") List<String> ids);
    
    /**
     *
     * @param id  the id
     * @param bean  the bean
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @PUT(value="warnsignalvisuellentity/{id}")
    Call<WarnsignalVisuellEntityBean> update(@Path(value="id") long id, WarnsignalVisuellEntityBean bean);
    
    /**
     *
     * @param bean  the bean
     * @return
     */
    @Headers(value={"Accept: application/json", "Content-Type: application/json"})
    @POST(value="warnsignalvisuellentity")
    Call<WarnsignalVisuellEntityBean> create(WarnsignalVisuellEntityBean bean);
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityResourceService.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityResourceService.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityResourceService.additional.elements.in.type:DA-END
} // end of java type