package com.vd.server;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;


@ApplicationPath(value="apppath/v1")
public class RestApplication extends Application { // start of class

    
    /**
     *
     * @return
     */
    public Set<Class<?>> getClasses() {
        //DA-START:com.vd.server.RestApplication.getClasses.Class:DA-START
        //DA-ELSE:com.vd.server.RestApplication.getClasses.Class:DA-ELSE
        Set<Class<?>> result = new LinkedHashSet<Class<?>>();
        result.add(org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.class);
        result.add(org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.class);
        result.add(org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.class);
        result.add(org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.class);
        result.add(org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.class);
        result.add(JacksonConfigurator.class);
        return result;
        //DA-END:com.vd.server.RestApplication.getClasses.Class:DA-END
    }
    
    /**
     *
     * @return
     */
    public Map<String, Object> getProperties() {
        //DA-START:com.vd.server.RestApplication.getProperties.Class:DA-START
        //DA-ELSE:com.vd.server.RestApplication.getProperties.Class:DA-ELSE
        return super.getProperties();
        //DA-END:com.vd.server.RestApplication.getProperties.Class:DA-END
    }
    
    /**
     *
     * @return
     */
    public Set<Object> getSingletons() {
        //DA-START:com.vd.server.RestApplication.getSingletons.Class:DA-START
        //DA-ELSE:com.vd.server.RestApplication.getSingletons.Class:DA-ELSE
        Set<Object> singletons = new HashSet<Object>();
        singletons.add(new JacksonJaxbJsonProvider());
        return singletons;
        //DA-END:com.vd.server.RestApplication.getSingletons.Class:DA-END
    }
    
    //DA-START:com.vd.server.RestApplication.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.server.RestApplication.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.server.RestApplication.additional.elements.in.type:DA-END
} // end of java type