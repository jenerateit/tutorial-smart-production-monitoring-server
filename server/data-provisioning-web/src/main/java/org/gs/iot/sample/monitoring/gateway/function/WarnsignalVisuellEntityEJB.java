package org.gs.iot.sample.monitoring.gateway.function;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.gs.iot.sample.monitoring.gateway.GeoCoordinates;
import org.gs.iot.sample.monitoring.gateway.MeasurementInfo;
import org.gs.iot.sample.monitoring.gateway.WarnsignalVisuell;
import org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntity;
import org.gs.iot.sample.monitoring.gateway.WarnsignalVisuellEntityDAO;
import org.gs.iot.sample.monitoring.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.monitoring.gateway.basic.MeasurementInfoBean;
import org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellBean;
import org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean;


//DA-START:function.WarnsignalVisuellEntityEJB:DA-START
//DA-ELSE:function.WarnsignalVisuellEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.WarnsignalVisuellEntityEJB:DA-END

public class WarnsignalVisuellEntityEJB implements WarnsignalVisuellEntityI { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="H2DS-IOT-DEV")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of WarnsignalVisuellEntityEJB
     */
    public WarnsignalVisuellEntityEJB() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB:DA-END
    }
    
    
    /**
     *
     * @param id
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.delete.long:DA-ELSE
        WarnsignalVisuellEntity entity = WarnsignalVisuellEntityDAO.get(entityManager, id);
        if (entity != null) {
            WarnsignalVisuellEntityDAO.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.delete.long:DA-END
    }
    
    /**
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.read.long.annotations:DA-END
    public WarnsignalVisuellEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.read.long.WarnsignalVisuellEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.read.long.WarnsignalVisuellEntityBean:DA-ELSE
        WarnsignalVisuellEntity entity = WarnsignalVisuellEntityDAO.get(entityManager, id);
        WarnsignalVisuellEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.read.long.WarnsignalVisuellEntityBean:DA-END
    }
    
    /**
     *
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.readList.int.int.List.annotations:DA-END
    public List<WarnsignalVisuellEntityBean> readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.readList.int.int.List.List:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.readList.int.int.List.List:DA-ELSE
        List<WarnsignalVisuellEntityBean> result = new ArrayList<>();
        for (WarnsignalVisuellEntity entity : WarnsignalVisuellEntityDAO.getAll(entityManager)) {
            WarnsignalVisuellEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.add(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.readList.int.int.List.List:DA-END
    }
    
    /**
     *
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.update.long.WarnsignalVisuellEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.update.long.WarnsignalVisuellEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.update.long.WarnsignalVisuellEntityBean.annotations:DA-END
    public WarnsignalVisuellEntityBean update(long id, WarnsignalVisuellEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.update.long.WarnsignalVisuellEntityBean.WarnsignalVisuellEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.update.long.WarnsignalVisuellEntityBean.WarnsignalVisuellEntityBean:DA-ELSE
        WarnsignalVisuellEntity updatedEntity = WarnsignalVisuellEntityDAO.get(entityManager, bean.getPk());
        WarnsignalVisuellEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = WarnsignalVisuellEntityDAO.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.update.long.WarnsignalVisuellEntityBean.WarnsignalVisuellEntityBean:DA-END
    }
    
    /**
     *
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.create.WarnsignalVisuellEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.create.WarnsignalVisuellEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.create.WarnsignalVisuellEntityBean.annotations:DA-END
    public WarnsignalVisuellEntityBean create(WarnsignalVisuellEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.create.WarnsignalVisuellEntityBean.WarnsignalVisuellEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.create.WarnsignalVisuellEntityBean.WarnsignalVisuellEntityBean:DA-ELSE
        WarnsignalVisuellEntity createdEntity = null;
        WarnsignalVisuellEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null, entityManager);
        if (createdEntity != null) {
            createdEntity = WarnsignalVisuellEntityDAO.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.create.WarnsignalVisuellEntityBean.WarnsignalVisuellEntityBean:DA-END
    }
    
    /**
     *
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.convertFromBeanToEntity.WarnsignalVisuellEntityBean.WarnsignalVisuellEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.convertFromBeanToEntity.WarnsignalVisuellEntityBean.WarnsignalVisuellEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.convertFromBeanToEntity.WarnsignalVisuellEntityBean.WarnsignalVisuellEntity.EntityManager.annotations:DA-END
    public static WarnsignalVisuellEntity convertFromBeanToEntity(WarnsignalVisuellEntityBean bean, WarnsignalVisuellEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.convertFromBeanToEntity.WarnsignalVisuellEntityBean.WarnsignalVisuellEntity.EntityManager.WarnsignalVisuellEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.convertFromBeanToEntity.WarnsignalVisuellEntityBean.WarnsignalVisuellEntity.EntityManager.WarnsignalVisuellEntity:DA-ELSE
        WarnsignalVisuellEntity result = entity;
        if (result == null) {
            result = new WarnsignalVisuellEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            WarnsignalVisuell dataObj = new WarnsignalVisuell();
            dataObj.setStatus(bean.getData().isStatus());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.convertFromBeanToEntity.WarnsignalVisuellEntityBean.WarnsignalVisuellEntity.EntityManager.WarnsignalVisuellEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.convertFromEntityToBean.WarnsignalVisuellEntity.WarnsignalVisuellEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.convertFromEntityToBean.WarnsignalVisuellEntity.WarnsignalVisuellEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.convertFromEntityToBean.WarnsignalVisuellEntity.WarnsignalVisuellEntityBean.Map.annotations:DA-END
    public static WarnsignalVisuellEntityBean convertFromEntityToBean(WarnsignalVisuellEntity entity, WarnsignalVisuellEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.convertFromEntityToBean.WarnsignalVisuellEntity.WarnsignalVisuellEntityBean.Map.WarnsignalVisuellEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.convertFromEntityToBean.WarnsignalVisuellEntity.WarnsignalVisuellEntityBean.Map.WarnsignalVisuellEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        WarnsignalVisuellEntityBean result = bean;
        if (result == null) {
            result = new WarnsignalVisuellEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            WarnsignalVisuellBean dataObj = new WarnsignalVisuellBean();
            dataObj.setStatus(entity.getData().isStatus());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.convertFromEntityToBean.WarnsignalVisuellEntity.WarnsignalVisuellEntityBean.Map.WarnsignalVisuellEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityEJB.additional.elements.in.type:DA-END
} // end of java type