package org.gs.iot.sample.monitoring.gateway.function.server;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Providers;

import org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean;
import org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityI;

//DA-START:server.IlluminanceEntityResource:DA-START
//DA-ELSE:server.IlluminanceEntityResource:DA-ELSE
@Consumes(value={"application/xml", "application/json"})
@Produces(value={"application/xml", "application/json"})
@Path(value="/illuminanceentity")
@RequestScoped
//DA-END:server.IlluminanceEntityResource:DA-END

public class IlluminanceEntityResource { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.uriInfo:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.uriInfo:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.uriInfo:DA-END
    private UriInfo uriInfo;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.httpHeaders:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.httpHeaders:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.httpHeaders:DA-END
    private HttpHeaders httpHeaders;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.securityContext:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.securityContext:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.securityContext:DA-END
    private SecurityContext securityContext;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.request:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.request:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.request:DA-END
    private Request request;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.providers:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.providers:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.providers:DA-END
    private Providers providers;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.illuminanceEntityBean:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.illuminanceEntityBean:DA-ELSE
    @EJB
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.illuminanceEntityBean:DA-END
    private IlluminanceEntityI illuminanceEntityBean;
    
    
    /**
     * there is no Java type to be used to provide data for the response body
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.delete.long.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.delete.long.annotations:DA-END
    public Response delete(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.delete.long.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.delete.long.Response:DA-ELSE
        illuminanceEntityBean.delete(id);
        return Response.ok().build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.delete.long.Response:DA-END
    }
    
    /**
     * there is no Java type to be used to provide data for the response body
     *
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.deleteByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.deleteByType.long.String.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.deleteByType.long.String.annotations:DA-END
    public Response deleteByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.deleteByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.deleteByType.long.Response.String:DA-ELSE
        illuminanceEntityBean.delete(id);
        return Response.ok().build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.deleteByType.long.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityBean'
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.read.long.annotations:DA-ELSE
    @GET
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.read.long.annotations:DA-END
    public Response read(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.read.long.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.read.long.Response:DA-ELSE
        IlluminanceEntityBean result = illuminanceEntityBean.read(id);
        return Response.ok(result, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.read.long.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityBean'
     *
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readByType.long.String.annotations:DA-ELSE
    @GET
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readByType.long.String.annotations:DA-END
    public Response readByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readByType.long.Response.String:DA-ELSE
        IlluminanceEntityBean result = illuminanceEntityBean.read(id);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readByType.long.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'List<IlluminanceEntityBean>'
     *
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readList.int.int.List.annotations:DA-ELSE
    @GET
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readList.int.int.List.annotations:DA-END
    public Response readList(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readList.int.int.List.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readList.int.int.List.Response:DA-ELSE
        List<IlluminanceEntityBean> result = illuminanceEntityBean.readList(offset, limit, ids);
        GenericEntity<List<IlluminanceEntityBean>> entity = new GenericEntity<List<IlluminanceEntityBean>>(result) {};
        return Response.ok(entity, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readList.int.int.List.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'List<IlluminanceEntityBean>'
     *
     * @param offset
     * @param limit
     * @param ids
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readListByType.int.int.List.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readListByType.int.int.List.String.annotations:DA-ELSE
    @GET
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readListByType.int.int.List.String.annotations:DA-END
    public Response readListByType(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readListByType.int.int.List.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readListByType.int.int.List.Response.String:DA-ELSE
        List<IlluminanceEntityBean> result = illuminanceEntityBean.readList(offset, limit, ids);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.readListByType.int.int.List.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityBean'
     *
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.update.long.IlluminanceEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.update.long.IlluminanceEntityBean.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.update.long.IlluminanceEntityBean.annotations:DA-END
    public Response update(@PathParam(value="id") long id, IlluminanceEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.update.long.IlluminanceEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.update.long.IlluminanceEntityBean.Response:DA-ELSE
        IlluminanceEntityBean result = illuminanceEntityBean.update(id, bean);
        return Response.ok(result, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.update.long.IlluminanceEntityBean.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityBean'
     *
     * @param id
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.updateByType.long.IlluminanceEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.updateByType.long.IlluminanceEntityBean.String.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.updateByType.long.IlluminanceEntityBean.String.annotations:DA-END
    public Response updateByType(@PathParam(value="id") long id, IlluminanceEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.updateByType.long.IlluminanceEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.updateByType.long.IlluminanceEntityBean.Response.String:DA-ELSE
        IlluminanceEntityBean result = illuminanceEntityBean.update(id, bean);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.updateByType.long.IlluminanceEntityBean.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityBean'
     *
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.create.IlluminanceEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.create.IlluminanceEntityBean.annotations:DA-ELSE
    @POST
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.create.IlluminanceEntityBean.annotations:DA-END
    public Response create(IlluminanceEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.create.IlluminanceEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.create.IlluminanceEntityBean.Response:DA-ELSE
        IlluminanceEntityBean result = illuminanceEntityBean.create(bean);
        String mediaType = httpHeaders.getRequestHeader(HttpHeaders.ACCEPT).get(0);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.create.IlluminanceEntityBean.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityBean'
     *
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.createByType.IlluminanceEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.createByType.IlluminanceEntityBean.String.annotations:DA-ELSE
    @POST
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.createByType.IlluminanceEntityBean.String.annotations:DA-END
    public Response createByType(IlluminanceEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.createByType.IlluminanceEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.createByType.IlluminanceEntityBean.Response.String:DA-ELSE
        IlluminanceEntityBean result = illuminanceEntityBean.create(bean);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.createByType.IlluminanceEntityBean.Response.String:DA-END
    }
    
    /**
     * setter for the field uriInfo
     *
     *
     *
     * @param uriInfo  the uriInfo
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setUriInfo.UriInfo.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setUriInfo.UriInfo.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setUriInfo.UriInfo.annotations:DA-END
    public void setUriInfo(UriInfo uriInfo) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setUriInfo.UriInfo:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setUriInfo.UriInfo:DA-ELSE
        this.uriInfo = uriInfo;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setUriInfo.UriInfo:DA-END
    }
    
    /**
     * setter for the field httpHeaders
     *
     *
     *
     * @param httpHeaders  the httpHeaders
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-END
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setHttpHeaders.HttpHeaders:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setHttpHeaders.HttpHeaders:DA-ELSE
        this.httpHeaders = httpHeaders;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setHttpHeaders.HttpHeaders:DA-END
    }
    
    /**
     * setter for the field securityContext
     *
     *
     *
     * @param securityContext  the securityContext
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setSecurityContext.SecurityContext.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setSecurityContext.SecurityContext.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setSecurityContext.SecurityContext.annotations:DA-END
    public void setSecurityContext(SecurityContext securityContext) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setSecurityContext.SecurityContext:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setSecurityContext.SecurityContext:DA-ELSE
        this.securityContext = securityContext;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setSecurityContext.SecurityContext:DA-END
    }
    
    /**
     * setter for the field request
     *
     *
     *
     * @param request  the request
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setRequest.Request.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setRequest.Request.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setRequest.Request.annotations:DA-END
    public void setRequest(Request request) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setRequest.Request:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setRequest.Request:DA-ELSE
        this.request = request;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setRequest.Request:DA-END
    }
    
    /**
     * setter for the field providers
     *
     *
     *
     * @param providers  the providers
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setProviders.Providers.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setProviders.Providers.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setProviders.Providers.annotations:DA-END
    public void setProviders(Providers providers) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setProviders.Providers:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setProviders.Providers:DA-ELSE
        this.providers = providers;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.setProviders.Providers:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.IlluminanceEntityResource.additional.elements.in.type:DA-END
} // end of java type