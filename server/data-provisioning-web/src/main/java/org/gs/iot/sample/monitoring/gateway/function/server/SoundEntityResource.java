package org.gs.iot.sample.monitoring.gateway.function.server;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Providers;

import org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean;
import org.gs.iot.sample.monitoring.gateway.function.SoundEntityI;

//DA-START:server.SoundEntityResource:DA-START
//DA-ELSE:server.SoundEntityResource:DA-ELSE
@Consumes(value={"application/xml", "application/json"})
@Produces(value={"application/xml", "application/json"})
@Path(value="/soundentity")
@RequestScoped
//DA-END:server.SoundEntityResource:DA-END

public class SoundEntityResource { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.uriInfo:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.uriInfo:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.uriInfo:DA-END
    private UriInfo uriInfo;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.httpHeaders:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.httpHeaders:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.httpHeaders:DA-END
    private HttpHeaders httpHeaders;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.securityContext:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.securityContext:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.securityContext:DA-END
    private SecurityContext securityContext;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.request:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.request:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.request:DA-END
    private Request request;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.providers:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.providers:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.providers:DA-END
    private Providers providers;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.soundEntityBean:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.soundEntityBean:DA-ELSE
    @EJB
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.soundEntityBean:DA-END
    private SoundEntityI soundEntityBean;
    
    
    /**
     * there is no Java type to be used to provide data for the response body
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.delete.long.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.delete.long.annotations:DA-END
    public Response delete(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.delete.long.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.delete.long.Response:DA-ELSE
        soundEntityBean.delete(id);
        return Response.ok().build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.delete.long.Response:DA-END
    }
    
    /**
     * there is no Java type to be used to provide data for the response body
     *
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.deleteByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.deleteByType.long.String.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.deleteByType.long.String.annotations:DA-END
    public Response deleteByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.deleteByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.deleteByType.long.Response.String:DA-ELSE
        soundEntityBean.delete(id);
        return Response.ok().build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.deleteByType.long.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.SoundEntityBean'
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.read.long.annotations:DA-ELSE
    @GET
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.read.long.annotations:DA-END
    public Response read(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.read.long.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.read.long.Response:DA-ELSE
        SoundEntityBean result = soundEntityBean.read(id);
        return Response.ok(result, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.read.long.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.SoundEntityBean'
     *
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readByType.long.String.annotations:DA-ELSE
    @GET
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readByType.long.String.annotations:DA-END
    public Response readByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readByType.long.Response.String:DA-ELSE
        SoundEntityBean result = soundEntityBean.read(id);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readByType.long.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'List<SoundEntityBean>'
     *
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readList.int.int.List.annotations:DA-ELSE
    @GET
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readList.int.int.List.annotations:DA-END
    public Response readList(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readList.int.int.List.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readList.int.int.List.Response:DA-ELSE
        List<SoundEntityBean> result = soundEntityBean.readList(offset, limit, ids);
        GenericEntity<List<SoundEntityBean>> entity = new GenericEntity<List<SoundEntityBean>>(result) {};
        return Response.ok(entity, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readList.int.int.List.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'List<SoundEntityBean>'
     *
     * @param offset
     * @param limit
     * @param ids
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readListByType.int.int.List.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readListByType.int.int.List.String.annotations:DA-ELSE
    @GET
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readListByType.int.int.List.String.annotations:DA-END
    public Response readListByType(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readListByType.int.int.List.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readListByType.int.int.List.Response.String:DA-ELSE
        List<SoundEntityBean> result = soundEntityBean.readList(offset, limit, ids);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.readListByType.int.int.List.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.SoundEntityBean'
     *
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.update.long.SoundEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.update.long.SoundEntityBean.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.update.long.SoundEntityBean.annotations:DA-END
    public Response update(@PathParam(value="id") long id, SoundEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.update.long.SoundEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.update.long.SoundEntityBean.Response:DA-ELSE
        SoundEntityBean result = soundEntityBean.update(id, bean);
        return Response.ok(result, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.update.long.SoundEntityBean.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.SoundEntityBean'
     *
     * @param id
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.updateByType.long.SoundEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.updateByType.long.SoundEntityBean.String.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.updateByType.long.SoundEntityBean.String.annotations:DA-END
    public Response updateByType(@PathParam(value="id") long id, SoundEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.updateByType.long.SoundEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.updateByType.long.SoundEntityBean.Response.String:DA-ELSE
        SoundEntityBean result = soundEntityBean.update(id, bean);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.updateByType.long.SoundEntityBean.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.SoundEntityBean'
     *
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.create.SoundEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.create.SoundEntityBean.annotations:DA-ELSE
    @POST
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.create.SoundEntityBean.annotations:DA-END
    public Response create(SoundEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.create.SoundEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.create.SoundEntityBean.Response:DA-ELSE
        SoundEntityBean result = soundEntityBean.create(bean);
        String mediaType = httpHeaders.getRequestHeader(HttpHeaders.ACCEPT).get(0);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.create.SoundEntityBean.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.SoundEntityBean'
     *
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.createByType.SoundEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.createByType.SoundEntityBean.String.annotations:DA-ELSE
    @POST
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.createByType.SoundEntityBean.String.annotations:DA-END
    public Response createByType(SoundEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.createByType.SoundEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.createByType.SoundEntityBean.Response.String:DA-ELSE
        SoundEntityBean result = soundEntityBean.create(bean);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.createByType.SoundEntityBean.Response.String:DA-END
    }
    
    /**
     * setter for the field uriInfo
     *
     *
     *
     * @param uriInfo  the uriInfo
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setUriInfo.UriInfo.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setUriInfo.UriInfo.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setUriInfo.UriInfo.annotations:DA-END
    public void setUriInfo(UriInfo uriInfo) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setUriInfo.UriInfo:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setUriInfo.UriInfo:DA-ELSE
        this.uriInfo = uriInfo;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setUriInfo.UriInfo:DA-END
    }
    
    /**
     * setter for the field httpHeaders
     *
     *
     *
     * @param httpHeaders  the httpHeaders
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-END
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setHttpHeaders.HttpHeaders:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setHttpHeaders.HttpHeaders:DA-ELSE
        this.httpHeaders = httpHeaders;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setHttpHeaders.HttpHeaders:DA-END
    }
    
    /**
     * setter for the field securityContext
     *
     *
     *
     * @param securityContext  the securityContext
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setSecurityContext.SecurityContext.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setSecurityContext.SecurityContext.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setSecurityContext.SecurityContext.annotations:DA-END
    public void setSecurityContext(SecurityContext securityContext) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setSecurityContext.SecurityContext:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setSecurityContext.SecurityContext:DA-ELSE
        this.securityContext = securityContext;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setSecurityContext.SecurityContext:DA-END
    }
    
    /**
     * setter for the field request
     *
     *
     *
     * @param request  the request
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setRequest.Request.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setRequest.Request.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setRequest.Request.annotations:DA-END
    public void setRequest(Request request) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setRequest.Request:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setRequest.Request:DA-ELSE
        this.request = request;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setRequest.Request:DA-END
    }
    
    /**
     * setter for the field providers
     *
     *
     *
     * @param providers  the providers
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setProviders.Providers.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setProviders.Providers.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setProviders.Providers.annotations:DA-END
    public void setProviders(Providers providers) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setProviders.Providers:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setProviders.Providers:DA-ELSE
        this.providers = providers;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.setProviders.Providers:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.SoundEntityResource.additional.elements.in.type:DA-END
} // end of java type