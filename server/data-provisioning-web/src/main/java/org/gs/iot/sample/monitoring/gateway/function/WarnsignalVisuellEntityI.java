package org.gs.iot.sample.monitoring.gateway.function;
import java.util.List;

import org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean;


public interface WarnsignalVisuellEntityI { // start of interface

    
    /**
     *
     * @param id  the id
     */
    void delete(long id);
    
    /**
     *
     * @param id  the id
     * @return
     */
    WarnsignalVisuellEntityBean read(long id);
    
    /**
     *
     * @param offset  the offset
     * @param limit  the limit
     * @param ids  the ids
     * @return
     */
    List<WarnsignalVisuellEntityBean> readList(int offset, int limit, List<String> ids);
    
    /**
     *
     * @param id  the id
     * @param bean  the bean
     * @return
     */
    WarnsignalVisuellEntityBean update(long id, WarnsignalVisuellEntityBean bean);
    
    /**
     *
     * @param bean  the bean
     * @return
     */
    WarnsignalVisuellEntityBean create(WarnsignalVisuellEntityBean bean);
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityI.additional.elements.in.type:DA-END
} // end of java type