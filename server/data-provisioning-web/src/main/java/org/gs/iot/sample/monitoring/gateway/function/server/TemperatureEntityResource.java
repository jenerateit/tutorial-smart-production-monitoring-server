package org.gs.iot.sample.monitoring.gateway.function.server;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Providers;

import org.gs.iot.sample.monitoring.gateway.basic.TemperatureEntityBean;
import org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityI;

//DA-START:server.TemperatureEntityResource:DA-START
//DA-ELSE:server.TemperatureEntityResource:DA-ELSE
@Consumes(value={"application/xml", "application/json"})
@Produces(value={"application/xml", "application/json"})
@Path(value="/temperatureentity")
@RequestScoped
//DA-END:server.TemperatureEntityResource:DA-END

public class TemperatureEntityResource { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.uriInfo:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.uriInfo:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.uriInfo:DA-END
    private UriInfo uriInfo;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.httpHeaders:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.httpHeaders:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.httpHeaders:DA-END
    private HttpHeaders httpHeaders;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.securityContext:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.securityContext:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.securityContext:DA-END
    private SecurityContext securityContext;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.request:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.request:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.request:DA-END
    private Request request;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.providers:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.providers:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.providers:DA-END
    private Providers providers;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.temperatureEntityBean:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.temperatureEntityBean:DA-ELSE
    @EJB
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.temperatureEntityBean:DA-END
    private TemperatureEntityI temperatureEntityBean;
    
    
    /**
     * there is no Java type to be used to provide data for the response body
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.delete.long.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.delete.long.annotations:DA-END
    public Response delete(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.delete.long.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.delete.long.Response:DA-ELSE
        temperatureEntityBean.delete(id);
        return Response.ok().build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.delete.long.Response:DA-END
    }
    
    /**
     * there is no Java type to be used to provide data for the response body
     *
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.deleteByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.deleteByType.long.String.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.deleteByType.long.String.annotations:DA-END
    public Response deleteByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.deleteByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.deleteByType.long.Response.String:DA-ELSE
        temperatureEntityBean.delete(id);
        return Response.ok().build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.deleteByType.long.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityBean'
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.read.long.annotations:DA-ELSE
    @GET
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.read.long.annotations:DA-END
    public Response read(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.read.long.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.read.long.Response:DA-ELSE
        TemperatureEntityBean result = temperatureEntityBean.read(id);
        return Response.ok(result, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.read.long.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityBean'
     *
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readByType.long.String.annotations:DA-ELSE
    @GET
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readByType.long.String.annotations:DA-END
    public Response readByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readByType.long.Response.String:DA-ELSE
        TemperatureEntityBean result = temperatureEntityBean.read(id);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readByType.long.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'List<TemperatureEntityBean>'
     *
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readList.int.int.List.annotations:DA-ELSE
    @GET
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readList.int.int.List.annotations:DA-END
    public Response readList(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readList.int.int.List.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readList.int.int.List.Response:DA-ELSE
        List<TemperatureEntityBean> result = temperatureEntityBean.readList(offset, limit, ids);
        GenericEntity<List<TemperatureEntityBean>> entity = new GenericEntity<List<TemperatureEntityBean>>(result) {};
        return Response.ok(entity, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readList.int.int.List.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'List<TemperatureEntityBean>'
     *
     * @param offset
     * @param limit
     * @param ids
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readListByType.int.int.List.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readListByType.int.int.List.String.annotations:DA-ELSE
    @GET
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readListByType.int.int.List.String.annotations:DA-END
    public Response readListByType(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readListByType.int.int.List.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readListByType.int.int.List.Response.String:DA-ELSE
        List<TemperatureEntityBean> result = temperatureEntityBean.readList(offset, limit, ids);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.readListByType.int.int.List.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityBean'
     *
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.update.long.TemperatureEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.update.long.TemperatureEntityBean.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.update.long.TemperatureEntityBean.annotations:DA-END
    public Response update(@PathParam(value="id") long id, TemperatureEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.update.long.TemperatureEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.update.long.TemperatureEntityBean.Response:DA-ELSE
        TemperatureEntityBean result = temperatureEntityBean.update(id, bean);
        return Response.ok(result, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.update.long.TemperatureEntityBean.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityBean'
     *
     * @param id
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.updateByType.long.TemperatureEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.updateByType.long.TemperatureEntityBean.String.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.updateByType.long.TemperatureEntityBean.String.annotations:DA-END
    public Response updateByType(@PathParam(value="id") long id, TemperatureEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.updateByType.long.TemperatureEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.updateByType.long.TemperatureEntityBean.Response.String:DA-ELSE
        TemperatureEntityBean result = temperatureEntityBean.update(id, bean);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.updateByType.long.TemperatureEntityBean.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityBean'
     *
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.create.TemperatureEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.create.TemperatureEntityBean.annotations:DA-ELSE
    @POST
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.create.TemperatureEntityBean.annotations:DA-END
    public Response create(TemperatureEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.create.TemperatureEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.create.TemperatureEntityBean.Response:DA-ELSE
        TemperatureEntityBean result = temperatureEntityBean.create(bean);
        String mediaType = httpHeaders.getRequestHeader(HttpHeaders.ACCEPT).get(0);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.create.TemperatureEntityBean.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityBean'
     *
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.createByType.TemperatureEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.createByType.TemperatureEntityBean.String.annotations:DA-ELSE
    @POST
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.createByType.TemperatureEntityBean.String.annotations:DA-END
    public Response createByType(TemperatureEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.createByType.TemperatureEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.createByType.TemperatureEntityBean.Response.String:DA-ELSE
        TemperatureEntityBean result = temperatureEntityBean.create(bean);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.createByType.TemperatureEntityBean.Response.String:DA-END
    }
    
    /**
     * setter for the field uriInfo
     *
     *
     *
     * @param uriInfo  the uriInfo
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setUriInfo.UriInfo.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setUriInfo.UriInfo.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setUriInfo.UriInfo.annotations:DA-END
    public void setUriInfo(UriInfo uriInfo) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setUriInfo.UriInfo:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setUriInfo.UriInfo:DA-ELSE
        this.uriInfo = uriInfo;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setUriInfo.UriInfo:DA-END
    }
    
    /**
     * setter for the field httpHeaders
     *
     *
     *
     * @param httpHeaders  the httpHeaders
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-END
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setHttpHeaders.HttpHeaders:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setHttpHeaders.HttpHeaders:DA-ELSE
        this.httpHeaders = httpHeaders;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setHttpHeaders.HttpHeaders:DA-END
    }
    
    /**
     * setter for the field securityContext
     *
     *
     *
     * @param securityContext  the securityContext
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setSecurityContext.SecurityContext.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setSecurityContext.SecurityContext.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setSecurityContext.SecurityContext.annotations:DA-END
    public void setSecurityContext(SecurityContext securityContext) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setSecurityContext.SecurityContext:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setSecurityContext.SecurityContext:DA-ELSE
        this.securityContext = securityContext;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setSecurityContext.SecurityContext:DA-END
    }
    
    /**
     * setter for the field request
     *
     *
     *
     * @param request  the request
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setRequest.Request.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setRequest.Request.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setRequest.Request.annotations:DA-END
    public void setRequest(Request request) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setRequest.Request:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setRequest.Request:DA-ELSE
        this.request = request;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setRequest.Request:DA-END
    }
    
    /**
     * setter for the field providers
     *
     *
     *
     * @param providers  the providers
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setProviders.Providers.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setProviders.Providers.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setProviders.Providers.annotations:DA-END
    public void setProviders(Providers providers) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setProviders.Providers:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setProviders.Providers:DA-ELSE
        this.providers = providers;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.setProviders.Providers:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.TemperatureEntityResource.additional.elements.in.type:DA-END
} // end of java type