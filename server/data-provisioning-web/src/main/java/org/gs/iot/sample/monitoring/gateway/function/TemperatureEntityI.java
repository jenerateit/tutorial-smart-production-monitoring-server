package org.gs.iot.sample.monitoring.gateway.function;
import java.util.List;

import org.gs.iot.sample.monitoring.gateway.basic.TemperatureEntityBean;


public interface TemperatureEntityI { // start of interface

    
    /**
     *
     * @param id  the id
     */
    void delete(long id);
    
    /**
     *
     * @param id  the id
     * @return
     */
    TemperatureEntityBean read(long id);
    
    /**
     *
     * @param offset  the offset
     * @param limit  the limit
     * @param ids  the ids
     * @return
     */
    List<TemperatureEntityBean> readList(int offset, int limit, List<String> ids);
    
    /**
     *
     * @param id  the id
     * @param bean  the bean
     * @return
     */
    TemperatureEntityBean update(long id, TemperatureEntityBean bean);
    
    /**
     *
     * @param bean  the bean
     * @return
     */
    TemperatureEntityBean create(TemperatureEntityBean bean);
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityI.additional.elements.in.type:DA-END
} // end of java type