package org.gs.iot.sample.monitoring.gateway.function.server;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Providers;

import org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean;
import org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityI;

//DA-START:server.WarnsignalAkustischEntityResource:DA-START
//DA-ELSE:server.WarnsignalAkustischEntityResource:DA-ELSE
@Consumes(value={"application/xml", "application/json"})
@Produces(value={"application/xml", "application/json"})
@Path(value="/warnsignalakustischentity")
@RequestScoped
//DA-END:server.WarnsignalAkustischEntityResource:DA-END

public class WarnsignalAkustischEntityResource { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.uriInfo:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.uriInfo:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.uriInfo:DA-END
    private UriInfo uriInfo;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.httpHeaders:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.httpHeaders:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.httpHeaders:DA-END
    private HttpHeaders httpHeaders;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.securityContext:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.securityContext:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.securityContext:DA-END
    private SecurityContext securityContext;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.request:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.request:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.request:DA-END
    private Request request;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.providers:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.providers:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.providers:DA-END
    private Providers providers;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.warnsignalAkustischEntityBean:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.warnsignalAkustischEntityBean:DA-ELSE
    @EJB
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.warnsignalAkustischEntityBean:DA-END
    private WarnsignalAkustischEntityI warnsignalAkustischEntityBean;
    
    
    /**
     * there is no Java type to be used to provide data for the response body
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.delete.long.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.delete.long.annotations:DA-END
    public Response delete(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.delete.long.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.delete.long.Response:DA-ELSE
        warnsignalAkustischEntityBean.delete(id);
        return Response.ok().build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.delete.long.Response:DA-END
    }
    
    /**
     * there is no Java type to be used to provide data for the response body
     *
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.deleteByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.deleteByType.long.String.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.deleteByType.long.String.annotations:DA-END
    public Response deleteByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.deleteByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.deleteByType.long.Response.String:DA-ELSE
        warnsignalAkustischEntityBean.delete(id);
        return Response.ok().build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.deleteByType.long.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityBean'
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.read.long.annotations:DA-ELSE
    @GET
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.read.long.annotations:DA-END
    public Response read(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.read.long.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.read.long.Response:DA-ELSE
        WarnsignalAkustischEntityBean result = warnsignalAkustischEntityBean.read(id);
        return Response.ok(result, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.read.long.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityBean'
     *
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readByType.long.String.annotations:DA-ELSE
    @GET
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readByType.long.String.annotations:DA-END
    public Response readByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readByType.long.Response.String:DA-ELSE
        WarnsignalAkustischEntityBean result = warnsignalAkustischEntityBean.read(id);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readByType.long.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'List<WarnsignalAkustischEntityBean>'
     *
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readList.int.int.List.annotations:DA-ELSE
    @GET
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readList.int.int.List.annotations:DA-END
    public Response readList(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readList.int.int.List.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readList.int.int.List.Response:DA-ELSE
        List<WarnsignalAkustischEntityBean> result = warnsignalAkustischEntityBean.readList(offset, limit, ids);
        GenericEntity<List<WarnsignalAkustischEntityBean>> entity = new GenericEntity<List<WarnsignalAkustischEntityBean>>(result) {};
        return Response.ok(entity, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readList.int.int.List.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'List<WarnsignalAkustischEntityBean>'
     *
     * @param offset
     * @param limit
     * @param ids
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readListByType.int.int.List.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readListByType.int.int.List.String.annotations:DA-ELSE
    @GET
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readListByType.int.int.List.String.annotations:DA-END
    public Response readListByType(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readListByType.int.int.List.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readListByType.int.int.List.Response.String:DA-ELSE
        List<WarnsignalAkustischEntityBean> result = warnsignalAkustischEntityBean.readList(offset, limit, ids);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.readListByType.int.int.List.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityBean'
     *
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.update.long.WarnsignalAkustischEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.update.long.WarnsignalAkustischEntityBean.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.update.long.WarnsignalAkustischEntityBean.annotations:DA-END
    public Response update(@PathParam(value="id") long id, WarnsignalAkustischEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.update.long.WarnsignalAkustischEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.update.long.WarnsignalAkustischEntityBean.Response:DA-ELSE
        WarnsignalAkustischEntityBean result = warnsignalAkustischEntityBean.update(id, bean);
        return Response.ok(result, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.update.long.WarnsignalAkustischEntityBean.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityBean'
     *
     * @param id
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.updateByType.long.WarnsignalAkustischEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.updateByType.long.WarnsignalAkustischEntityBean.String.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.updateByType.long.WarnsignalAkustischEntityBean.String.annotations:DA-END
    public Response updateByType(@PathParam(value="id") long id, WarnsignalAkustischEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.updateByType.long.WarnsignalAkustischEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.updateByType.long.WarnsignalAkustischEntityBean.Response.String:DA-ELSE
        WarnsignalAkustischEntityBean result = warnsignalAkustischEntityBean.update(id, bean);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.updateByType.long.WarnsignalAkustischEntityBean.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityBean'
     *
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.create.WarnsignalAkustischEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.create.WarnsignalAkustischEntityBean.annotations:DA-ELSE
    @POST
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.create.WarnsignalAkustischEntityBean.annotations:DA-END
    public Response create(WarnsignalAkustischEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.create.WarnsignalAkustischEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.create.WarnsignalAkustischEntityBean.Response:DA-ELSE
        WarnsignalAkustischEntityBean result = warnsignalAkustischEntityBean.create(bean);
        String mediaType = httpHeaders.getRequestHeader(HttpHeaders.ACCEPT).get(0);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.create.WarnsignalAkustischEntityBean.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityBean'
     *
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.createByType.WarnsignalAkustischEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.createByType.WarnsignalAkustischEntityBean.String.annotations:DA-ELSE
    @POST
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.createByType.WarnsignalAkustischEntityBean.String.annotations:DA-END
    public Response createByType(WarnsignalAkustischEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.createByType.WarnsignalAkustischEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.createByType.WarnsignalAkustischEntityBean.Response.String:DA-ELSE
        WarnsignalAkustischEntityBean result = warnsignalAkustischEntityBean.create(bean);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.createByType.WarnsignalAkustischEntityBean.Response.String:DA-END
    }
    
    /**
     * setter for the field uriInfo
     *
     *
     *
     * @param uriInfo  the uriInfo
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setUriInfo.UriInfo.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setUriInfo.UriInfo.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setUriInfo.UriInfo.annotations:DA-END
    public void setUriInfo(UriInfo uriInfo) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setUriInfo.UriInfo:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setUriInfo.UriInfo:DA-ELSE
        this.uriInfo = uriInfo;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setUriInfo.UriInfo:DA-END
    }
    
    /**
     * setter for the field httpHeaders
     *
     *
     *
     * @param httpHeaders  the httpHeaders
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-END
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setHttpHeaders.HttpHeaders:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setHttpHeaders.HttpHeaders:DA-ELSE
        this.httpHeaders = httpHeaders;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setHttpHeaders.HttpHeaders:DA-END
    }
    
    /**
     * setter for the field securityContext
     *
     *
     *
     * @param securityContext  the securityContext
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setSecurityContext.SecurityContext.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setSecurityContext.SecurityContext.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setSecurityContext.SecurityContext.annotations:DA-END
    public void setSecurityContext(SecurityContext securityContext) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setSecurityContext.SecurityContext:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setSecurityContext.SecurityContext:DA-ELSE
        this.securityContext = securityContext;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setSecurityContext.SecurityContext:DA-END
    }
    
    /**
     * setter for the field request
     *
     *
     *
     * @param request  the request
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setRequest.Request.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setRequest.Request.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setRequest.Request.annotations:DA-END
    public void setRequest(Request request) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setRequest.Request:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setRequest.Request:DA-ELSE
        this.request = request;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setRequest.Request:DA-END
    }
    
    /**
     * setter for the field providers
     *
     *
     *
     * @param providers  the providers
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setProviders.Providers.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setProviders.Providers.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setProviders.Providers.annotations:DA-END
    public void setProviders(Providers providers) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setProviders.Providers:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setProviders.Providers:DA-ELSE
        this.providers = providers;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.setProviders.Providers:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalAkustischEntityResource.additional.elements.in.type:DA-END
} // end of java type