package org.gs.iot.sample.monitoring.gateway.function;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.gs.iot.sample.monitoring.gateway.GeoCoordinates;
import org.gs.iot.sample.monitoring.gateway.MeasurementInfo;
import org.gs.iot.sample.monitoring.gateway.WarnsignalAkustisch;
import org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntity;
import org.gs.iot.sample.monitoring.gateway.WarnsignalAkustischEntityDAO;
import org.gs.iot.sample.monitoring.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.monitoring.gateway.basic.MeasurementInfoBean;
import org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischBean;
import org.gs.iot.sample.monitoring.gateway.basic.WarnsignalAkustischEntityBean;


//DA-START:function.WarnsignalAkustischEntityEJB:DA-START
//DA-ELSE:function.WarnsignalAkustischEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.WarnsignalAkustischEntityEJB:DA-END

public class WarnsignalAkustischEntityEJB implements WarnsignalAkustischEntityI { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="H2DS-IOT-DEV")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of WarnsignalAkustischEntityEJB
     */
    public WarnsignalAkustischEntityEJB() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB:DA-END
    }
    
    
    /**
     *
     * @param id
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.delete.long:DA-ELSE
        WarnsignalAkustischEntity entity = WarnsignalAkustischEntityDAO.get(entityManager, id);
        if (entity != null) {
            WarnsignalAkustischEntityDAO.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.delete.long:DA-END
    }
    
    /**
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.read.long.annotations:DA-END
    public WarnsignalAkustischEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.read.long.WarnsignalAkustischEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.read.long.WarnsignalAkustischEntityBean:DA-ELSE
        WarnsignalAkustischEntity entity = WarnsignalAkustischEntityDAO.get(entityManager, id);
        WarnsignalAkustischEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.read.long.WarnsignalAkustischEntityBean:DA-END
    }
    
    /**
     *
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.readList.int.int.List.annotations:DA-END
    public List<WarnsignalAkustischEntityBean> readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.readList.int.int.List.List:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.readList.int.int.List.List:DA-ELSE
        List<WarnsignalAkustischEntityBean> result = new ArrayList<>();
        for (WarnsignalAkustischEntity entity : WarnsignalAkustischEntityDAO.getAll(entityManager)) {
            WarnsignalAkustischEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.add(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.readList.int.int.List.List:DA-END
    }
    
    /**
     *
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.update.long.WarnsignalAkustischEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.update.long.WarnsignalAkustischEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.update.long.WarnsignalAkustischEntityBean.annotations:DA-END
    public WarnsignalAkustischEntityBean update(long id, WarnsignalAkustischEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.update.long.WarnsignalAkustischEntityBean.WarnsignalAkustischEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.update.long.WarnsignalAkustischEntityBean.WarnsignalAkustischEntityBean:DA-ELSE
        WarnsignalAkustischEntity updatedEntity = WarnsignalAkustischEntityDAO.get(entityManager, bean.getPk());
        WarnsignalAkustischEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = WarnsignalAkustischEntityDAO.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.update.long.WarnsignalAkustischEntityBean.WarnsignalAkustischEntityBean:DA-END
    }
    
    /**
     *
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.create.WarnsignalAkustischEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.create.WarnsignalAkustischEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.create.WarnsignalAkustischEntityBean.annotations:DA-END
    public WarnsignalAkustischEntityBean create(WarnsignalAkustischEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.create.WarnsignalAkustischEntityBean.WarnsignalAkustischEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.create.WarnsignalAkustischEntityBean.WarnsignalAkustischEntityBean:DA-ELSE
        WarnsignalAkustischEntity createdEntity = null;
        WarnsignalAkustischEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null, entityManager);
        if (createdEntity != null) {
            createdEntity = WarnsignalAkustischEntityDAO.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.create.WarnsignalAkustischEntityBean.WarnsignalAkustischEntityBean:DA-END
    }
    
    /**
     *
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.convertFromBeanToEntity.WarnsignalAkustischEntityBean.WarnsignalAkustischEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.convertFromBeanToEntity.WarnsignalAkustischEntityBean.WarnsignalAkustischEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.convertFromBeanToEntity.WarnsignalAkustischEntityBean.WarnsignalAkustischEntity.EntityManager.annotations:DA-END
    public static WarnsignalAkustischEntity convertFromBeanToEntity(WarnsignalAkustischEntityBean bean, WarnsignalAkustischEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.convertFromBeanToEntity.WarnsignalAkustischEntityBean.WarnsignalAkustischEntity.EntityManager.WarnsignalAkustischEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.convertFromBeanToEntity.WarnsignalAkustischEntityBean.WarnsignalAkustischEntity.EntityManager.WarnsignalAkustischEntity:DA-ELSE
        WarnsignalAkustischEntity result = entity;
        if (result == null) {
            result = new WarnsignalAkustischEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            WarnsignalAkustisch dataObj = new WarnsignalAkustisch();
            dataObj.setStatus(bean.getData().isStatus());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.convertFromBeanToEntity.WarnsignalAkustischEntityBean.WarnsignalAkustischEntity.EntityManager.WarnsignalAkustischEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.convertFromEntityToBean.WarnsignalAkustischEntity.WarnsignalAkustischEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.convertFromEntityToBean.WarnsignalAkustischEntity.WarnsignalAkustischEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.convertFromEntityToBean.WarnsignalAkustischEntity.WarnsignalAkustischEntityBean.Map.annotations:DA-END
    public static WarnsignalAkustischEntityBean convertFromEntityToBean(WarnsignalAkustischEntity entity, WarnsignalAkustischEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.convertFromEntityToBean.WarnsignalAkustischEntity.WarnsignalAkustischEntityBean.Map.WarnsignalAkustischEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.convertFromEntityToBean.WarnsignalAkustischEntity.WarnsignalAkustischEntityBean.Map.WarnsignalAkustischEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        WarnsignalAkustischEntityBean result = bean;
        if (result == null) {
            result = new WarnsignalAkustischEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            WarnsignalAkustischBean dataObj = new WarnsignalAkustischBean();
            dataObj.setStatus(entity.getData().isStatus());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.convertFromEntityToBean.WarnsignalAkustischEntity.WarnsignalAkustischEntityBean.Map.WarnsignalAkustischEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.WarnsignalAkustischEntityEJB.additional.elements.in.type:DA-END
} // end of java type