package org.gs.iot.sample.monitoring.gateway.function;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.gs.iot.sample.monitoring.gateway.GeoCoordinates;
import org.gs.iot.sample.monitoring.gateway.MeasurementInfo;
import org.gs.iot.sample.monitoring.gateway.Sound;
import org.gs.iot.sample.monitoring.gateway.SoundEntity;
import org.gs.iot.sample.monitoring.gateway.SoundEntityDAO;
import org.gs.iot.sample.monitoring.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.monitoring.gateway.basic.MeasurementInfoBean;
import org.gs.iot.sample.monitoring.gateway.basic.SoundBean;
import org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean;


//DA-START:function.SoundEntityEJB:DA-START
//DA-ELSE:function.SoundEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.SoundEntityEJB:DA-END

public class SoundEntityEJB implements SoundEntityI { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="H2DS-IOT-DEV")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of SoundEntityEJB
     */
    public SoundEntityEJB() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB:DA-END
    }
    
    
    /**
     *
     * @param id
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.delete.long:DA-ELSE
        SoundEntity entity = SoundEntityDAO.get(entityManager, id);
        if (entity != null) {
            SoundEntityDAO.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.delete.long:DA-END
    }
    
    /**
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.read.long.annotations:DA-END
    public SoundEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.read.long.SoundEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.read.long.SoundEntityBean:DA-ELSE
        SoundEntity entity = SoundEntityDAO.get(entityManager, id);
        SoundEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.read.long.SoundEntityBean:DA-END
    }
    
    /**
     *
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.readList.int.int.List.annotations:DA-END
    public List<SoundEntityBean> readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.readList.int.int.List.List:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.readList.int.int.List.List:DA-ELSE
        List<SoundEntityBean> result = new ArrayList<>();
        for (SoundEntity entity : SoundEntityDAO.getAll(entityManager)) {
            SoundEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.add(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.readList.int.int.List.List:DA-END
    }
    
    /**
     *
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.update.long.SoundEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.update.long.SoundEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.update.long.SoundEntityBean.annotations:DA-END
    public SoundEntityBean update(long id, SoundEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.update.long.SoundEntityBean.SoundEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.update.long.SoundEntityBean.SoundEntityBean:DA-ELSE
        SoundEntity updatedEntity = SoundEntityDAO.get(entityManager, bean.getPk());
        SoundEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = SoundEntityDAO.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.update.long.SoundEntityBean.SoundEntityBean:DA-END
    }
    
    /**
     *
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.create.SoundEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.create.SoundEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.create.SoundEntityBean.annotations:DA-END
    public SoundEntityBean create(SoundEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.create.SoundEntityBean.SoundEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.create.SoundEntityBean.SoundEntityBean:DA-ELSE
        SoundEntity createdEntity = null;
        SoundEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null, entityManager);
        if (createdEntity != null) {
            createdEntity = SoundEntityDAO.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.create.SoundEntityBean.SoundEntityBean:DA-END
    }
    
    /**
     *
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.convertFromBeanToEntity.SoundEntityBean.SoundEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.convertFromBeanToEntity.SoundEntityBean.SoundEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.convertFromBeanToEntity.SoundEntityBean.SoundEntity.EntityManager.annotations:DA-END
    public static SoundEntity convertFromBeanToEntity(SoundEntityBean bean, SoundEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.convertFromBeanToEntity.SoundEntityBean.SoundEntity.EntityManager.SoundEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.convertFromBeanToEntity.SoundEntityBean.SoundEntity.EntityManager.SoundEntity:DA-ELSE
        SoundEntity result = entity;
        if (result == null) {
            result = new SoundEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            Sound dataObj = new Sound();
            dataObj.setStatus(bean.getData().getStatus());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.convertFromBeanToEntity.SoundEntityBean.SoundEntity.EntityManager.SoundEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.convertFromEntityToBean.SoundEntity.SoundEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.convertFromEntityToBean.SoundEntity.SoundEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.convertFromEntityToBean.SoundEntity.SoundEntityBean.Map.annotations:DA-END
    public static SoundEntityBean convertFromEntityToBean(SoundEntity entity, SoundEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.convertFromEntityToBean.SoundEntity.SoundEntityBean.Map.SoundEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.convertFromEntityToBean.SoundEntity.SoundEntityBean.Map.SoundEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        SoundEntityBean result = bean;
        if (result == null) {
            result = new SoundEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            SoundBean dataObj = new SoundBean();
            dataObj.setStatus(entity.getData().getStatus());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.convertFromEntityToBean.SoundEntity.SoundEntityBean.Map.SoundEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityEJB.additional.elements.in.type:DA-END
} // end of java type