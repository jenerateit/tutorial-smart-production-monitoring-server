package org.gs.iot.sample.monitoring.gateway.function;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.gs.iot.sample.monitoring.gateway.GeoCoordinates;
import org.gs.iot.sample.monitoring.gateway.MeasurementInfo;
import org.gs.iot.sample.monitoring.gateway.Temperature;
import org.gs.iot.sample.monitoring.gateway.TemperatureEntity;
import org.gs.iot.sample.monitoring.gateway.TemperatureEntityDAO;
import org.gs.iot.sample.monitoring.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.monitoring.gateway.basic.MeasurementInfoBean;
import org.gs.iot.sample.monitoring.gateway.basic.TemperatureBean;
import org.gs.iot.sample.monitoring.gateway.basic.TemperatureEntityBean;


//DA-START:function.TemperatureEntityEJB:DA-START
//DA-ELSE:function.TemperatureEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.TemperatureEntityEJB:DA-END

public class TemperatureEntityEJB implements TemperatureEntityI { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="H2DS-IOT-DEV")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of TemperatureEntityEJB
     */
    public TemperatureEntityEJB() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB:DA-END
    }
    
    
    /**
     *
     * @param id
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.delete.long:DA-ELSE
        TemperatureEntity entity = TemperatureEntityDAO.get(entityManager, id);
        if (entity != null) {
            TemperatureEntityDAO.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.delete.long:DA-END
    }
    
    /**
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.read.long.annotations:DA-END
    public TemperatureEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.read.long.TemperatureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.read.long.TemperatureEntityBean:DA-ELSE
        TemperatureEntity entity = TemperatureEntityDAO.get(entityManager, id);
        TemperatureEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.read.long.TemperatureEntityBean:DA-END
    }
    
    /**
     *
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.readList.int.int.List.annotations:DA-END
    public List<TemperatureEntityBean> readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.readList.int.int.List.List:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.readList.int.int.List.List:DA-ELSE
        List<TemperatureEntityBean> result = new ArrayList<>();
        for (TemperatureEntity entity : TemperatureEntityDAO.getAll(entityManager)) {
            TemperatureEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.add(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.readList.int.int.List.List:DA-END
    }
    
    /**
     *
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.update.long.TemperatureEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.update.long.TemperatureEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.update.long.TemperatureEntityBean.annotations:DA-END
    public TemperatureEntityBean update(long id, TemperatureEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.update.long.TemperatureEntityBean.TemperatureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.update.long.TemperatureEntityBean.TemperatureEntityBean:DA-ELSE
        TemperatureEntity updatedEntity = TemperatureEntityDAO.get(entityManager, bean.getPk());
        TemperatureEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = TemperatureEntityDAO.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.update.long.TemperatureEntityBean.TemperatureEntityBean:DA-END
    }
    
    /**
     *
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.create.TemperatureEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.create.TemperatureEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.create.TemperatureEntityBean.annotations:DA-END
    public TemperatureEntityBean create(TemperatureEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.create.TemperatureEntityBean.TemperatureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.create.TemperatureEntityBean.TemperatureEntityBean:DA-ELSE
        TemperatureEntity createdEntity = null;
        TemperatureEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null, entityManager);
        if (createdEntity != null) {
            createdEntity = TemperatureEntityDAO.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.create.TemperatureEntityBean.TemperatureEntityBean:DA-END
    }
    
    /**
     *
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.convertFromBeanToEntity.TemperatureEntityBean.TemperatureEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.convertFromBeanToEntity.TemperatureEntityBean.TemperatureEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.convertFromBeanToEntity.TemperatureEntityBean.TemperatureEntity.EntityManager.annotations:DA-END
    public static TemperatureEntity convertFromBeanToEntity(TemperatureEntityBean bean, TemperatureEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.convertFromBeanToEntity.TemperatureEntityBean.TemperatureEntity.EntityManager.TemperatureEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.convertFromBeanToEntity.TemperatureEntityBean.TemperatureEntity.EntityManager.TemperatureEntity:DA-ELSE
        TemperatureEntity result = entity;
        if (result == null) {
            result = new TemperatureEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            Temperature dataObj = new Temperature();
            dataObj.setStatus(bean.getData().getStatus());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.convertFromBeanToEntity.TemperatureEntityBean.TemperatureEntity.EntityManager.TemperatureEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.convertFromEntityToBean.TemperatureEntity.TemperatureEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.convertFromEntityToBean.TemperatureEntity.TemperatureEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.convertFromEntityToBean.TemperatureEntity.TemperatureEntityBean.Map.annotations:DA-END
    public static TemperatureEntityBean convertFromEntityToBean(TemperatureEntity entity, TemperatureEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.convertFromEntityToBean.TemperatureEntity.TemperatureEntityBean.Map.TemperatureEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.convertFromEntityToBean.TemperatureEntity.TemperatureEntityBean.Map.TemperatureEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        TemperatureEntityBean result = bean;
        if (result == null) {
            result = new TemperatureEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            TemperatureBean dataObj = new TemperatureBean();
            dataObj.setStatus(entity.getData().getStatus());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.convertFromEntityToBean.TemperatureEntity.TemperatureEntityBean.Map.TemperatureEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TemperatureEntityEJB.additional.elements.in.type:DA-END
} // end of java type