package org.gs.iot.sample.monitoring.gateway.function;
import java.util.List;

import org.gs.iot.sample.monitoring.gateway.basic.SoundEntityBean;


public interface SoundEntityI { // start of interface

    
    /**
     *
     * @param id  the id
     */
    void delete(long id);
    
    /**
     *
     * @param id  the id
     * @return
     */
    SoundEntityBean read(long id);
    
    /**
     *
     * @param offset  the offset
     * @param limit  the limit
     * @param ids  the ids
     * @return
     */
    List<SoundEntityBean> readList(int offset, int limit, List<String> ids);
    
    /**
     *
     * @param id  the id
     * @param bean  the bean
     * @return
     */
    SoundEntityBean update(long id, SoundEntityBean bean);
    
    /**
     *
     * @param bean  the bean
     * @return
     */
    SoundEntityBean create(SoundEntityBean bean);
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.SoundEntityI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.SoundEntityI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.SoundEntityI.additional.elements.in.type:DA-END
} // end of java type