package org.gs.iot.sample.monitoring.gateway.function;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.gs.iot.sample.monitoring.gateway.GeoCoordinates;
import org.gs.iot.sample.monitoring.gateway.Illuminance;
import org.gs.iot.sample.monitoring.gateway.IlluminanceEntity;
import org.gs.iot.sample.monitoring.gateway.IlluminanceEntityDAO;
import org.gs.iot.sample.monitoring.gateway.MeasurementInfo;
import org.gs.iot.sample.monitoring.gateway.basic.GeoCoordinatesBean;
import org.gs.iot.sample.monitoring.gateway.basic.IlluminanceBean;
import org.gs.iot.sample.monitoring.gateway.basic.IlluminanceEntityBean;
import org.gs.iot.sample.monitoring.gateway.basic.MeasurementInfoBean;


//DA-START:function.IlluminanceEntityEJB:DA-START
//DA-ELSE:function.IlluminanceEntityEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.IlluminanceEntityEJB:DA-END

public class IlluminanceEntityEJB implements IlluminanceEntityI { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.entityManager:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="H2DS-IOT-DEV")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of IlluminanceEntityEJB
     */
    public IlluminanceEntityEJB() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB:DA-END
    }
    
    
    /**
     *
     * @param id
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.delete.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.delete.long:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.delete.long:DA-ELSE
        IlluminanceEntity entity = IlluminanceEntityDAO.get(entityManager, id);
        if (entity != null) {
            IlluminanceEntityDAO.delete(entityManager, entity);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.delete.long:DA-END
    }
    
    /**
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.read.long.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.read.long.annotations:DA-END
    public IlluminanceEntityBean read(long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.read.long.IlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.read.long.IlluminanceEntityBean:DA-ELSE
        IlluminanceEntity entity = IlluminanceEntityDAO.get(entityManager, id);
        IlluminanceEntityBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.read.long.IlluminanceEntityBean:DA-END
    }
    
    /**
     *
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.readList.int.int.List.annotations:DA-END
    public List<IlluminanceEntityBean> readList(int offset, int limit, List<String> ids) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.readList.int.int.List.List:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.readList.int.int.List.List:DA-ELSE
        List<IlluminanceEntityBean> result = new ArrayList<>();
        for (IlluminanceEntity entity : IlluminanceEntityDAO.getAll(entityManager)) {
            IlluminanceEntityBean bean = convertFromEntityToBean(entity, null, null);
            result.add(bean);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.readList.int.int.List.List:DA-END
    }
    
    /**
     *
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.update.long.IlluminanceEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.update.long.IlluminanceEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.update.long.IlluminanceEntityBean.annotations:DA-END
    public IlluminanceEntityBean update(long id, IlluminanceEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.update.long.IlluminanceEntityBean.IlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.update.long.IlluminanceEntityBean.IlluminanceEntityBean:DA-ELSE
        IlluminanceEntity updatedEntity = IlluminanceEntityDAO.get(entityManager, bean.getPk());
        IlluminanceEntityBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = IlluminanceEntityDAO.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.update.long.IlluminanceEntityBean.IlluminanceEntityBean:DA-END
    }
    
    /**
     *
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.create.IlluminanceEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.create.IlluminanceEntityBean.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.create.IlluminanceEntityBean.annotations:DA-END
    public IlluminanceEntityBean create(IlluminanceEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.create.IlluminanceEntityBean.IlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.create.IlluminanceEntityBean.IlluminanceEntityBean:DA-ELSE
        IlluminanceEntity createdEntity = null;
        IlluminanceEntityBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null, entityManager);
        if (createdEntity != null) {
            createdEntity = IlluminanceEntityDAO.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.create.IlluminanceEntityBean.IlluminanceEntityBean:DA-END
    }
    
    /**
     *
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.convertFromBeanToEntity.IlluminanceEntityBean.IlluminanceEntity.EntityManager.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.convertFromBeanToEntity.IlluminanceEntityBean.IlluminanceEntity.EntityManager.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.convertFromBeanToEntity.IlluminanceEntityBean.IlluminanceEntity.EntityManager.annotations:DA-END
    public static IlluminanceEntity convertFromBeanToEntity(IlluminanceEntityBean bean, IlluminanceEntity entity, EntityManager em) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.convertFromBeanToEntity.IlluminanceEntityBean.IlluminanceEntity.EntityManager.IlluminanceEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.convertFromBeanToEntity.IlluminanceEntityBean.IlluminanceEntity.EntityManager.IlluminanceEntity:DA-ELSE
        IlluminanceEntity result = entity;
        if (result == null) {
            result = new IlluminanceEntity();
        }
        result.setPk(bean.getPk());
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setTimeOfMessageReception(bean.getTimeOfMessageReception());
        if (bean.getData() != null) {
            Illuminance dataObj = new Illuminance();
            dataObj.setStatus(bean.getData().getStatus());
            result.setData(dataObj);
        }
        if (bean.getGeoCoordinates() != null) {
            GeoCoordinates geoCoordinatesObj = new GeoCoordinates();
            geoCoordinatesObj.setLng(bean.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(bean.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(bean.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (bean.getMeasurementInfo() != null) {
            MeasurementInfo measurementInfoObj = new MeasurementInfo();
            measurementInfoObj.setTimeOfMeasurement(bean.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(bean.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.convertFromBeanToEntity.IlluminanceEntityBean.IlluminanceEntity.EntityManager.IlluminanceEntity:DA-END
    }
    
    /**
     *
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.convertFromEntityToBean.IlluminanceEntity.IlluminanceEntityBean.Map.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.convertFromEntityToBean.IlluminanceEntity.IlluminanceEntityBean.Map.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.convertFromEntityToBean.IlluminanceEntity.IlluminanceEntityBean.Map.annotations:DA-END
    public static IlluminanceEntityBean convertFromEntityToBean(IlluminanceEntity entity, IlluminanceEntityBean bean, Map<Object, Object> existingBeans) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.convertFromEntityToBean.IlluminanceEntity.IlluminanceEntityBean.Map.IlluminanceEntityBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.convertFromEntityToBean.IlluminanceEntity.IlluminanceEntityBean.Map.IlluminanceEntityBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        IlluminanceEntityBean result = bean;
        if (result == null) {
            result = new IlluminanceEntityBean();
        }
        result.setPk(entity.getPk());
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setTimeOfMessageReception(entity.getTimeOfMessageReception());
        if (entity.getData() != null) {
            IlluminanceBean dataObj = new IlluminanceBean();
            dataObj.setStatus(entity.getData().getStatus());
            result.setData(dataObj);
        }
        if (entity.getGeoCoordinates() != null) {
            GeoCoordinatesBean geoCoordinatesObj = new GeoCoordinatesBean();
            geoCoordinatesObj.setLng(entity.getGeoCoordinates().getLng());
            geoCoordinatesObj.setLat(entity.getGeoCoordinates().getLat());
            geoCoordinatesObj.setAlt(entity.getGeoCoordinates().getAlt());
            result.setGeoCoordinates(geoCoordinatesObj);
        }
        if (entity.getMeasurementInfo() != null) {
            MeasurementInfoBean measurementInfoObj = new MeasurementInfoBean();
            measurementInfoObj.setTimeOfMeasurement(entity.getMeasurementInfo().getTimeOfMeasurement());
            measurementInfoObj.setTimeZoneOfMeasurement(entity.getMeasurementInfo().getTimeZoneOfMeasurement());
            result.setMeasurementInfo(measurementInfoObj);
        }
        return result;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.convertFromEntityToBean.IlluminanceEntity.IlluminanceEntityBean.Map.IlluminanceEntityBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.IlluminanceEntityEJB.additional.elements.in.type:DA-END
} // end of java type