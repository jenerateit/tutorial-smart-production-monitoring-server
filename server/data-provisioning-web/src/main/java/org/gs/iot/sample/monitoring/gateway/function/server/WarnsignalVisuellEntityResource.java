package org.gs.iot.sample.monitoring.gateway.function.server;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Providers;

import org.gs.iot.sample.monitoring.gateway.basic.WarnsignalVisuellEntityBean;
import org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityI;

//DA-START:server.WarnsignalVisuellEntityResource:DA-START
//DA-ELSE:server.WarnsignalVisuellEntityResource:DA-ELSE
@Consumes(value={"application/xml", "application/json"})
@Produces(value={"application/xml", "application/json"})
@Path(value="/warnsignalvisuellentity")
@RequestScoped
//DA-END:server.WarnsignalVisuellEntityResource:DA-END

public class WarnsignalVisuellEntityResource { // start of class

    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.uriInfo:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.uriInfo:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.uriInfo:DA-END
    private UriInfo uriInfo;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.httpHeaders:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.httpHeaders:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.httpHeaders:DA-END
    private HttpHeaders httpHeaders;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.securityContext:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.securityContext:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.securityContext:DA-END
    private SecurityContext securityContext;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.request:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.request:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.request:DA-END
    private Request request;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.providers:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.providers:DA-ELSE
    @Context
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.providers:DA-END
    private Providers providers;
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.warnsignalVisuellEntityBean:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.warnsignalVisuellEntityBean:DA-ELSE
    @EJB
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.warnsignalVisuellEntityBean:DA-END
    private WarnsignalVisuellEntityI warnsignalVisuellEntityBean;
    
    
    /**
     * there is no Java type to be used to provide data for the response body
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.delete.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.delete.long.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.delete.long.annotations:DA-END
    public Response delete(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.delete.long.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.delete.long.Response:DA-ELSE
        warnsignalVisuellEntityBean.delete(id);
        return Response.ok().build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.delete.long.Response:DA-END
    }
    
    /**
     * there is no Java type to be used to provide data for the response body
     *
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.deleteByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.deleteByType.long.String.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.deleteByType.long.String.annotations:DA-END
    public Response deleteByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.deleteByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.deleteByType.long.Response.String:DA-ELSE
        warnsignalVisuellEntityBean.delete(id);
        return Response.ok().build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.deleteByType.long.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityBean'
     *
     * @param id
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.read.long.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.read.long.annotations:DA-ELSE
    @GET
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.read.long.annotations:DA-END
    public Response read(@PathParam(value="id") long id) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.read.long.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.read.long.Response:DA-ELSE
        WarnsignalVisuellEntityBean result = warnsignalVisuellEntityBean.read(id);
        return Response.ok(result, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.read.long.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityBean'
     *
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readByType.long.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readByType.long.String.annotations:DA-ELSE
    @GET
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readByType.long.String.annotations:DA-END
    public Response readByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readByType.long.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readByType.long.Response.String:DA-ELSE
        WarnsignalVisuellEntityBean result = warnsignalVisuellEntityBean.read(id);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readByType.long.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'List<WarnsignalVisuellEntityBean>'
     *
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readList.int.int.List.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readList.int.int.List.annotations:DA-ELSE
    @GET
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readList.int.int.List.annotations:DA-END
    public Response readList(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readList.int.int.List.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readList.int.int.List.Response:DA-ELSE
        List<WarnsignalVisuellEntityBean> result = warnsignalVisuellEntityBean.readList(offset, limit, ids);
        GenericEntity<List<WarnsignalVisuellEntityBean>> entity = new GenericEntity<List<WarnsignalVisuellEntityBean>>(result) {};
        return Response.ok(entity, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readList.int.int.List.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'List<WarnsignalVisuellEntityBean>'
     *
     * @param offset
     * @param limit
     * @param ids
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readListByType.int.int.List.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readListByType.int.int.List.String.annotations:DA-ELSE
    @GET
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readListByType.int.int.List.String.annotations:DA-END
    public Response readListByType(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readListByType.int.int.List.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readListByType.int.int.List.Response.String:DA-ELSE
        List<WarnsignalVisuellEntityBean> result = warnsignalVisuellEntityBean.readList(offset, limit, ids);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.readListByType.int.int.List.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityBean'
     *
     * @param id
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.update.long.WarnsignalVisuellEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.update.long.WarnsignalVisuellEntityBean.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.update.long.WarnsignalVisuellEntityBean.annotations:DA-END
    public Response update(@PathParam(value="id") long id, WarnsignalVisuellEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.update.long.WarnsignalVisuellEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.update.long.WarnsignalVisuellEntityBean.Response:DA-ELSE
        WarnsignalVisuellEntityBean result = warnsignalVisuellEntityBean.update(id, bean);
        return Response.ok(result, httpHeaders.getAcceptableMediaTypes().get(0)).build();
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.update.long.WarnsignalVisuellEntityBean.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityBean'
     *
     * @param id
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.updateByType.long.WarnsignalVisuellEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.updateByType.long.WarnsignalVisuellEntityBean.String.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.updateByType.long.WarnsignalVisuellEntityBean.String.annotations:DA-END
    public Response updateByType(@PathParam(value="id") long id, WarnsignalVisuellEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.updateByType.long.WarnsignalVisuellEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.updateByType.long.WarnsignalVisuellEntityBean.Response.String:DA-ELSE
        WarnsignalVisuellEntityBean result = warnsignalVisuellEntityBean.update(id, bean);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.updateByType.long.WarnsignalVisuellEntityBean.Response.String:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityBean'
     *
     * @param bean
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.create.WarnsignalVisuellEntityBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.create.WarnsignalVisuellEntityBean.annotations:DA-ELSE
    @POST
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.create.WarnsignalVisuellEntityBean.annotations:DA-END
    public Response create(WarnsignalVisuellEntityBean bean) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.create.WarnsignalVisuellEntityBean.Response:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.create.WarnsignalVisuellEntityBean.Response:DA-ELSE
        WarnsignalVisuellEntityBean result = warnsignalVisuellEntityBean.create(bean);
        String mediaType = httpHeaders.getRequestHeader(HttpHeaders.ACCEPT).get(0);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.create.WarnsignalVisuellEntityBean.Response:DA-END
    }
    
    /**
     * the response body gets its data from an instance of 'org.gs.iot.sample.monitoring.gateway.function.WarnsignalVisuellEntityBean'
     *
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.createByType.WarnsignalVisuellEntityBean.String.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.createByType.WarnsignalVisuellEntityBean.String.annotations:DA-ELSE
    @POST
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.createByType.WarnsignalVisuellEntityBean.String.annotations:DA-END
    public Response createByType(WarnsignalVisuellEntityBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.createByType.WarnsignalVisuellEntityBean.Response.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.createByType.WarnsignalVisuellEntityBean.Response.String:DA-ELSE
        WarnsignalVisuellEntityBean result = warnsignalVisuellEntityBean.create(bean);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(result.getPk().toString());
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.createByType.WarnsignalVisuellEntityBean.Response.String:DA-END
    }
    
    /**
     * setter for the field uriInfo
     *
     *
     *
     * @param uriInfo  the uriInfo
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setUriInfo.UriInfo.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setUriInfo.UriInfo.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setUriInfo.UriInfo.annotations:DA-END
    public void setUriInfo(UriInfo uriInfo) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setUriInfo.UriInfo:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setUriInfo.UriInfo:DA-ELSE
        this.uriInfo = uriInfo;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setUriInfo.UriInfo:DA-END
    }
    
    /**
     * setter for the field httpHeaders
     *
     *
     *
     * @param httpHeaders  the httpHeaders
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setHttpHeaders.HttpHeaders.annotations:DA-END
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setHttpHeaders.HttpHeaders:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setHttpHeaders.HttpHeaders:DA-ELSE
        this.httpHeaders = httpHeaders;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setHttpHeaders.HttpHeaders:DA-END
    }
    
    /**
     * setter for the field securityContext
     *
     *
     *
     * @param securityContext  the securityContext
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setSecurityContext.SecurityContext.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setSecurityContext.SecurityContext.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setSecurityContext.SecurityContext.annotations:DA-END
    public void setSecurityContext(SecurityContext securityContext) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setSecurityContext.SecurityContext:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setSecurityContext.SecurityContext:DA-ELSE
        this.securityContext = securityContext;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setSecurityContext.SecurityContext:DA-END
    }
    
    /**
     * setter for the field request
     *
     *
     *
     * @param request  the request
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setRequest.Request.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setRequest.Request.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setRequest.Request.annotations:DA-END
    public void setRequest(Request request) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setRequest.Request:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setRequest.Request:DA-ELSE
        this.request = request;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setRequest.Request:DA-END
    }
    
    /**
     * setter for the field providers
     *
     *
     *
     * @param providers  the providers
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setProviders.Providers.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setProviders.Providers.annotations:DA-ELSE
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setProviders.Providers.annotations:DA-END
    public void setProviders(Providers providers) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setProviders.Providers:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setProviders.Providers:DA-ELSE
        this.providers = providers;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.setProviders.Providers:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.server.WarnsignalVisuellEntityResource.additional.elements.in.type:DA-END
} // end of java type