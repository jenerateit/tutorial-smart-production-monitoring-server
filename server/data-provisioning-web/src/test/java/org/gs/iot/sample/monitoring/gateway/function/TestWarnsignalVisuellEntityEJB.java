package org.gs.iot.sample.monitoring.gateway.function;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


//DA-START:function.TestWarnsignalVisuellEntityEJB:DA-START
//DA-ELSE:function.TestWarnsignalVisuellEntityEJB:DA-ELSE
//DA-END:function.TestWarnsignalVisuellEntityEJB:DA-END
public class TestWarnsignalVisuellEntityEJB { // start of class

    /**
     * creates an instance of TestWarnsignalVisuellEntityEJB
     */
    public TestWarnsignalVisuellEntityEJB() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB:DA-END
    }
    
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.setUpBeforeClass:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.tearDownAfterClass:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.setUp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.setUp:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.setUp:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.tearDown.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.tearDown:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.tearDown:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testDelete.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testDelete:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testDelete:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testRead.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testRead:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testRead:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testRead:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testReadList.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testReadList:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testReadList:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testReadList:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testUpdate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testUpdate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testUpdate:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testCreate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testCreate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testCreate:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testConvertFromBeanToEntity:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalVisuellEntityEJB.additional.elements.in.type:DA-END
} // end of java type