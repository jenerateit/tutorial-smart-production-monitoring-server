package org.gs.iot.sample.monitoring.gateway.function;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


//DA-START:function.TestWarnsignalAkustischEntityEJB:DA-START
//DA-ELSE:function.TestWarnsignalAkustischEntityEJB:DA-ELSE
//DA-END:function.TestWarnsignalAkustischEntityEJB:DA-END
public class TestWarnsignalAkustischEntityEJB { // start of class

    /**
     * creates an instance of TestWarnsignalAkustischEntityEJB
     */
    public TestWarnsignalAkustischEntityEJB() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB:DA-END
    }
    
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.setUpBeforeClass:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.tearDownAfterClass:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.setUp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.setUp:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.setUp:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.tearDown.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.tearDown:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.tearDown:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testDelete.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testDelete:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testDelete:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testRead.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testRead:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testRead:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testRead:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testReadList.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testReadList:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testReadList:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testReadList:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testUpdate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testUpdate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testUpdate:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testCreate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testCreate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testCreate:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testConvertFromBeanToEntity:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestWarnsignalAkustischEntityEJB.additional.elements.in.type:DA-END
} // end of java type