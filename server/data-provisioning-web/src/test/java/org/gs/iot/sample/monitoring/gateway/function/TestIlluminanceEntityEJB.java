package org.gs.iot.sample.monitoring.gateway.function;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


//DA-START:function.TestIlluminanceEntityEJB:DA-START
//DA-ELSE:function.TestIlluminanceEntityEJB:DA-ELSE
//DA-END:function.TestIlluminanceEntityEJB:DA-END
public class TestIlluminanceEntityEJB { // start of class

    /**
     * creates an instance of TestIlluminanceEntityEJB
     */
    public TestIlluminanceEntityEJB() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB:DA-END
    }
    
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.setUpBeforeClass:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.tearDownAfterClass:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.setUp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.setUp:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.setUp:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.tearDown.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.tearDown:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.tearDown:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testDelete.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testDelete:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testDelete:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testRead.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testRead:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testRead:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testRead:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testReadList.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testReadList:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testReadList:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testReadList:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testUpdate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testUpdate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testUpdate:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testCreate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testCreate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testCreate:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testConvertFromBeanToEntity:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestIlluminanceEntityEJB.additional.elements.in.type:DA-END
} // end of java type