package org.gs.iot.sample.monitoring.gateway.function;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


//DA-START:function.TestSoundEntityEJB:DA-START
//DA-ELSE:function.TestSoundEntityEJB:DA-ELSE
//DA-END:function.TestSoundEntityEJB:DA-END
public class TestSoundEntityEJB { // start of class

    /**
     * creates an instance of TestSoundEntityEJB
     */
    public TestSoundEntityEJB() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB:DA-END
    }
    
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.setUpBeforeClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.setUpBeforeClass:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.tearDownAfterClass:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.tearDownAfterClass:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.setUp.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.setUp:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.setUp:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.setUp:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.tearDown.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.tearDown:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.tearDown:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.tearDown:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testDelete.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testDelete:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testDelete:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testDelete:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testRead.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testRead:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testRead:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testRead:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testReadList.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testReadList:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testReadList:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testReadList:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testUpdate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testUpdate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testUpdate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testUpdate:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testCreate.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testCreate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testCreate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testCreate:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testConvertFromBeanToEntity:DA-END
    }
    
    /**
     */
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.function.TestSoundEntityEJB.additional.elements.in.type:DA-END
} // end of java type